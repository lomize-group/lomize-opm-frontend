import * as appendQuery from "append-query";

export const Membranome = {
    urls: {
        //api_base: "https://lomize-group-membranome.herokuapp.com",
        api_base: "https://opm-back.cc.lehigh.edu/membranome-backend",
        // api_base: "http://localhost:8000",
        websiteBase: "https://membranome.org",
        protein_link: function (protein_id: string) {
            return this.websiteBase + "/proteins/" + protein_id;
        },
        api_url: function (suffix: string, query = {}) {
            const url = this.api_base + "/" + suffix;
            return appendQuery(url, query);
        },
    },

    definitions: {
        opm_links: {
            name: "opmlinks",
            api: {
                route: "/opm_links",
            },
        },
    },
};
