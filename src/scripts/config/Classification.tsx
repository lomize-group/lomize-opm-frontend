const categories = ["proteins", "assemblies"] as const;

const proteins = [
    "protein_types",
    "protein_classes",
    "protein_superfamilies",
    "protein_families",
    "protein_species",
    "protein_localizations",
    "proteins",
] as const;

const assemblies = [
    "assembly_superfamilies",
    "assembly_families",
    "assembly_localizations",
    "assemblies",
] as const;

const ctypes = [
    ...proteins,
    ...assemblies,
    "secondary_representations",
    "structure_subunits",
    "citations",
] as const;

export type CTypeCategory = typeof categories[number];

export type ProteinCType = typeof proteins[number];
export type AssemblyCType = typeof assemblies[number];
export type CType = typeof ctypes[number];

export interface Definition {
    name: string;
    api: {
        route: string;
        accessor: {
            object: string;
            count: string;
        };
    };
    parents: Array<CType>;
    child?: CType;
    column?: number;
    [key: string]: any;
}

interface IClassification {
    definitions: { [key in CType]: Definition };
    ctypes: typeof ctypes;
    proteins: typeof proteins;
    assemblies: typeof assemblies;
    [key: string]: any;
}

// The PFAM database was discontinued but a copy of it is at Interpro.
// The Interpro db is a separate entity, however.
const interproPfamUrl =  (pfam_id: string): string => {
    if (pfam_id[0] === "C" && pfam_id[1] === "L") {
        return "https://www.ebi.ac.uk/interpro/set/pfam/" + pfam_id + "/";
    }
    return "https://www.ebi.ac.uk/interpro/entry/pfam/" + pfam_id + "/";
};

const Classification: IClassification = {
    ctypes,
    proteins,
    assemblies,

    definitions: {
        protein_types: {
            name: "types",
            api: {
                route: "/types",
                accessor: {
                    object: "type",
                    count: "types_count",
                },
            },
            parents: [],
            child: "protein_classes",
        },
        protein_classes: {
            name: "classes",
            api: {
                route: "/classtypes",
                accessor: {
                    object: "classtype",
                    count: "classtypes_count",
                },
            },
            parents: ["protein_types"],
            child: "protein_superfamilies",
        },
        protein_superfamilies: {
            name: "superfamilies",
            api: {
                route: "/superfamilies",
                accessor: {
                    object: "superfamily",
                    count: "superfamilies_count",
                },
            },
            parents: ["protein_classes"],
            child: "protein_families",
            tcdb: (tcdb_id: string): string =>
                "http://www.tcdb.org/search/result.php?tc=" + tcdb_id,
            pfam: interproPfamUrl,
        },
        protein_families: {
            name: "families",
            column: 0,
            api: {
                route: "/families",
                accessor: {
                    object: "family",
                    count: "families_count",
                },
            },
            parents: ["protein_superfamilies"],
            child: "proteins",
            tcdb: (tcdb_id: string): string =>
                "http://www.tcdb.org/search/result.php?tc=" + tcdb_id,
            pfam: interproPfamUrl,
            interpro: (interpro_id: string): string =>
                "https://www.ebi.ac.uk/interpro/entry/InterPro/" + interpro_id + "/",
            pdb: (pfam_id: string): string =>
                "http://www.ebi.ac.uk/thornton-srv/databases/cgi-bin/pdbsum/GetPfamStr.pl?pfam_id=" +
                pfam_id,
        },
        protein_species: {
            name: "species",
            column: 3,
            api: {
                route: "/species",
                accessor: {
                    object: "species",
                    count: "species_count",
                },
            },
            parents: [],
            child: "proteins",
        },
        protein_localizations: {
            // aka membranes
            name: "localizations",
            column: 4,
            api: {
                route: "/membranes",
                accessor: {
                    object: "membrane",
                    count: "membranes_count",
                },
            },
            parents: [],
            child: "proteins",
        },
        proteins: {
            name: "proteins",
            api: {
                route: "/primary_structures",
                accessor: {
                    object: "primary_structure",
                    count: "primary_structures_count",
                },
            },
            parents: ["protein_families", "protein_localizations", "protein_species"],
            pdbsum: (pdb_id: string): string =>
                "http://www.ebi.ac.uk/thornton-srv/databases/cgi-bin/pdbsum/GetPage.pl?pdbcode=" +
                pdb_id,
            pdb: (pdb_id: string): string => "https://www.rcsb.org/structure/" + pdb_id,
            pdb_image: (pdb_id: string): string =>
                "https://biomembhub.org/shared/opm-assets/images/pdb/" + pdb_id + ".png",
            pdb_file: (pdb_id: string): string =>
                "https://biomembhub.org/shared/opm-assets/pdb/" + pdb_id + ".pdb",
            scop: (pdb_id: string): string =>
                "http://scop.mrc-lmb.cam.ac.uk/scop/pdb.cgi?disp=scop&id=" + pdb_id,
            msd: (pdb_id: string): string =>
                "http://www.ebi.ac.uk/pdbe-srv/view/entry/" + pdb_id + "/summary.html",
            oca: (pdb_id: string): string =>
                "http://bip.weizmann.ac.il/oca-bin/ocashort?id=" + pdb_id,
            mmdb: (pdb_id: string): string =>
                "http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=" + pdb_id,
            encompass: (pdb_id: string): string =>
                "https://encompass.ninds.nih.gov/pdb?pdbid=" + pdb_id,
        },
        secondary_representations: {
            name: "related PDB entries",
            api: {
                route: "/secondary_representations",
                accessor: {
                    object: "secondary_representation",
                    count: "secondary_representations_count",
                },
            },
            parents: [],
        },
        structure_subunits: {
            name: "subunits",
            api: {
                route: "/structure_subunits",
                accessor: {
                    object: "structure_subunits",
                    count: "structure_subunits_count",
                },
            },
            parents: [],
        },
        citations: {
            name: "citations",
            api: {
                route: "/citations",
                accessor: {
                    object: "citations",
                    count: "citations_count",
                },
            },
            parents: [],
        },
        assembly_superfamilies: {
            name: "superfamilies",
            api: {
                route: "/assembly_superfamilies",
                accessor: {
                    object: "superfamily",
                    count: "superfamilies_count",
                },
            },
            tcdb: (tcdb_id: string): string =>
                "http://www.tcdb.org/search/result.php?tc=" + tcdb_id,
            pfam: interproPfamUrl,
            parents: [],
            child: "assembly_families",
        },
        assembly_families: {
            name: "families",
            api: {
                route: "/assembly_families",
                accessor: {
                    object: "family",
                    count: "families_count",
                },
            },
            tcdb: (tcdb_id: string): string =>
                "http://www.tcdb.org/search/result.php?tc=" + tcdb_id,
            pfam: interproPfamUrl,
            pdb: (pfam_id: string): string =>
                "http://www.ebi.ac.uk/thornton-srv/databases/cgi-bin/pdbsum/GetPfamStr.pl?pfam_id=" +
                pfam_id,
            parents: ["assembly_superfamilies"],
            child: "assemblies",
        },
        assembly_localizations: {
            name: "localizations",
            api: {
                route: "/assembly_membranes",
                accessor: {
                    object: "membranes",
                    count: "membranes_count",
                },
            },
            parents: [],
            child: "assemblies",
        },
        assemblies: {
            name: "assemblies",
            api: {
                route: "/assemblies",
                accessor: {
                    object: "assembly",
                    count: "assemblies_count",
                },
            },
            pdb_file: (pdb_id: string, protein_letter: string): string =>
                "https://biomembhub.org/shared/opm-assets/assembly/" +
                pdb_id +
                protein_letter +
                "path.pdb",
            parents: ["assembly_superfamilies", "assembly_localizations"],
        },
    },
};

export default Classification;
