import { Urls } from "./Urls";

const { cloud_storage_prefix } = Urls;

export const Assets = {
    logfile: cloud_storage_prefix + "downloads/logfile.txt",
    tmpfold_source: cloud_storage_prefix + "tmpfold_code/tmpfold.tar.gz",
    images: {
        no_image: "/images/pdb_fallback.png",
        loading: "/images/loading.gif",
        loading_small: "/images/loading_small.gif",
        ppm_server: "/images/PPM_server_small.jpg",
        tmpfold_server: "/images/TMPfold_server_small.jpg",
        topology: cloud_storage_prefix + "images/bio_assets/topology.gif",
        andrei: cloud_storage_prefix + "images/portraits/andrei_lomize.jpg",
        irina: cloud_storage_prefix + "images/portraits/irina_pogozheva.jpg",
        alexey: cloud_storage_prefix + "images/portraits/alexey_kovalenko.jpg",
        directories: {
            bio: cloud_storage_prefix + "images/bio_assets/",
            math: cloud_storage_prefix + "images/math_assets/",
        },
    },
    pdb_tar_filenames: [
        "all_pdbs.tar.gz",
        "Alpha-helical_polytopic.tar.gz",
        "Beta-barrel_transmembrane.tar.gz",
        "Bitopic_proteins.tar.gz",
        "Monotopic_peripheral.tar.gz",
        "Peptides.tar.gz",
    ],

    image_asset: (imgName: string) => "https://biomembhub.org/shared/opm-assets/images/image_assets/" + imgName,
    // image_asset: (imgName: string) => "https://storage.googleapis.com/cellpm-assets/images/image_assets/" + imgName,
};
