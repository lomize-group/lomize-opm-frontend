import { CType } from "./Classification";

export type GenericObj<V = any> = { [key: string]: V };

export interface ApiPagedResponse<T = any> {
    total_objects: number;
    objects: Array<T>;
    sort: string;
    direction: string;
    search?: string;
    page_size: number;
    page_num: number;
}

export type TableColumn = {
    Header: () => JSX.Element;
    Cell: (row: any) => JSX.Element;
    id: string;
    flexGrow?: number;
    minWidth?: number;
    width?: number;
    cpage?: CType[];
};
