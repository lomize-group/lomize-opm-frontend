import { GenericObj } from "./Types";

export const validate = {
    email: (email?: string, allowEmpty: boolean = true): boolean => {
        if (!email) {
            return allowEmpty;
        }
        const re: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    },
    tmSegments: (segments: string): Array<string> => {
        // record and return all errors
        let errors: Array<string> = [];
        if (!segments) {
            errors.push("no segments entered");
            return errors;
        }

        // split segment text into separate segments
        // filter empty string to ignore consecutive newlines
        const eachSegment = segments.split("\n").filter((val) => val !== "");
        const segmentPattern = /^[a-zA-Z\d][:;](\d+\(\d+-\d+\),)*(\d+\(\d+-\d+\))$/;
        for (let i = 0; i < eachSegment.length; ++i) {
            // replace whitespace
            const seg = eachSegment[i].replace(/ /g, "").replace(/\t/g, "");
            if (!segmentPattern.test(seg)) {
                // error message for each bad line
                errors.push(`line ${i + 1}`);
            }
        }
        return errors;
    },
    pdbFilename: (filename?: string): boolean => {
        if (!filename || filename.indexOf(".") < 0) {
            return false;
        }
        const filename_parts = filename.split(".");
        return filename_parts[filename_parts.length - 1].toLowerCase() === "pdb";
    },
    pdbCode: (pdb_code: string): boolean => {
        // just make sure it's not blank
        return pdb_code.length > 0;
    },
} as const;

export const capitalize = (s: string): string => s.charAt(0).toUpperCase() + s.slice(1);

/**
 * Immutable sort of an array strings or array of objects with a string property.
 * @param objs array of string or object with string key to be sorted.
 * @param criteria the sort criteria.
 * 	if undefined: does a standard sort on the array of strings.
 * 		only makes sense to use if T is string.
 * 	if string: sorts the array of objects by key.
 * 		only makes sense to use if T is object.
 * 	if function: sorts the array of objects by the value returned by the function.
 * 		can be used if T is string or object.
 * @returns the objects sorted by given criteria
 */
export function sortStrings<T extends string | GenericObj>(
    [...objs]: Array<T>,
    criteria?: keyof T | ((item: any) => string),
    reverse = false
) {
    if (criteria !== undefined) {
        // sort object by criteria function
        if (typeof criteria === "function") {
            if (reverse) {
                return objs.sort((a, b) => {
                    const aCrit = criteria(a);
                    const bCrit = criteria(b);
                    return bCrit < aCrit ? -1 : bCrit > aCrit ? 1 : 0;
                });
            }
            return objs.sort((a, b) => {
                const aCrit = criteria(a);
                const bCrit = criteria(b);
                return aCrit < bCrit ? -1 : aCrit > bCrit ? 1 : 0;
            });
        }
        // sort object by string key
        if (reverse) {
            return objs.sort((a, b) =>
                b[criteria] < a[criteria] ? -1 : b[criteria] > a[criteria] ? 1 : 0
            );
        }
        return objs.sort((a, b) =>
            a[criteria] < b[criteria] ? -1 : a[criteria] > b[criteria] ? 1 : 0
        );
    }
    if (reverse) {
        return objs.sort((a, b) => (b < a ? -1 : b > a ? 1 : 0));
    }
    return objs.sort((a, b) => (a < b ? -1 : a > b ? 1 : 0));
}

/**
 * Reads a file into a string.
 *
 * @param inFile {File}: The file to read
 *
 * @returns {Promise<string>}: A promise containing the file as a string
 */
export async function readFile(inFile: File, onError?: () => any): Promise<string> {
    const reader = new FileReader();
    return new Promise((resolve, reject) => {
        reader.onload = () => resolve(reader.result as string);
        reader.onerror = () => reject(onError ? onError() : "ERROR READING FILE");
        reader.readAsText(inFile);
    });
}
