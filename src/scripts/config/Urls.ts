import * as appendQuery from "append-query";

export type ComputationServer = "memprot" | "cgopm" | "local";

const googleCloudPrefix = "https://biomembhub.org/shared/opm-assets/" as const;
const computation_servers = {
    memprot: "https://memprot.org/",
    cgopm: "https://cgopm.cc.lehigh.edu/",
    local: "http://localhost:9000/",
};

export const Urls = {
    computation_servers,
    cloud_storage_prefix: googleCloudPrefix,
    //api_base: "https://lomize-group-opm.herokuapp.com",
    api_base: "https://opm-back.cc.lehigh.edu/opm-backend",
    //api_base: "http://localhost:3000",
    fetchPdbInfo: computation_servers.memprot + "opm_pdb_retrieval",
    fetchPdbInfoPrimaryStructure: computation_servers.memprot + "opm_pdb_retrieval/primary_structure",
    pdf: (pdf_name: string): string => googleCloudPrefix + "pdfs/" + pdf_name + ".pdf",
    pdb_tar_file: (tar_filename: string): string =>
        googleCloudPrefix + "pdb/tar_files/" + tar_filename,
    pdb_source_file: (pdb_id: string): string => googleCloudPrefix + "pdb/" + pdb_id + ".pdb",
    pdb_lipid_file: (pdb_id: string): string => googleCloudPrefix + "pdb-lipids/" + pdb_id + ".pdb",
    rcsb: {
        asymmetric_unit: (pdb_id: string): string => "https://files.rcsb.org/download/" + pdb_id + ".pdb",
        biological_assembly: (pdb_id: string, number: number): string => "https://files.rcsb.org/download/" + pdb_id + ".pdb" + number + ".gz",
    },
    md_input_tar_file: (pdb_id: string): string =>
        googleCloudPrefix + "MD-inputs/" + pdb_id + ".tgz",
    jmol: (pdb_id: string): string =>
        "http://bioinformatics.org/firstglance/fgij//fg.htm?mol=" +
        googleCloudPrefix +
        "pdb/" +
        pdb_id +
        ".pdb",
    icn3d: (pdb_id: string): string =>
        "https://www.ncbi.nlm.nih.gov/Structure/icn3d/full.html?opmid=" + pdb_id,
    pubmed: (pmid: string): string => "https://www.ncbi.nlm.nih.gov/pubmed?cmd=search&term=" + pmid,
    uniprot: (uniprot_code: string): string => "https://www.uniprot.org/uniprot/" + uniprot_code,
    api_url: function (suffix: string, query: { [key: string]: any } = {}): string {
        const url = this.api_base + "/" + suffix;
        return appendQuery(url, query);
    },
} as const;
