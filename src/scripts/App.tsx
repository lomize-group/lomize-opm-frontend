import { History } from "history";
import * as qs from "query-string";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { match } from "react-router-dom";
import "./App.scss";
import Footer from "./components/footer/Footer";
import Header from "./components/header/Header";
import MainContent from "./components/main-content/MainContent";
import Search from "./components/search/Search";
import Sidebar from "./components/sidebar/Sidebar";
import { updateCurrentUrl } from "./store/url";

const connector = connect(undefined, { updateCurrentUrl });

type AppProps = ConnectedProps<typeof connector> & {
    match: match;
    location: {
        search: string;
    };
    history: History;
};

class App extends React.Component<AppProps> {
    constructor(props: AppProps) {
        super(props);
        props.updateCurrentUrl({
            ...props.match,
            query: qs.parse(props.location.search),
            history: props.history,
        });
    }

    handleSearch = (value: string) => {
        this.props.history.push("/proteins?search=" + value);
    };

    componentDidUpdate(nextProps: AppProps) {
        this.props.updateCurrentUrl({
            ...nextProps.match,
            query: qs.parse(nextProps.location.search),
            history: this.props.history,
        });
    }

    render() {
        // #layout of the main app
        return (
            <div className="app">
                <div className="search-header">
                    <div className="header-links">
                        <a target="_blank" href="http://www.umich.edu">
                            UNIVERSITY OF MICHIGAN
                        </a>{" "}
                        |{" "}
                        <a target="_blank" href="http://www.umich.edu/~pharmacy/">
                            COLLEGE OF PHARMACY
                        </a>{" "}
                        |{" "}
                        <a target="_blank" href="https://pharmacy.umich.edu/lomize-group">
                            LOMIZE GROUP
                        </a>
                    </div>
                    <div className="search-opm">
                        <Search
                            placeholder="Search proteins by PDB ID or name"
                            onSearch={this.handleSearch}
                            initial=""
                        />
                    </div>
                </div>
                <Header />
                <div className="flex-container">
                    <Sidebar />
                    <MainContent>{this.props.children}</MainContent>
                    <div className="sidebar-right"></div>
                </div>
                <Footer />
            </div>
        );
    }
}

export default connector(App);
