// polyfills
import "core-js/es6/map";
import "core-js/es6/set";
import "core-js/es6/number";
import "raf/polyfill";

import * as React from "react";
import * as ReactDOM from "react-dom";

import App from "./App";
import Error404 from "./Error404";

import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import { logger } from "redux-logger";

import { rootReducer } from "./store";

import * as qs from "query-string";
import * as appendQuery from "append-query";

import "bootstrap-sass/assets/stylesheets/_bootstrap.scss";

import Home from "./components/home/Home";
import Classification from "./components/classification/Classification.js";
import SpeciesTable from "./components/species_table/SpeciesTable";
import Protein from "./components/protein/Protein.js";
import ProteinLipid from "./components/protein_lipid/ProteinLipid.js";
import ProteinViewer from "./components/protein_viewer/ProteinViewer.js";
import About from "./components/about/About";
import Download from "./components/download/Download.js";
import Contact from "./components/contact/Contact.js";
import PPMDisambiguation from "./components/ppm_disambiguation/PPMDisambiguation";
import PPMServer from "./components/ppm_server/PPMServer";
import PPMServer3 from "./components/ppm_server3/PPMServer3";
import TMPfoldServer from "./components/tmpfold_server/TMPfoldServer";
import Assembly from "./components/assembly/Assembly.js";
import FetchPdbInfo from "./components/fetch_pdb_info/FetchPdbInfo";
import OpmUpdate from "./components/opm_update/OpmUpdate";
import OpmAlign from "./components/opm_align/OpmAlign";
import CSV2SQL from "./components/csv2sql/CSV2SQL";
import AdvancedSearch from "./components/advanced_search/AdvancedSearch.js";
import Fold from "./components/fold/Fold";

let store = createStore(
    rootReducer,
    applyMiddleware(logger)
);

import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import BiologicalMembranes from "./components/biological_membranes/BiologicalMembranes";

class Main extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <Switch>
                        <Route exact path="/" component={(props) => (<App children={<Home />} {...props} />)} />

                        <Route exact path="/about" component={(props) => (<App children={<About />} {...props} />)} />
                        <Route exact path="/about.php" component={() => <Redirect to="/about" />} />

                        <Route exact path="/biological_membranes/lipid_composition" component={(props) => (<App children={<BiologicalMembranes subsection={"lipid_composition"} />} {...props}/>)}/>
                        <Route exact path="/biological_membranes/membrane_properties" component={(props) => (<App children={<BiologicalMembranes subsection={"membrane_properties"} />} {...props}/>)}/>
                        <Route exact path="/biological_membranes/biological_descriptions" component={(props) => (<App children={<BiologicalMembranes subsection={"biological_descriptions"} />} {...props}/>)}/>

                        <Route exact path="/download" component={(props) => (<App children={<Download />} {...props} />)} />
                        <Route exact path="/download.php" component={() => <Redirect to="/download" />} />

                        <Route exact path="/contact" component={(props) => (<App children={<Contact />} {...props} />)} />
                        <Route exact path="/contact.php" component={() => <Redirect to="/contact" />} />

                        <Route exact path="/ppm_server" component={(props) => (<App {...props}><PPMDisambiguation /></App>)} />

                        <Route exact path="/ppm_server2" component={(props) => (<App children={<PPMServer computationServer="memprot" />} {...props} />)} />
                        <Route exact path="/ppm_server2/submitted" component={(props) => (<App children={<PPMServer submitted computationServer="memprot" />} {...props} />)} />
                        <Route exact path="/ppm_server2_cgopm" component={(props) => (<App children={<PPMServer computationServer="cgopm" />} {...props} />)} />
                        <Route exact path="/ppm_server2_cgopm/submitted" component={(props) => (<App children={<PPMServer submitted computationServer="cgopm" />} {...props} />)} />
                        <Route exact path="/ppm_server2_local" component={(props) => (<App children={<PPMServer computationServer="local" />} {...props} />)} />
                        <Route exact path="/ppm_server2_local/submitted" component={(props) => (<App children={<PPMServer submitted computationServer="local" />} {...props} />)} />
                        <Route exact path="/server" component={() => <Redirect to="/ppm_server2" />} />
                        <Route exact path="/server.php" component={() => <Redirect to="/ppm_server2" />} />

                        <Route exact path="/ppm_server3" component={(props) => (<App {...props}><PPMServer3 computationServer="memprot" /></App>)} />
                        <Route exact path="/ppm_server3/submitted" component={(props) => (<App {...props}><PPMServer3 submitted computationServer="memprot" /></App>)} />
                        <Route exact path="/ppm_server3_cgopm" component={(props) => (<App {...props}><PPMServer3 computationServer="cgopm" /></App>)} />
                        <Route exact path="/ppm_server3_cgopm/submitted" component={(props) => (<App {...props}><PPMServer3 submitted computationServer="cgopm" /></App>)} />
                        <Route exact path="/ppm_server3_local" component={(props) => (<App {...props}><PPMServer3 computationServer="local" /></App>)} />
                        <Route exact path="/ppm_server3_local/submitted" component={(props) => (<App {...props}><PPMServer3 submitted computationServer="local" /></App>)} />

                        <Route exact path="/tmpfold_server" component={(props) => (<App children={<TMPfoldServer computationServer="memprot" />} {...props} />)} />
                        <Route exact path="/tmpfold_server/submitted" component={(props) => (<App children={<TMPfoldServer submitted computationServer="memprot" />} {...props} />)} />
                        <Route exact path="/tmpfold_server_cgopm" component={(props) => (<App children={<TMPfoldServer computationServer="cgopm" />} {...props} />)} />
                        <Route exact path="/tmpfold_server_cgopm/submitted" component={(props) => (<App children={<TMPfoldServer submitted computationServer="cgopm" />} {...props} />)} />
                        <Route exact path="/tmpfold_server_local" component={(props) => (<App children={<TMPfoldServer computationServer="local" />} {...props} />)} />
                        <Route exact path="/tmpfold_server_local/submitted" component={(props) => (<App children={<TMPfoldServer submitted computationServer="local" />} {...props} />)} />

                        <Route exact path="/fold" component={(props) => (<App {...props}><Fold /></App>)} />
                        <Route exact path="/fold/submitted" component={(props) => (<App {...props}><Fold submitted /></App>)} />

                        <Route exact path="/fetch_pdb_info" component={(props) => (<App children={<FetchPdbInfo />} {...props} />)} />
                        <Route exact path="/fetch_pdb_info/submitted" component={(props) => (<App children={<FetchPdbInfo submitted />} {...props} />)} />

                        <Route exact path="/opm_update" component={(props) => (<App {...props}><OpmUpdate /></App>)} />
                        <Route exact path="/opm_align" component={(props) => (<App {...props}><OpmAlign/></App>)} />
                        <Route exact path="/csv2sql" component={(props) => (<App {...props}><CSV2SQL/></App>)} />
                        <Route exact path="/viewer" component={(props) => (<App children={<ProteinViewer />} {...props} />)} />

                        <Route exact path="/search" component={(props) => (<App children={<AdvancedSearch />} {...props} />)} />

                        <Route exact path="/protein_types" component={(props) => (<App children={<Classification ctype="protein_types" has_list />} {...props} />)} />
                        <Route exact path="/protein_types/:id" component={(props) => (<App children={<Classification ctype="protein_types" has_list has_table />} {...props} />)} />

                        <Route exact path="/protein_classes" component={(props) => (<App children={<Classification ctype="protein_classes" has_list />} {...props} />)} />
                        <Route exact path="/protein_classes/:id" component={(props) => (<App children={<Classification ctype="protein_classes" has_list has_table />} {...props} />)} />

                        <Route exact path="/protein_superfamilies" component={(props) => (<App children={<Classification ctype="protein_superfamilies" has_list />} {...props} />)} />
                        <Route exact path="/protein_superfamilies/:id" component={(props) => (<App children={<Classification ctype="protein_superfamilies" has_list has_table />} {...props} />)} />

                        <Route exact path="/protein_families" component={(props) => (<App children={<Classification ctype="protein_families" has_list />} {...props} />)} />
                        <Route exact path="/protein_families/:id" component={(props) => (<App children={<Classification ctype="protein_families" has_table />} {...props} />)} />

                        <Route exact path="/protein_species" component={(props) => (<App children={<SpeciesTable />} {...props} />)} />
                        <Route exact path="/protein_species/:id" component={(props) => (<App children={<Classification ctype="protein_species" has_table />} {...props} />)} />

                        <Route exact path="/protein_localizations" component={(props) => (<App children={<Classification ctype="protein_localizations" has_list />} {...props} />)} />
                        <Route exact path="/protein_localizations/:id" component={(props) => (<App children={<Classification ctype="protein_localizations" has_table />} {...props} />)} />

                        <Route exact path="/assembly_superfamilies" component={(props) => (<App children={<Classification ctype="assembly_superfamilies" has_list />} {...props} />)} />
                        <Route exact path="/assembly_superfamilies/:id" component={(props) => (<App children={<Classification ctype="assembly_superfamilies" has_list has_table />} {...props} />)} />

                        <Route exact path="/assembly_families" component={(props) => (<App children={<Classification ctype="assembly_families" has_list />} {...props} />)} />
                        <Route exact path="/assembly_families/:id" component={(props) => (<App children={<Classification ctype="assembly_families" has_table />} {...props} />)} />

                        <Route exact path="/assembly_localizations" component={(props) => (<App children={<Classification ctype="assembly_localizations" has_list />} {...props} />)} />
                        <Route exact path="/assembly_localizations/:id" component={(props) => (<App children={<Classification ctype="assembly_localizations" has_table />} {...props} />)} />

                        <Route exact path="/assemblies" component={(props) => (<App children={<Classification ctype="assemblies" has_table />} {...props} />)} />
                        <Route exact path="/assemblies/:id" component={(props) => (<App children={<Assembly />} {...props} />)} />


                        <Route exact path="/proteins" component={(props) => (<App children={<Classification ctype="proteins" has_table />} {...props} />)} />
                        <Route exact path="/proteins/:id" component={(props) => (<App children={<Protein />} {...props} />)} />
                        <Route exact path="/proteins/:id/lipids/" component={(props) => (<App children={<ProteinLipid />} {...props} />)} />
                        <Route exact path="/proteins/:id/glmol_viewer/:pdbid" component={(props) => (<App children={<ProteinViewer type="complex" viewer="glmol" />} {...props} />)} />
                        <Route exact path="/proteins/:id/litemol_viewer/:pdbid" component={(props) => (<App children={<ProteinViewer type="complex" viewer="litemol" />} {...props} />)} />
                        <Route exact path="/proteins.php" render={(props) => {

                            let params = qs.parse(props.location.search);
                            let url = "/proteins";
                            if (params.search !== undefined && params.search !== null && params.search !== "") {
                                url = appendQuery(url, { search: params.search });
                            }
                            return (
                                <Redirect to={url} />
                            );
                        }} />
                        <Route exact path="/protein.php" render={(props) => {

                            let params = qs.parse(props.location.search);
                            let url = "/proteins";
                            if (params.search !== undefined && params.search !== null && params.search !== "") {
                                url = appendQuery(url, { search: params.search });
                            }
                            else if (params.pdbid !== undefined && params.pdbid !== null && params.pdbid !== "") {
                                url = appendQuery(url, { search: params.pdbid });
                            }
                            else {
                                return (
                                    <Error404 />
                                );
                            }
                            return (
                                <Redirect to={url} />
                            );
                        }} />

                        <Route component={Error404} />
                    </Switch>
                </Router>
            </Provider>
        );
    }
}

export default Main;

ReactDOM.render(<Main />, document.getElementById("react-container"));

// Opt-in to Webpack hot module replacement
if (module.hot) {
    module.hot.accept()
}
