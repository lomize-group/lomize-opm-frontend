import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as PropTypes from 'prop-types';

import axios from 'axios'
import * as appendQuery from 'append-query';

import './react-virtualized.scss';
import { Column, Table as VTable, WindowScroller, AutoSizer, CellMeasurer, CellMeasurerCache } from 'react-virtualized';

import Search from '../search/Search';

import { Assets } from "../../config/Assets";
import ImgWrapper from '../img_wrapper/ImgWrapper';

import './Table.scss';

var CancelToken = axios.CancelToken;

class Table extends React.Component
{
	static propTypes = {
		url: PropTypes.string.isRequired,
		title: PropTypes.string.isRequired,
		columns: PropTypes.array.isRequired,
		title_size: PropTypes.oneOf(["small", "medium", "large"]),
		show_row_count: PropTypes.bool,
		search: PropTypes.string,
		sort: PropTypes.string,
		show_search: PropTypes.bool,
		redirect: PropTypes.bool,
		header_height: PropTypes.number,
		row_height: PropTypes.number
	};

	static defaultProps = {
		title_size: "large",
		show_row_count: true,
		search: "",
		sort: "",
		show_search: true,
		redirect: true,
		header_height: 65,
		row_height: 50
	};

	constructor(props)
	{
		super(props);

		this.state = {
			data: {
				total_objects: 0,
				objects: [],
				sort: props.sort,
				direction: ""
			},
			title: props.title,
			loading: true,
			isPrinting: false,
			search: props.search,
			display: false,
			header_height: props.header_height,
			row_height: props.row_height,
			style: {},
			name: '',
			src: ''
		};

		this.unmounted = false;
		this.requests = 0;

		this.gen_min_widths = () => {
			let width = 0;
			for (let i = 0; i < this.props.columns.length; i++) {
				if (this.props.columns[i].hasOwnProperty('width')){
					width += this.props.columns[i].width;
				}
				else {
					width += this.props.columns[i].minWidth;
				}
			}

			return width;
		}
		this.state.table_width = this.gen_min_widths();

		this.render_cell = (column) => {
			return (props) => {
				let row = props.rowData;
				row.onImgChange = (state) => { 
					this.setState(state); 
				};

				let color_class = (props.rowIndex % 2) === 0 ? "even" : "odd";

				return (
					<div className={color_class}>
						{ column.Cell(row) }
					</div>
				);
			}
		}

		this.render_header = (column) => {
			return () => {

				let sorted_class = "hidden";
				if (this.state.data.sort === column.id) {
					sorted_class = this.state.data.direction.toLowerCase();
				}

				return (
					<div className="header-wrapper">
						{ column.Header() }
						<div className={sorted_class}></div>
					</div>
				)
			}
		}

		this.row_height = () => {
			return this.state.row_height;
		}

		this.render_body = () => {
			let items = [];
			for (var i = 0; i < this.props.columns.length; i++) {
				items.push(
					<Column
						key={i}
				    	headerRenderer={this.render_header(this.props.columns[i])}
				    	cellRenderer={this.render_cell(this.props.columns[i])}
						width={!this.props.columns[i].hasOwnProperty('width') ? this.props.columns[i].minWidth : this.props.columns[i].width}
						flexGrow={!this.props.columns[i].hasOwnProperty('flexGrow') ? 0 : this.props.columns[i].flexGrow}
						flexShrink={!this.props.columns[i].hasOwnProperty('flexGrow') ? 0 : 1}
						minWidth={!this.props.columns[i].hasOwnProperty('minWidth') ? 0 : this.props.columns[i].minWidth}
						dataKey={this.props.columns[i].id}
				    />
				);
			}
			return items;
		}

		this.fetch_data = (params, search = false, replace = false) => {
			params.pageSize = 100;
			let url = appendQuery(this.props.url, params);

			this.requests++;
			var req = this.requests;
			axios.get(url)
		    .then(res => {
		    	if (this.unmounted || req !== this.requests) {
		    		return;
		    	}
		    	if (search && this.props.redirect && res.data.total_objects === 1) {
		    		if (replace) {
		    			this.props.currentUrl.history.replace('/proteins/' + res.data.objects[0].id);
		    			return;
		    		}

		    		this.props.currentUrl.history.push('/proteins/' + res.data.objects[0].id);
		    		return;
		    	}
		    	if (res.data.total_objects <= 100) {
		    		this.setState({
						data: res.data,
						loading: false
					});
		        	return;
		        }
		        this.setState({data: res.data});

		        params.pageSize = res.data.total_objects;
		        url = appendQuery(this.props.url, params);
		        this.requests++;
				req = this.requests;
		        axios.get(url)
			    .then((res) => {
			    	if (this.unmounted || req !== this.requests) {
			    		return;
			    	}
			        this.setState({
			         	data: res.data,
			        	loading: false
			        });
		    	});
		    });
		}

		this.handle_sort = (props) => {
			let direction = 'ASC';
			if (!props.dataKey) return;
			if (props.dataKey === this.state.data.sort && this.state.data.direction === 'ASC'){
				direction = 'DESC';
			}
			let params = {
				search: this.state.search,
				sort: props.dataKey,
				direction: direction
			};
		    this.fetch_data(params);
		    this.setState({
		    	loading: true,
		    	search: this.state.search,
		    	data: {
		    		total_objects: 0,
					objects: [],
					sort: props.dataKey,
					direction: direction
		    	},
		    	title: this.state.search === "" ? this.props.title : "Search Results for \"" + this.state.search + "\""
		    });
		}

		this.handle_search = (value, replace = false) => {
			let params = {
				search: value,
				sort: this.state.data.sort,
				direction: this.state.data.direction
			}; 
		    this.fetch_data(params, true, replace);
		    this.setState({
		    	loading: true,
		    	search: value,
		    	data: {
		    		total_objects: 0,
					objects: [],
					sort: this.state.data.sort,
					direction: this.state.data.direction
		    	},
		    	title: value === "" ? this.props.title : "Search Results for \"" + value + "\""
		    });
		}

		this.handle_no_rows = () => {
			return (
				<div className={this.state.loading ? "hidden" : "no-rows"}>
					no proteins found
				</div>
			);
		}

		if(props.hasOwnProperty('data')){
			this.state.data = props.data;
			this.state.loading = false;
		}
		else {
			this.fetch_data({ search: this.props.search, sort: this.props.sort }, this.props.search !== "", true);
			if (this.props.search !== ""){
				this.state.title = "Search Results for \"" + this.props.search + "\""
			}
		}
		this.updateSize = this.updateSize.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.search !== this.props.search) {
			let params = {
				search: nextProps.search,
			}; 
		    this.fetch_data(params, true, true);
		    this.setState({
		    	loading: true,
		    	search: nextProps.search,
		    	data: {
		    		total_objects: 0,
					objects: [],
					sort: '',
					direction: ''
		    	},
		    	species: [],
		    	title: nextProps.search === "" ? this.props.title : "Search Results for \"" + nextProps.search + "\""
		    });
		}
	}

	componentDidMount() {
		if (this.unmounted === false) {
			window.onbeforeprint = () => {
				this.setState({isPrinting: true})
			}
			window.onafterprint = () => {
				this.setState({isPrinting: false})
			}
		}
	}

	componentWillUnmount() {
		window.onbeforeprint = null;
		window.onafterprint = null;
		this.unmounted = true;
	}

	updateSize() {
		this.windowScrollerRef.updatePosition();
	}

	render()
	{
		// this will render the full table if passed as a prop to a <VTable>.
		// this is important for printing!
		const overscanIndicesGetter = ({cellCount}) => ({
			overscanStartIndex: 0,
			overscanStopIndex: cellCount - 1,
		});
		
		return(
			<div className="light-table">
				<h2 className={"table-header table-header-" + this.props.title_size}>
					{this.state.title}
					{this.props.show_row_count && " (" + this.state.data.total_objects + " entries)"}
				</h2>
				<div className="search-section">
					{this.props.show_search &&
						<Search
							onSearch={this.handle_search}
							placeholder="Search table..."
							initial={this.state.search}
						/>
					}
				</div>
				<div className="virtual-table">
					<WindowScroller ref={ref => this.windowScrollerRef = ref }>
	    				{({ height, isScrolling, onChildScroll, scrollTop }) => (
	    					<AutoSizer disableHeight>
	    						{({ width }) => (
									<VTable
										autoHeight
								        height={height}
								        isScrolling={isScrolling}
								        onScroll={onChildScroll}
								        scrollTop={scrollTop}
									    width={width > this.state.table_width ? width : this.state.table_width}
									    headerHeight={this.state.header_height}
									    rowHeight={this.row_height}
									    rowCount={this.state.data.objects.length}
									    rowGetter={({ index }) => this.state.data.objects[index]}
									    onHeaderClick={this.handle_sort}
										noRowsRenderer={this.handle_no_rows}
										overscanIndicesGetter={this.state.isPrinting ? overscanIndicesGetter : undefined}
									>
									    { this.render_body() }
									</VTable>
								)}
							</AutoSizer>
						)}
	  				</WindowScroller>
	  				<div className={this.state.display ? "floating-image" : "hidden"} style={this.state.style}>
						<div className="image-header">
							<span className="header-text">{this.state.name}</span>
						</div>
						<ImgWrapper src={this.state.src}/>
					</div>
				</div>
				<div className={this.state.loading ? "info-loading" : "hidden"}>
					<img className="loading-image" src={Assets.images.loading}/>
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		currentUrl: state.currentUrl,
	};
}

export default connect(mapStateToProps, null, null, {forwardRef: true})(Table);