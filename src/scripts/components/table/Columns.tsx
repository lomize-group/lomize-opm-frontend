import * as React from "react";
import { Link } from "react-router-dom";

import { TableColumn } from "../../config/Types";

import PDBLink from "./pdb_link/PDBLink.js";

import Assembly from "../assembly/Assembly.js";
import { CType } from "../../config/Classification";

import { Properties as CSSProperties } from "csstype";

class ColumnRepresentation
{
	private columnDefs: ReadonlyArray<TableColumn>;

	constructor(columnDefs: ReadonlyArray<TableColumn>) {
		this.columnDefs = columnDefs;
	}

	get columns() {
		return this.columnDefs;
	}

	filterByPage(ctype: CType) {
		return new ColumnRepresentation(this.columnDefs.filter(col => !col.cpage || col.cpage.includes(ctype)));
	}

	without(index: number) {
		let arr: TableColumn[] = [];
		for (let i = 0; i < this.columnDefs.length; ++i) {
			if (i !== index) {
				arr.push(this.columnDefs[i]);
			}
		}
		return new ColumnRepresentation(arr);
	}
}

export function create_header_cell(cellTitle: string, cellBody: React.ReactNode) {
	return () => <div className="header-cell" title={cellTitle}>{cellBody}</div>;
}

const protein_column_defs: ReadonlyArray<TableColumn> = [
	{
		Header: create_header_cell("Family", <div>Family</div>),
		id: "family_name_cache",
		flexGrow: 3,
		minWidth: 150,
		Cell: (row: any) => <div className="cell" title={row.family_name_cache}>
						<div>
							<Link to={"/protein_families/" + row.family_id}>
								{
									row.family_name_cache.length > 40 ? 
									row.family_name_cache.slice(0, 40).trim() + '...' :
									row.family_name_cache
								}
							</Link>
						</div>
					</div>
	},
	{
		Header: create_header_cell("Protein Name", <div>Protein Name</div>),
		id: "name",
		flexGrow: 3,
		minWidth: 150,
		Cell: (row: any) => <div className="cell">
						<div>
							<PDBLink 
								id={row.id} 
								name={row.name} 
								pdb={row.pdbid}
								cell={
									row.name.length > 40 ? 
									row.name.slice(0, 40).trim() + '...' :
									row.name
								}
								onImgChange={row.onImgChange}
							/>	
						</div>
					</div>

	},
	{
		Header: create_header_cell("PDB ID", <div>PDB<br/>ID</div>),
		width: 55,
		id: "pdbid",
		Cell: (row: any) => <div className="cell">
								<div>
									<PDBLink 
										id={row.id} 
										name={row.name} 
										pdb={row.pdbid}
										cell={row.pdbid}
										onImgChange={row.onImgChange}
									/>
								</div>
							</div>
	},
	{
		Header: create_header_cell("Resolution", <div>Resolution</div>),
		width: 75,
		id: 'resolution',
		Cell: (row: any) => <div className="cell">
								<div>
									{ row.resolution }
								</div>
							</div>
	},
	{
		Header: create_header_cell("Species", <div>Species</div>),
		id: 'species_name_cache',
		flexGrow: 1,
		minWidth: 115,
		Cell: (row: any) => <div className="cell" title={row.species_name_cache}>
								<div>
									<Link to={"/protein_species/" + row.species_id}>
										<i>{row.species_name_cache}</i>
									</Link>
								</div>
							</div>
	},
	{
		Header: create_header_cell("Localization", <div>Localization</div>),
		id: 'membrane_name_cache',
		flexGrow: 1,
		minWidth: 83,
		Cell: (row: any) => <div className="cell" title={row.membrane_name_cache}>
								<div>
									<Link to={"/protein_localizations/" + row.membrane_id}>
										{row.membrane_name_cache}
									</Link>
								</div>
							</div>
	},
	{
		Header: create_header_cell("Number of Subunits", <div>Num. TM Subunits</div>),
		width: 65,
		id: 'structure_subunits_count',
		Cell: (row: any) => <div className="cell">
								<div>{row.structure_subunits_count}</div>
							</div>
	},
	{
		Header: create_header_cell("Number TM. Secondary Structures", <div>Num. TM. Sec. Structs.</div>),
		width: 75,
		id: 'subunit_segments',
		Cell: (row: any) => <div className="cell">
								<div>{row.subunit_segments}</div>
							</div>
	},
	{
		Header: create_header_cell("Hydrophobic Thickness or Depth (Å)", <div>Hydrophobic Thickness or Depth (Å)</div>),
		width: 90,
		id: 'thickness',
		Cell: (row: any) => <div className="cell">
						<div>
							{ row.thickness.toFixed(1) }
						</div>
					</div>
	},
	{
		Header: create_header_cell("Tilt Angle (°)", <div>Tilt Angle (°)</div>),
		width: 65,
		id: 'tilt',
		Cell: (row: any) => <div className="cell">
						<div>
							{ row.tilt }
						</div>
					</div>
	},
	{
		Header: create_header_cell("Radius of curvature (Å)", <div>Radius of curvature (Å)</div>),
		width: 65,
		id: 'tau',
		Cell: (row: any) => <div className="cell">
						<div>
							{ row.tau }
						</div>
					</div>
	},
	{
		Header: create_header_cell("ΔG (transfer) (kcal/mol)", <div><span>ΔG<sub>transfer</sub><br/>(kcal/mol)</span></div>),
		width: 75,
		id: 'gibbs',
		Cell: (row: any) => <div className="cell">
								<div>
									{ row.gibbs.toFixed(1) }
								</div>
							</div>
	}
];

const assembly_column_defs: ReadonlyArray<TableColumn> = [
	{
		Header: () => <div className="header-cell" title="Family">
						<div>Family</div>
					</div>,
		id: "family_name_cache",
		cpage: ["assemblies"],
		flexGrow: 3,
		minWidth: 130,
		Cell: (row: any) => <div className="cell" title={row.family_name_cache}>
								<div>
									<Link to={"/protein_families/" + row.family_id}>
										{
											row.family_name_cache.length > 40 ? 
											row.family_name_cache.slice(0, 40).trim() + '...' :
											row.family_name_cache
										}
									</Link>
								</div>
							</div>
	},
	{
		Header: create_header_cell("Protein Name", <div>Protein Name</div>),
		id: "primary_structure_name_cache",
		flexGrow: 3,
		minWidth: 120,
		Cell: (row: any) => <div className="cell" title={row.primary_structure_name_cache}>
								<div>
									<PDBLink 
										id={row.primary_structure_id} 
										name={row.primary_structure_name_cache} 
										pdb={row.pdbid}
										cell={row.name}
										onImgChange={row.onImgChange}
									/>	
								</div>
							</div>
	},
	{
		Header: create_header_cell("Sub-unit", <div>Sub-<br/>unit</div>),
		id: "protein_letter",
		width: 40,
		Cell: (row: any) => <div className="cell" title={row.protein_letter}>
								<div>
									<Link to={"/assemblies/" + row.id}>
										{row.protein_letter}
									</Link>
								</div>
							</div>
	},
	{
		Header: create_header_cell("PDB ID", <div>PDB<br/>ID</div>),
		width: 50,
		id: "pdbid",
		Cell: (row: any) => <div className="cell">
								<div>
									<PDBLink 
										id={row.primary_structure_id} 
										name={row.primary_structure_name_cache} 
										pdb={row.pdbid}
										cell={row.pdbid}
										onImgChange={row.onImgChange}
									/>
								</div>
							</div>
	},
	{
		Header: create_header_cell("# TM Helices", <div># TM<br/>Helices</div>),
		width: 52,
		id: "transmembrane_alpha_helix_count",
		cpage: ["assemblies"],
		Cell: (row: any) => <div className="cell">
								<div>
									{row.transmembrane_alpha_helix_count}
								</div>
							</div>
	},
	{
		Header: create_header_cell("Resolution", <div>Resolution</div>),
		width: 72,
		id: "resolution",
		Cell: (row: any) => <div className="cell">
						<div>
							{row.resolution}
						</div>
					</div>
	},
	{
		Header: create_header_cell("Folding Equation", <div>Folding Equation</div>),
		id: "equation",
		cpage: ["assemblies"],
		flexGrow: 3,
		minWidth: 150,
		Cell: (row: any) => <div className="cell">
						<div>
							{row.equation}
						</div>
					</div>
	},
	{
		Header: create_header_cell("TMH Pairwise Energies", <div>TMH Pairwise Energies</div>),
		id: "helix_interaction",
		cpage: ["assembly_superfamilies", "assembly_families", "assembly_localizations"],
		flexGrow: 3,
		minWidth: 300,
		Cell: (row: any) => {
			const interactions = Assembly.parseHelixInteractions(row.helix_interaction);
			const borderStyle: CSSProperties = {
				borderLeft: "1px solid lightgray",
				padding: "0 3px",
				textAlign: "center"
			};

			let helices = [<td key={-1}><b>TMH&nbsp;</b></td>];
			let energies = [<td key={-1}><b>Energy&nbsp;</b></td>];
			for (let i = 0; i < interactions.length; ++i) {
				helices.push(
					<td key={i} style={borderStyle}>{interactions[i].helix1}, {interactions[i].helix2}</td>
				);
				energies.push(
					<td key={i} style={borderStyle}>{interactions[i].energy}</td>
				);
			}
			return (
				<div className="cell">
					<table style={{marginLeft: "5px"}}>
						<tbody>
							<tr>{helices}</tr>
							<tr style={{borderTop: "1px solid lightgray"}}>{energies}</tr>
						</tbody>
					</table>
				</div>
			);
		}
	},
	{
		Header: create_header_cell("Assembly Page", <div>Assembly Page</div>),
		minWidth: 200,
		id: "",
		Cell: (row: any) => {
			return (
				<div className="cell">
					<div>
						<Link to={"/assemblies/" + row.id}>
							View Assembly Page
						</Link>
					</div>
				</div>
			);
		}
	}
];

const lipid_column_defs: ReadonlyArray<TableColumn> = [
	{
		Header: create_header_cell("Lipid", <div>Lipid</div>),
		minWidth: 100,
		flexGrow: 2,
		id: "lipid",
		Cell: (row: any) => <div className="cell">
						<div>
							{row.lipid}
						</div>
					</div>
	},
	{
		Header: create_header_cell("Headgroup", <div>Headgroup</div>),
		minWidth: 50,
		flexGrow: 1,
		id: "headgroup",
		Cell: (row: any) => <div className="cell">
						<div>
							{row.headgroup}
						</div>
					</div>
	},
	{
		Header: create_header_cell("Acyl1", <div>Acyl Tail <i>sn</i>-1</div>),
		minWidth: 80,
		flexGrow: 1,
		id: "acyl1",
		Cell: (row: any) => <div className="cell">
						<div>
							{row.acyl1}
						</div>
					</div>
	},
	{
		Header: create_header_cell("Acyl2", <div>Acyl Tail <i>sn</i>-2</div>),
		minWidth: 80,
		flexGrow: 1,
		id: "acyl2",
		Cell: (row: any) => <div className="cell">
						<div>
							{row.acyl2}
						</div>
					</div>
	},
	{
		Header: create_header_cell("Num. Lipids Per Leaflet", <div># of lipids per leaflet</div>),
		minWidth: 50,
		flexGrow: 1,
		id: "number_per_leaflet",
		Cell: (row: any) => <div className="cell">
						<div>
							{row.number_per_leaflet}
						</div>
					</div>
	},
	{
		Header: create_header_cell("Percentage", <div>Lipids %</div>),
		minWidth: 50,
		flexGrow: 1,
		id: "percentage",
		Cell: (row: any) => <div className="cell">
						<div>
							{row.percentage}
						</div>
					</div>
	}
];

const Columns = {
	proteins: new ColumnRepresentation(protein_column_defs),
	assemblies: new ColumnRepresentation(assembly_column_defs),
	lipids: new ColumnRepresentation(lipid_column_defs)
} as const;

export default Columns;
