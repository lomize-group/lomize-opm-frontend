import * as React from "react";
import { Assets } from "../../config/Assets";

import "./ImgWrapper.scss";

interface OptionalImgWrapperProps {
	className: string,
	fallback: string
};

export interface ImgWrapperProps extends OptionalImgWrapperProps {
	src: string
};

interface ImgWrapperState {
	src: string,
	loaded: boolean,
	found: boolean
};

export default class ImgWrapper extends React.Component<ImgWrapperProps, ImgWrapperState>
{
	static defaultProps: OptionalImgWrapperProps = {
		className: "",
		fallback: ""
	};

	constructor(props: ImgWrapperProps)
	{
		super(props);
		this.state = {
			src: props.src,
			loaded: false,
			found: true
		};
	}

	handleLoad = (): void => {
		this.setState({ loaded: true });
	}

	handleError = (e: React.SyntheticEvent<HTMLImageElement, Event>) => {
		(e.target as HTMLImageElement).src = this.props.fallback === "" ? Assets.images.no_image : this.props.fallback;
	}

	static getDerivedStateFromProps(props: ImgWrapperProps, state: ImgWrapperState)
	{
		if (props.src !== state.src) {
			return {
				src: props.src,
				loaded: false,
				found: true,
			};
		}
		return null;
	}

	render(): React.ReactElement
	{	
		return(
			<span className="image-wrapper">
				<img 
					className={this.state.loaded ? this.props.className : "hidden"}
					src={this.state.src}
					onLoad={this.handleLoad}
					onError={this.handleError}
				/>
				<img
					className={this.state.loaded ? "hidden" : this.props.className}
					src={Assets.images.loading}
				/>
			</span>
		);
	}
}
