import * as React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import axios from "axios";

import PDB from "./pdb/PDB";
import Info from "../info/Info.js";

import { Assets } from "../../config/Assets";
import { Urls } from "../../config/Urls";
import { Membranome } from "../../config/Membranome";
import Classification from "../../config/Classification";

import ExternalLink from "../external_link/ExternalLink";
import ImgWrapper from "../img_wrapper/ImgWrapper";

import "./Protein.scss";

class Protein extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            has_data: false,
            id: props.currentUrl.params.id,
            membranome_links: undefined,
        };

        axios
            .get(
                Urls.api_url(
                    Classification.definitions.proteins.api.route + "/" + props.currentUrl.params.id
                )
            )
            .then((res) => {
                let newstate = res.data;

                const in_or_out = res.data.topology_show_in
                    ? res.data.membrane.topology_in
                    : res.data.membrane.topology_out;
                if (res.data.topology_subunit === "") {
                    newstate.topology = in_or_out;
                } else {
                    newstate.topology =
                        "subunit " + res.data.topology_subunit + " (N terminus " + in_or_out + ")";
                }

                newstate.header = res.data.name;
                newstate.image_pdbid = res.data.pdbid;
                newstate.has_data = true;
                newstate.info = {};
                newstate.use_external_pdb = newstate.resolution !== "model";

                function createInfo(cur_type, cur_obj) {
                    const cur_type_def = Classification.definitions[cur_type];
                    if (cur_type_def.hasOwnProperty("parents")) {
                        for (let i = 0; i < cur_type_def.parents.length; i++) {
                            const parent = cur_type_def.parents[i];
                            const parent_obj =
                                cur_obj[Classification.definitions[parent].api.accessor.object];
                            newstate.info[parent] = {
                                id: parent_obj.id,
                                name: parent_obj.name,
                                subclasses: parent_obj[cur_type_def.api.accessor.count],
                                pfam: parent_obj.pfam,
                                interpro: parent_obj.interpro,
                                tcdb: parent_obj.tcdb,
                            };
                            // recurse with cur_type = parent, cur_obj = parent_obj
                            createInfo(parent, parent_obj);
                        }
                    }
                }
                createInfo("proteins", res.data);
                this.setState(newstate);

                // get links to related membranome data
                axios
                    .get(
                        Membranome.urls.api_url(
                            Membranome.definitions.opm_links.api.route + "/" + newstate.pdbid
                        )
                    )
                    .then((membranome_res) =>
                        this.setState({ membranome_links: membranome_res.data })
                    );
            }).catch((error) => {
                console.log(error);
                this.setState({ error });
            });
    }

    renderInfo() {
        if (this.state.hasOwnProperty("info")) {
            return (
                <div>
                    <Info {...this.state.info} />
                </div>
            );
        }
        return null;
    }

    renderTableRow(right, left) {
        return (
            <tr>
                <td className="right">
                    <b>{right}</b>
                </td>
                <td className="left">{left}</td>
            </tr>
        );
    }

    renderLinks(pdbid) {
        const { proteins } = Classification.definitions;
        return (
            <span>
                <a className="cursor-hand external" target="_blank" href={proteins.pdbsum(pdbid)}>
                    PDB Sum
                </a>
                ,{" "}
                <a className="cursor-hand external" target="_blank" href={proteins.pdb(pdbid)}>
                    PDB
                </a>
                ,{" "}
                <a className="cursor-hand external" target="_blank" href={proteins.msd(pdbid)}>
                    MSD
                </a>
                ,{" "}
                <a className="cursor-hand external" target="_blank" href={proteins.mmdb(pdbid)}>
                    MMDB
                </a>
                {this.state.info.protein_classes.id === 1 && (
                    <span>
                        ,{" "}
                        <a
                            className="cursor-hand external"
                            target="_blank"
                            href={proteins.encompass(pdbid)}
                        >
                            Encompass
                        </a>
                    </span>
                )}
            </span>
        );
    }

    changeImage(new_pdbid) {
        this.setState({ image_pdbid: new_pdbid });
    }

    renderSimplePDBLink(pdbid, showComma, key, resolution = undefined) {
        let after = "";
        if (resolution) {
            after += ` (${resolution})`;
        }
        if (showComma) {
            after += ", ";
        }

        if (this.state.image_pdbid == pdbid) {
            return (
                <span key={key}>
                    <strong>{pdbid}</strong>
                    {after}
                </span>
            );
        } else {
            return (
                <span className="cursor-hand" key={key}>
                    <a onClick={() => this.changeImage(pdbid)}>{pdbid}</a>
                    {after}
                </span>
            );
        }
    }

    renderOtherPDBs() {
        if (this.state.secondary_representations.length === 0) {
            return <span>none</span>;
        }
        let items = [];
        for (let i = 0; i < this.state.secondary_representations.length; i++) {
            const secRep = this.state.secondary_representations[i];
            let showComma = i !== this.state.secondary_representations.length - 1;
            items.push(this.renderSimplePDBLink(secRep.pdbid, showComma, i, secRep.resolution));
        }
        return items;
    }

    renderDescription() {
        if (this.state.description) {
            return (
                <p key="description">
                    <span className="green italic">Description: </span>
                    {this.state.description}
                </p>
            );
        }
    }

    renderComments() {
        if (this.state.comments) {
            return (
                <p key="comments">
                    <span className="green italic">Comments: </span>
                    {this.state.comments}
                </p>
            );
        }
    }

    renderVerification() {
        if (this.state.verification) {
            return (
                <div className="info-table full">
                    <table>
                        <thead>
                            <tr>
                                <th colSpan="2">
                                    <span className="green italic">Verification:</span>{" "}
                                    {this.state.name}{" "}
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colSpan="2" className="small-text">
                                    {this.state.verification}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            );
        }
    }

    renderSubunit(key, data) {
        return (
            <tr key={key}>
                <td colSpan="2" className="small-text break">
                    <b>{data.protein_letter}</b> - Tilt: {data.tilt} - TM segments: {data.segment}
                </td>
                <td colSpan="2" className="bold center">
                    {data.assembly_id && (
                        <Link to={"/assemblies/" + data.assembly_id}>view assembly page</Link>
                    )}
                </td>
            </tr>
        );
    }

    renderSubunits() {
        if (this.state.subunits.length == 0) {
            return;
        }
        let subunits = [];
        for (let i = 0; i < this.state.subunits.length; i++) {
            let struct = this.state.subunits[i];
            subunits.push(this.renderSubunit(i, struct));
        }
        return (
            <div className="info-table full">
                <table>
                    <thead>
                        <tr>
                            <th colSpan="4">
                                <span className="green italic">Subunits: </span>{" "}
                                {this.state.subunits.length}
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {subunits}
                    </tbody>
                </table>
            </div>
        );
    }

    renderCitations() {
        if (this.state.citations.length == 0) {
            return;
        }
        let citations = [];
        for (let i = 0; i < this.state.citations.length; i++) {
            const citation = this.state.citations[i];
            citations.push(
                <tr key={i}>
                    <td colSpan="2" className="small-text">
                        {citation.maintext}{" "}
                        <ExternalLink href={Urls.pubmed(citation.pmid)}>PubMed</ExternalLink>
                    </td>
                </tr>
            );
        }
        return (
            <div className="info-table full">
                <table>
                    <thead>
                        <tr>
                            <th colSpan="2">
                                <span className="green italic"> References</span>:{" "}
                                {this.state.citations.length}
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {citations}
                    </tbody>
                </table>
            </div>
        );
    }

    renderMembranome() {
        if (this.state.membranome_links === undefined) {
            return (
                <div className="info-loading">
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            );
        }
        if (this.state.membranome_links.length === 0) {
            return <span>none</span>;
        }

        let items = [];
        for (let i = 0; i < this.state.membranome_links.length; ++i) {
            const item = this.state.membranome_links[i];
            items.push(
                <span key={i}>
                    {i > 0 && ", "}
                    <a target="_blank" href={Membranome.urls.protein_link(item.protein_id)}>
                        {item.protein_pdbid}
                    </a>
                    {item.subunit_name && " (" + item.subunit_name + ")"}
                </span>
            );
        }
        return items;
    }

    renderUniprot() {
        return this.state.uniprotcodes.map((item, idx) => (
            <span key={idx}>
                {idx > 0 && ", "}
                <a target="_blank" href={Urls.uniprot(item)}>
                    {item}
                </a>
            </span>
        ));
    }

    render() {
        if (!this.state.has_data) {
            if (this.state.error !== undefined) {
                return (
                    <div className="protein">
                        <div className="page-header">
                            Error
                        </div>
                        <div className="protein-content">
                            {`There was an error getting ${this.state.error.config.url}`}
                        </div>
                    </div>
                );
            }

            return (
                <div className="info-loading">
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            );
        }
        return (
            <div className="protein">
                <div className="page-header">
                    {this.state.pdbid} » {this.state.name}
                </div>
                <div className="protein-content">
                    <div className="top-grid">
                        <div>
                            <div className="protein-info">{this.renderInfo()}</div>
                            <div className="info-table">
                                <table>
                                    <thead>
                                        <tr>
                                            <th colSpan="2">
                                                {this.state.pdbid} {">>"} {this.state.name}
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.renderTableRow(
                                            "Hydrophobic Thickness or Depth",
                                            this.state.thickness.toFixed(1) +
                                                " Å",
                                        )}
                                        {this.renderTableRow(
                                            "Tilt Angle",
                                            this.state.tilt + "°",
                                        )}
                                        {this.state.tau && this.renderTableRow(
                                            "Radius of curvature",
                                            this.state.tau + " Å",
                                        )}
                                        {this.renderTableRow(
                                            <span>
                                                ΔG<sub>transfer</sub>
                                            </span>,
                                            this.state.gibbs.toFixed(1) + " kcal/mol",
                                        )}
                                        {this.state.use_external_pdb &&
                                            this.renderTableRow(
                                                "Links to " + this.state.pdbid,
                                                this.renderLinks(this.state.pdbid),
                                            )}
                                        {this.renderTableRow("Topology", this.state.topology)}
                                        {this.renderTableRow("Resolution", this.state.resolution)}
                                        {this.renderTableRow(
                                            "Primary PDB represention",
                                            this.renderSimplePDBLink(this.state.pdbid, false),
                                        )}
                                        {this.renderTableRow(
                                            "Other PDB entries representing this structure",
                                            this.renderOtherPDBs(),
                                        )}
                                        {this.renderTableRow(
                                            "Number of TM secondary structures",
                                            this.state.subunit_segments,
                                        )}
                                        {this.renderTableRow("Membranome", this.renderMembranome())}
                                        {this.renderTableRow("Uniprot", this.renderUniprot())}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div>
                            <div className="random-protein">
                                <PDB
                                    pdbid={this.state.image_pdbid}
                                    id={this.state.id}
                                    name={this.state.name}
                                    show_links={this.state.use_external_pdb}
                                />
                                <div className="topology-section">
                                    <div className="topology-header">
                                        Topology in {this.state.membrane.name}
                                    </div>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td rowSpan="2">
                                                    <ImgWrapper
                                                        className="img-style"
                                                        src={Assets.images.topology}
                                                    />
                                                </td>
                                                <td className="topology-info out">
                                                    {this.state.membrane.topology_out}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td className="topology-info in">
                                                    {this.state.membrane.topology_in}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div className="jmol-viewer">
                                    3D view in{" "}
                                    <Link
                                        to={
                                            "/proteins/" +
                                            this.state.id +
                                            "/glmol_viewer/" +
                                            this.state.image_pdbid
                                        }
                                        target="_blank"
                                        className="link"
                                    >
                                        GLMol
                                    </Link>
                                    {" | "}
                                    <Link
                                        to={
                                            "/proteins/" +
                                            this.state.id +
                                            "/litemol_viewer/" +
                                            this.state.image_pdbid
                                        }
                                        target="_blank"
                                        className="link"
                                    >
                                        LiteMol
                                    </Link>
                                    {" | "}
                                    <ExternalLink href={Urls.jmol(this.state.image_pdbid)}>
                                        Jmol
                                    </ExternalLink>
                                    {" | "}
                                    <ExternalLink href={Urls.icn3d(this.state.image_pdbid)}>
                                        iCn3D
                                    </ExternalLink>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.renderDescription()}
                    {this.renderComments()}
                    {this.renderVerification()}
                    {this.renderSubunits()}
                    {this.renderCitations()}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUrl: state.currentUrl,
    };
}

export default connect(mapStateToProps)(Protein);
