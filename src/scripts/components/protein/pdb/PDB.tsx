import * as React from "react";
import { Link } from "react-router-dom";

import axios from "axios";

import Classification from "../../../config/Classification";

import ExternalLink from "../../external_link/ExternalLink";
import ImgWrapper from "../../img_wrapper/ImgWrapper";

import { Assets } from "../../../config/Assets";
import { Urls } from "../../../config/Urls";

import "./PDB.scss";

interface OptionalPDBProps {
    showLinks: boolean;
}

export interface PDBProps extends OptionalPDBProps {
    pdbid: string;
    id: number;
    name: string;
}

interface PDBState {
    lipidLoading: boolean;
    hasLipid: boolean;
}

export default class PDB extends React.Component<PDBProps, PDBState> {
    static defaultProps: OptionalPDBProps = {
        showLinks: true,
    };

    state: PDBState = {
        lipidLoading: true,
        hasLipid: false,
    };

    constructor(props: PDBProps) {
        super(props);

        axios
            .get(Urls.pdb_lipid_file(props.pdbid))
            .then((res) => {
                this.setState({
                    hasLipid: true,
                    lipidLoading: false,
                });
            })
            .catch((err) => {
                this.setState({
                    hasLipid: false,
                    lipidLoading: false,
                });
            });
    }

    renderLipidLink() {
        if (this.state.lipidLoading) {
            return (
                <div className="info-loading">
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            );
        }

        if (!this.state.hasLipid) {
            return null;
        }

        return (
            <Link to={`/proteins/${this.props.id}/lipids`} target="_blank" className="link">
                Model with explicit lipids
            </Link>
        );
    }

    render() {
        const { proteins } = Classification.definitions;

        return (
            <div className="pdb">
                <a href={proteins.pdb_image(this.props.pdbid)} target="_blank">
                    <ImgWrapper className="img-style" src={proteins.pdb_image(this.props.pdbid)} />
                </a>
                <hr className="divider" />
                <div className="protein-link">
                    <a href={proteins.pdb_file(this.props.pdbid)} download>
                        Download OPM File: {this.props.pdbid}.pdb
                    </a>
                </div>
                <div className="pdb-links">{this.renderLipidLink()}</div>
                {this.props.showLinks && (
                    <div className="pdb-links">
                        <ExternalLink href={proteins.pdbsum(this.props.pdbid)}>
                            PDB Sum
                        </ExternalLink>
                        , <ExternalLink href={proteins.pdb(this.props.pdbid)}>PDB</ExternalLink>
                    </div>
                )}
            </div>
        );
    }
}
