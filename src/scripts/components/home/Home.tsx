import * as appendQuery from "append-query";
import Axios from 'axios';
import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Link } from 'react-router-dom';
import { Assets } from "../../config/Assets";
import Classification from "../../config/Classification";
import { GenericObj } from '../../config/Types';
import { Urls } from "../../config/Urls";
import { RootState } from '../../store';
import ExternalLink from "../external_link/ExternalLink";
import ImgWrapper from '../img_wrapper/ImgWrapper';
import PubMedLink from "../pubmed_link/PubMedLink";
import './Home.scss';

function makeTypeStat(key: number, type: any) {
	let classStats = []
	for (var i = 0; i < type.classes.length; i++) {
		const classtype = type.classes[i].table
		classStats.push(
			<div key={i} className="classtype-stat">
				<p><span className="green italic">Class:</span> <Link to={"/protein_classes/" + classtype.id}>{classtype.name}</Link> - <span className="bold">({classtype.primary_structures_count}/{classtype.primary_structures_count + classtype.secondary_representations_count})</span></p>
			</div>
		);
	}
	return (
		<div key={key} className="type-stat">
			<p><span className="green italic">Type:</span> <Link to={"/protein_types/" + type.id}>{type.name}</Link> - <span><strong>({type.primary_structures_count}/{type.primary_structures_count + type.secondary_representations_count})</strong></span></p>
			<div className="classtype-stat-holder">
				{classStats}
			</div>
		</div>
	);
}

function mapStateToProps(state: RootState) {
	return {
		stats: state.stats
	};
}

const connector = connect(mapStateToProps);

export type HomeState = {
	randProtein?: GenericObj,
	lastUpdated?: string
}
export type HomeProps = ConnectedProps<typeof connector>;

class Home extends React.Component<HomeProps, HomeState> {

	state: HomeState = {
		randProtein: undefined,
		lastUpdated: undefined
	};

	fetchRandomProtein = async () => {
		const { data } = await Axios.get(Urls.api_base + Classification.definitions.proteins.api.route + "/random");
		this.setState({ randProtein: data });
	}

	fetchLastUpdated = async () => {
		const key = "lastUpdated";
		const fetchUrl = appendQuery(Urls.api_base + "/constants", { names: key });
		const { data } = await Axios.get(fetchUrl);
		this.setState({ lastUpdated: data.objects[key].value });
	}

	componentDidMount() {
		this.fetchRandomProtein();
		this.fetchLastUpdated();
	}

	renderStructureStats() {
		const { stats } = this.props;
		if (stats === null) {
			return (
				<div className="info-loading">
					<img className="loading-image" src={Assets.images.loading} />
				</div>
			);
		}

		let typeStats = []
		for (let i = 0; i < stats.protein_structure_stats.length; i++) {
			let type = stats.protein_structure_stats[i].table;
			typeStats.push(makeTypeStat(i, type));
		}

		return typeStats;
	}

	renderImage() {
		const protein = this.state.randProtein;

		if (protein === undefined) {
			return null;
		}

		return (
			<div>
				<Link to={"/proteins/" + protein.id}>
					<ImgWrapper className="img-style" src={Classification.definitions.proteins.pdb_image(protein.pdbid)} />
				</Link>
				<div className="protein-link">
					<Link to={"/proteins/" + protein.id}>{protein.name} pdb-{protein.pdbid}</Link>
				</div>
				<hr />
				<div className="protein-link">
					<a href={Classification.definitions.proteins.pdb_file(protein.pdbid)} download>Download File: {protein.pdbid}.pdb</a>
				</div>
			</div>
		);
	}

	render() {
		return (
			<div className="home">
				<div>
					<h1>Orientations of Proteins in Membranes (OPM) database</h1>
					<div className="random-protein">
						{this.renderImage()}
					</div>
					<div className="home-text">

						<div className="indented">
							<p>OPM provides spatial arrangements of membrane proteins with respect to the hydrocarbon core of the lipid bilayer.</p>
							<p>OPM includes all unique experimental structures of transmembrane proteins and some peripheral proteins and membrane-active peptides (<Link to="/about#features">Features</Link>).</p>
							<p>Each protein is positioned in a lipid bilayer of adjustable thickness and curvature by minimizing its transfer energy from water to the membrane (<Link to="/about#methods_and_definitions">Methods</Link>).</p>
							<p>OPM provides structural classification and sorting according to different criteria (<Link to="/about#classification">Classification</Link>).</p>
							<p>
								<p>
									<b>
										OPM also provides a few preliminary results of <Link to="/about#tm_helix_assembly">our computational analysis</Link> of transmembrane &alpha;-helix
									association in experimental structures of selected polytopic proteins (Assembly pages).
								</b>
								</p>
								<b>
									<i>For more information on single-spanning transmembrane proteins please see our <a href="http://membranome.org/">Membranome database</a></i>
								</b>
							</p>
						</div>
						<div>
							<p className="p-header"><b>In citing the Orientations of Proteins in Membranes (OPM) database please refer to</b></p>
							<p>Lomize MA, Pogozheva ID, Joo H., Mosberg HI, Lomize AL (2012) OPM database and PPM web server: resources for positioning of proteins in membranes.
								<i> Nucleic Acids Res.</i>
								<b> 40 </b>
								(Database issue), D370-D376.
								<a href={Urls.pdf('gkr703')}> PDF </a>
								<PubMedLink id="21890895" />
							</p>
							<p className="p-header"><b>For an explanation of our method please refer to</b></p>
							<p>Lomize AL, Pogozheva ID, Lomize MA, Mosberg HI (2006) Positioning of proteins in membranes: A computational approach.
								<i> Protein Science.</i>
								<b> 15</b>
								, 1318-1333.
								<a href={Urls.pdf('PPM')}> PDF </a>
								<PubMedLink id="16731967" />
							</p>
							<p>Lomize AL, Pogozheva ID, Mosberg HI (2011) Anisotropic solvent model of the lipid bilayer. 2. Energetics of insertion of small molecules, peptides, and proteins in membranes.
								<i> J Chem Inf Model.</i>
								<b> 51</b>
								, 930-946.
								<a href={Urls.pdf('PPM2')}> PDF </a>
								<a href={Urls.pdf('PPM2s')}> PDF (supplementary) </a>
								<PubMedLink id="21438606" />
							</p>
							<p className="p-header"><b>For a new version of our method please refer to</b></p>
							<p>Lomize AL, Todd SC, Pogozheva ID. (2022) Spatial arrangement of proteins in planar and curved membranes by PPM 3.0.
								<i> Protein Sci.</i>
								<b> 31</b>
								: 930-946.
								<a href={Urls.pdf('PPM3')}> PDF </a>
								<PubMedLink id="34716622" />
							</p>
							<p>See also <a href="https://biomembhub.org">BioMembHub</a> project that includes several
							 databases and webservers for analyzing proteins, peptides, and small molecules in biomembranes.</p>

							<div className="questions-box structure-stats">
								<div className="box-header">
									<b className="header-text">Structure Statistics (distinct protein structures/PDB entries)</b>
								</div>
								<div className="box-body">
									{this.renderStructureStats()}
								</div>
							</div>

							<div className="questions-box">
								<div className="box-header">
									<b className="header-text">Questions</b>
								</div>
								<div className="box-body">
									<p><b>1. Calculate orientation for unreleased structure or membrane protein was not in OPM?</b></p>
									<p>Please use our <Link to="/ppm_server2">web server</Link> or we can process your coordinates through email (<a href="mailto:lomizeal@gmail.com">lomizeal@gmail.com</a>). All information is kept confidential. Orientation of membrane protein cannot be predicted if all its membrane-anchoring elements are missing or disordered.</p>
									<p><b>2. Errors in orientation or experimental verification description?</b></p>
									<p>Please <Link to="/contact">contact us</Link>.</p>
								</div>
							</div>
							<div>
								<p>Last updated {this.state.lastUpdated || null}.</p>
							</div>
							<div>
								<p><ExternalLink href="https://www.nsf.gov/awardsearch/showAward?AWD_ID=2010851">OPM is funded by the National Science Foundation (NSF)</ExternalLink></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default connector(Home);
