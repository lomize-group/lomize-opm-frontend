import * as React from 'react';
import { Link } from 'react-router-dom';
import * as appendQuery from 'append-query';

import ListItem from '../list_item/ListItem.js';
import * as PropTypes from 'prop-types';
import axios from 'axios';
import Search from '../search/Search';

import { Assets } from "../../config/Assets";
import { Urls } from "../../config/Urls";
import Classification from "../../config/Classification";

import './List.scss';


// List usage example:
//	<List
//		url={'http://localhost:3000/superfamilies'}
//		type='superfamilies'
//		sub_type='families'
//	/>

class List extends React.Component
{
	static propTypes = {
		url: PropTypes.string.isRequired,
		ctype: PropTypes.string.isRequired,
		sub_type: PropTypes.string.isRequired,
		searchable: PropTypes.bool,
		expandable_items:  PropTypes.bool,
		has_table: PropTypes.bool,
		onResize: PropTypes.func
	};

	static defaultProps = {
		searchable: true,
		has_table: false,
		expandable_items: false,
		onResize: () => {}
	};

	constructor(props)
	{
		super(props);
		this.state = {
			expand_disabled: true,
			data: {
				page_num: 0,
				page_size: 100
			},
			list_data: [],
			entry_refs: [],
			all_shown: false,
			search: '',
			url: props.url,
			ctype: props.ctype,
			sub_type: props.sub_type,
			loading: true
		};

		this.new_refs = [];

		this.set_ref = (item) => {
			this.new_refs.push(item);
			this.setState({entry_refs: this.new_refs});
		}

		this.fetch_entries = (objects, curr_length) => {
			let new_list_data = [];

			for (let i = 0; i < objects.length; i++) {
				const obj = objects[i];
				const type_config = Classification.definitions[this.state.ctype];
				const child_config = Classification.definitions[type_config.child];
				const idx = curr_length+new_list_data.length;

				new_list_data.push({
					set_ref: this.set_ref,
					expand_on_open: this.state.all_shown,
					ctype: this.state.ctype,
					name: obj.name,
					id: obj.id,
					subclasses: obj[child_config.api.accessor.count],
					tcdb: obj.tcdb,
					pfam: obj.pfam,
                    interpro: obj.interpro,
					expandable: this.props.expandable_items,
					expand_url: Urls.api_url(type_config.api.route + '/' + obj.id + child_config.api.route),
					onResize: this.props.onResize,
					index: idx
				});
			}

			return new_list_data;

		}

		this.render_list_items = () => {
			let entries = [];

			for (let i = 0; i < this.state.list_data.length; ++i) {
				entries.push(
					<li key={this.state.list_data[i].index} className="list-link">
						<ListItem {...this.state.list_data[i]} />
					</li>
				);
			}

			return entries;
		}

		this.expand_all = () => {
			this.setState({all_shown: true});
			for (let i = 0; i < this.state.entry_refs.length; ++i) {
				this.state.entry_refs[i].set_expand(true);
			}
		}

		this.collapse_all = () => {
			this.setState({all_shown: false});
			for (let i = 0; i < this.state.entry_refs.length; ++i) {
				this.state.entry_refs[i].set_expand(false);
			}
		}

		this.handle_search = (value) => {
			this.setState({
				expand_disabled: true,
				loading: true,
				list_data: []
			});

			let params = {pageSize: this.state.data.page_size};

			if (value !== "") {
				params.search = value;
			}

			let new_url = appendQuery(this.state.url, params);

			axios.get(new_url)
			.then(response => {
				let new_list_data = this.fetch_entries(response.data.objects, 0);
				let disable = (new_list_data.length === response.data.total_objects);
				this.setState({
					list_data: new_list_data,
					search: value,
					expand_disabled: disable,
					data: response.data,
					loading: false
				})

			})

		}

		this.handle_expansion = () => { 
			// Load next page of data
		    let params = {};
		    let new_page = this.state.data.page_num + 1;

		    params.pageSize = this.state.data.total_objects;

		    if (this.state.search !== "") {
		    	params.search = this.state.search;
		    }

		    let new_url = appendQuery(this.state.url, params)
		    axios.get(new_url)
		    .then(response => {
		    	let new_list_data = this.fetch_entries(response.data.objects.slice(this.state.list_data.length, response.data.objects.length), this.state.list_data.length);
		    	let curr_list_data = this.state.list_data.slice().concat(new_list_data);
		    	let disable = (curr_list_data.length === response.data.total_objects);
		    	let old_length =  this.state.list_data.length;

		    	// Render the new data
			    this.setState({
			    	list_data: curr_list_data,
			    	expand_disabled: true,
			    	data: {
			    		page_num: new_page,
			    		page_size: params.pageSize
			    	},
			    	loading: false
			    });


		    });

		    this.setState({
				expand_disabled: true,
				loading: true
			});
		}
		

		if (!props.hasOwnProperty('data')) {
			let params = {
				pageSize: this.state.data.page_size
			};
			let new_url = appendQuery(this.state.url, params);

			axios.get(new_url)
			.then(response => {
					let new_list_data = this.fetch_entries(response.data.objects, 0);
					let disable = (new_list_data.length === response.data.total_objects);

					this.setState({
						data: response.data,
						list_data: new_list_data,
						expand_disabled: disable,
						loading: false
					}, () => {
						if (!props.has_table) {
							this.handle_expansion();
						}
					});
				}
			);
		}
		else {
			this.state.data = props.data;
			this.state.list_data = this.fetch_entries(this.state.data.objects, 0);
			this.state.expand_disabled = (this.state.list_data.length === this.state.data.total_objects);
			this.state.loading = false;
		}

	}
	componentDidMount() {
		if (this.props.hasOwnProperty('onResize')){
			this.props.onResize();
		}
	}

	componentDidUpdate() {
		if (this.props.hasOwnProperty('onResize')){
			this.props.onResize();
		}
	}

	render()
	{	
		this.new_refs = [];

		return(
			<div className="list">
				{ this.props.expandable_items && 
					<div>
						<span className="expand-item" onClick={this.expand_all}>
							show all
						</span>
						{" "}
						<span className="expand-item" onClick={this.collapse_all}>
							hide all
						</span>
					</div>
				}
				<div className={this.props.searchable ? "search-section" : "hidden"}>
					<Search
						onSearch={this.handle_search}
						placeholder="Search list..."
					/>
				</div>
				<ol className="list-data">
					{ this.render_list_items() }
				</ol>
				<div className={this.state.expand_disabled ? "hidden" : "expand"} onClick={this.handle_expansion}>
					See All...
				</div>
				<div className={this.state.loading ? "info-loading" : "hidden"}>
					<img className="loading-image" src={Assets.images.loading}/>
				</div>
			</div>
		);
	}
}

export default List;
