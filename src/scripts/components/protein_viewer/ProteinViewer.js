import * as React from "react";
import * as PropTypes from "prop-types";
import { connect } from "react-redux";
import axios from "axios";

import GLmolViewer from "../visualizers/glmol/GLMOL.js";
import LiteMolViewer from "../visualizers/litemol/LiteMol.js";

import { Assets } from "../../config/Assets";
import { Urls } from "../../config/Urls";
import Classification from "../../config/Classification";

import "./ProteinViewer.scss";

class ProteinViewer extends React.Component
{
	static propTypes = {
		type: PropTypes.string.isRequired,
		viewer: PropTypes.string
	};

	static defaultProps = {
		viewer: "glmol"
	};

	constructor(props)
	{
		super(props);

		// get state from <Link> params
		this.state = {
			name: undefined,
			id: undefined,
			pdbid: undefined,
			url: undefined,
			data_loading: false,
		};

		const { params, query } = props.currentUrl;
		if (params.hasOwnProperty("id") && params.hasOwnProperty("pdbid")) {
			// is a protein in our database
			this.state.id = params.id;
			this.state.pdbid = params.pdbid;
			this.state.url = Urls.pdb_source_file(params.pdbid);
			this.state.type = props.type;

			// fetch protein name if not available
			if (!this.state.name) {
				this.state.data_loading = true;
				const fetch_url = Urls.api_url(Classification.definitions.proteins.api.route + "/" + this.state.id);
				axios.get(fetch_url).then(res => {
					this.setState({
						name: res.data.name,
						data_loading: false
					});
				});
			}
		}

		else {
			// came from a link to the viewer page with a URL to a PDB file in the query
			this.state.type = query.type;
			this.state.url = query.url;
		}

		this.state.dimensions = {
			height: this.state.type === "complex" ? "700px" : "500px",
			width: "100%",
			maxWidth: "100%"
		};
	}

	render()
	{
		if (this.state.data_loading) {
			return(
				<div className="info-loading">
					<img className="loading-image" src={Assets.images.loading}/>
				</div>
			);
		}

		let viewer = null;
		if (this.props.viewer === "glmol") {
			viewer = (
				<div className="protein-glmol">
					<GLmolViewer
						url={this.state.url}
						type={this.state.type}
						useFullWidth={this.state.type === "complex"}
						color_by_default={this.state.type === "complex" ? "chain" : "polarity"}
						show_sidechains_default={this.state.type !== "complex"}
						{...this.state.dimensions}
					/>
				</div>
			);
		}
		else if (this.props.viewer === "litemol") {
			viewer = (
				<div className="protein-litemol">
					<LiteMolViewer
						url={this.state.url}
						type={this.state.type}
						{...this.state.dimensions}
					/>
				</div>
			);
		}

		return(
			<div className="protein-viewer">
				<div className="page-header">
					{this.state.pdbid && this.state.name
						? this.state.pdbid + " » " + this.state.name
						: "GLmol Viewer"
					}
				</div>
				{viewer}
			</div>
		)
	}
}


function mapStateToProps(state) {
  return {
		currentUrl: state.currentUrl,
	};
}

export default connect(mapStateToProps)(ProteinViewer);