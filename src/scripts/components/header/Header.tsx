import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import { HashLink } from "react-router-hash-link";

import "./Header.scss"
import { RootState } from "../../store";

export type BasicNavbarItem = {
	name: string;
	route: string;
};

export type NavBarItem = BasicNavbarItem & {
	menuItems?: BasicNavbarItem[] | readonly BasicNavbarItem[];
};

function mapStateToProps(state: RootState) {
	return {
		currentUrl: state.currentUrl
	};
}

const connector = connect(mapStateToProps);

type HeaderProps = ConnectedProps<typeof connector>;

class Header extends React.Component<HeaderProps>
{
	private static readonly navbarItems: ReadonlyArray<NavBarItem> = [
		{
			name: "HOME",
			route: "/"
		},
		{
			name: "ABOUT OPM",
			route: "/about",
			menuItems: [
				{
					name: "Introduction",
					route: "/about",
				},
				{
					name: "Features",
					route: "/about#features",
				},
				{
					name: "Methods and Definitions",
					route: "/about#methods_and_definitions",
				},
				{
					name: "Precision",
					route: "/about#precision",
				},
				{
					name: "Classification",
					route: "/about#classification",
				},
				{
					name: "Topology Definitions",
					route: "/about#topology_definitions",
				},
				{
					name: "TM Helix Assembly",
					route: "/about#tm_helix_assembly",
				},
				{
					name: "Acknowledgements",
					route: "/about#acknowledgements",
				},
				
			],
		},
		{
			name: "BIOLOGICAL MEMBRANES",
			route: "/biological_membranes",
			menuItems: [
				{
					name: "Description",
					route: "/biological_membranes/biological_descriptions",
				},
				{
					name: "Lipid composition",
					route: "/biological_membranes/lipid_composition",
				},
				{
					name: "Membrane properties",
					route: "/biological_membranes/membrane_properties",
				},
			],
		},
		{
			name: "SEARCH",
			route: "/search"
		},
		{
			name: "DOWNLOAD OPM FILES",
			route: "/download"
		},
		{
			name: "CONTACT US",
			route: "/contact"
		},
		{
			name: "PPM",
			route: "/ppm_server"
		},
		{
			name: "TMPFOLD SERVER",
			route: "/tmpfold_server_cgopm"
		}
	];

	renderMenuItems(navItem: NavBarItem) {
		if (!navItem.menuItems) {
			return null;
		}
		const items = [];
		for (let i = 0; i < navItem.menuItems.length; ++i) {
			const menu_item = navItem.menuItems[i];
			let classes = "nav-bar-item";
			
			items.push(
				<div key={i} className={classes}>
					<HashLink to={menu_item.route}  >
						<strong>{menu_item.name}</strong>
					</HashLink>
				</div>
			);
			if(menu_item.route==navItem.route+"refTable2"){
				window.location.reload();
			}
		}
		return <div className="dropdown-list">{items}</div>;
	}

	renderNavItem(key: number, name: string, route: string, is_selected: boolean, navItem: NavBarItem): React.ReactElement {
		// check if it's a dropdown
		if (navItem.menuItems) {
			return (
				<div key={key} className="nav-bar-item dropdown">
					<strong style={{ cursor: "pointer" }}>{navItem.name}</strong>
					{this.renderMenuItems(navItem)}
				</div>
			);
		}

		if (is_selected) {
			return (
				<div key={key} className="nav-bar-item selected">
					<strong>{name}</strong>
				</div>
			);
		}
		return (
			<div key={key} className="nav-bar-item">
				<Link to={route}>
					<strong>{name}</strong>
				</Link>
			</div>
		);

	}

	renderNavbar(): Array<React.ReactElement> {
		let items: Array<React.ReactElement> = [];
		for (let i: number = 0; i < Header.navbarItems.length; i++) {
			const navItem: NavBarItem = Header.navbarItems[i];
			items.push(this.renderNavItem(i, navItem.name, navItem.route, this.props.currentUrl.url === navItem.route, navItem));
		}
		return items;
	}

	render(): React.ReactElement {
		return (
			<div className="header">
				<img className="header-img" src="/images/opm-background.png" alt="" />
				<div className="nav-bar">
					{this.renderNavbar()}
				</div>
			</div>
		);
	}
}

export default connector(Header);
