/* derived from ApiSearch.tsx
 * this module verifies that a pdb id exists in the external pdb database */
import * as React from "react";
import * as PropTypes from "prop-types";
import axios from "axios";

import { Urls } from "../../config/Urls";

import "bootstrap";
import "./PdbSearch.scss";

export type PdbSearchTypes = "asymmetric_unit" | "biological_assembly";

type PdbSearchProps = {
	className: string,
	placeholder: string,
	searchButtonText: string,
	useEnterSubmit: boolean
	onSearch: (search: string) => boolean | void,
	onSearchComplete: (pdb_id: any, errorMsg: string) => void,
    pdbSearchType: PdbSearchTypes,
    biologicalAssemblyNumber: number | null,
};

export default class PdbSearch extends React.Component<PdbSearchProps>
{
	static defaultProps = {
		className: "",
		placeholder: "Search PDB",
		searchButtonText: "search",
		useEnterSubmit: false,
		onSearch: () => true
	};

	private search: string = "";

	handleSearch = () => {
        let url;

        if (this.props.pdbSearchType === "asymmetric_unit") {
            url = Urls.rcsb.asymmetric_unit(this.search);
        } else {
            // pdbSearchType === "biological_assembly"
            if (this.props.biologicalAssemblyNumber == null) return;
            url = Urls.rcsb.biological_assembly(this.search, this.props.biologicalAssemblyNumber);
        }

		const result = this.props.onSearch(this.search);
		if (result === false) {
			return;
		}

        // just headers, to comfirm that the PDB ID exists
        axios.head(url)
        .then(() => {
            this.props.onSearchComplete(this.search, "");
        })
        .catch((error) => {
            console.log(error);
            this.props.onSearchComplete(null, this.getErrorMsg());
        });
	}

	handleTextChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
		this.search = e.target.value;
	}

	handleKeyPress = (e: React.KeyboardEvent<HTMLInputElement>): void => {
		if (this.props.useEnterSubmit && e.key === "Enter") {
			this.handleSearch();
		}
	}

    getErrorMsg() {
        if (this.props.pdbSearchType === "asymmetric_unit") {
            return `The asymmetric unit of ${this.search} was not included in the PDB database.`;
        }
        else {
            // pdbSearchType === "biological_assembly"
            return `The biological assembly #${this.props.biologicalAssemblyNumber} of `
                    + `${this.search}  was not included in the PDB database.`;
        }
    }

	render(): React.ReactElement
	{
		return(
			<div className="api-search">
				<div className="input-group">
					<input
						type="text"
						id="search-box"
						className={"search-bar form-control input-sm " + this.props.className}
						placeholder={this.props.placeholder}
						onKeyPress={this.handleKeyPress} 
						onChange={this.handleTextChange}
					/>
					<span className="submit-button input-group-addon" onClick={this.handleSearch}>
					    {this.props.searchButtonText}
					</span>
				</div>
			</div>
		);
	}
}
