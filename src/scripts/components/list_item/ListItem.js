import * as React from 'react';
import { Link } from 'react-router-dom';

import axios from 'axios';
import * as appendQuery from 'append-query';
import * as PropTypes from 'prop-types';

import List from '../list/List.js';
import ProteinList from '../list/protein/ProteinList.js';

import { Assets } from "../../config/Assets";
import Classification from "../../config/Classification";
import ExternalLink from "../external_link/ExternalLink";

import './ListItem.scss';

class ListItem extends React.Component
{
	static propTypes = {
		// types, classes, superfamilies, families, localizations (membranes), species, proteins (structure_groupings) 
		ctype: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		id: PropTypes.number.isRequired,
		subclasses: PropTypes.number.isRequired,
		tcdb: PropTypes.string,
		pfam: PropTypes.string,
        interpro: PropTypes.string,
		base: PropTypes.bool,
		expandable: PropTypes.bool,
		expand_url: PropTypes.string,
		onResize: PropTypes.func
	};

	static defaultProps = {
		tcdb: "",
		pfam: "",
		base: false,
		expandable: false,
		expand_url: "",
		onResize: () => {}
	};
	
	constructor(props)
	{
		super(props);
		this.state = {
			expanded: false,
			loading: false,
		};

		this.set_expand = (should_expand) => {
			if ((should_expand && !this.state.expanded)
			|| (!should_expand && this.state.expanded)) {
				this.expand();
			}
		}

		this.expand = () => {
			if (!this.state.expanded) {
				let params = {
					pageSize: 100,
				};
				if (this.props.ctype === 'protein_families') {
					params.sort = 'name';
				}
				let new_url = appendQuery(this.props.expand_url, params);
				axios.get(new_url)
				.then((response) => {
						this.setState({
							listData: response.data,
							loading: false
						});
					}
				);
				
			}
			this.setState({
				expanded: !this.state.expanded,
				loading: !this.state.expanded
			});

		};

		this.renderItem = () => {
			let parts = [];
			const type_config = Classification.definitions[this.props.ctype];
			const child_name = Classification.definitions[type_config.child].name;

			// name link
			if (this.props.base){
				parts.push(
					<strong key={0}>
						{this.props.name}
					</strong>
				);
			}
			else {
				parts.push(
					<Link key={0} to={'/' + this.props.ctype + '/' + this.props.id}>
						{this.props.name}
					</Link>
				);
			}

			if (this.props.base) {
				if (this.props.expandable && !this.state.loading) {
					parts.push(
						<strong key={1}>
							{" "}({this.props.subclasses}{" "}{child_name}{" "}
							<span key={1} className="expand-item" onClick={this.expand}>
								{this.state.expanded ? "hide" : "show"}
							</span>
							{" "})
						</strong>
					);
				}
				else if (this.props.expandable && this.state.loading){
					parts.push(
						<strong key={1}>
							{" "}({this.props.subclasses}{" "}{child_name}{" "}
							<span className="small-loader">
								<img src={Assets.images.loading_small}/>
							</span>
							)
						</strong>
					);
				}
				else {
					parts.push(
						<strong key={1}>
							{" "}({this.props.subclasses}{" "}{child_name})
						</strong>
					);
				}
			}
			else {
				if (this.props.expandable && !this.state.loading) {
					parts.push(
						<span key={1}>
							{" "}({this.props.subclasses}{" "}{child_name}{" "}
							<span className="expand-item" onClick={this.expand}>
								{this.state.expanded ? "hide" : "show"}
							</span>
							{" "})
						</span>
					);
				}
				else if (this.props.expandable && this.state.loading){
					parts.push(
						<span key={1}>
							{" "}({this.props.subclasses}{" "}{child_name}{" "}
							<span className="small-loader">
								<img src={Assets.images.loading_small}/>
							</span>
							)
						</span>
					);
				}
				else {
					parts.push(
						<span key={1}>
							{" "}({this.props.subclasses}{" "}{child_name})
						</span>
					);
				}
			}
			
				
			if (type_config.hasOwnProperty('tcdb') && this.props.tcdb !== "") {
				parts.push(
					<span key={2}>
						{" "}
						<ExternalLink href={type_config.tcdb(this.props.tcdb)}>
							{this.props.tcdb}{" "}(TCDB)
						</ExternalLink>
					</span>
				);
			}
		

			if (type_config.hasOwnProperty('pfam') && this.props.pfam !== null && this.props.pfam !== ""){
				parts.push(
					<span key={3}>
						{" "}
						<ExternalLink href={type_config.pfam(this.props.pfam)}>
							{this.props.pfam}
						</ExternalLink>
					</span>
				);
			}

            if (type_config.hasOwnProperty('interpro') && this.props.interpro !== null && this.props.interpro !== undefined && this.props.interpro !== "") {
                parts.push(
                    <span key={3.5}>
                        {" "}
                        <ExternalLink href={type_config.interpro(this.props.interpro)}>
                            {this.props.interpro}
                        </ExternalLink>
                    </span>
                );
            }

			if (type_config.hasOwnProperty('pdb') && this.props.pfam !== ""){
				parts.push(
					<span key={4}>
						{" "}
						<ExternalLink href={type_config.pdb(this.props.pfam)}>
							PDBsum
						</ExternalLink>
					</span>
				);
			}

			if (this.state.expanded && type_config.child !== "proteins" && type_config.child !== "assemblies" && !this.state.loading){
				parts.push(
					<div key={6} className="expanded-list-section">
						<List
							url={this.props.expand_url}
							ctype={type_config.child}
							sub_type={Classification.definitions[type_config.child].child}
							searchable={false}
							expandable_items={true}
							data={this.state.listData}
							onResize={this.props.onResize}
						/>
					</div>
				);
			}
			else if (this.state.expanded && !this.state.loading) {
				if (type_config.child === "proteins") {
					parts.push(
						<div key={6}>
							<ProteinList
								url={this.props.expand_url}
								data={this.state.listData}
								onResize={this.props.onResize}
								sorted={this.props.ctype === 'protein_families'}
							/>
						</div>
					);
				}
			}
			
			return parts;
		};

	}
	componentWillReceiveProps(nextProps) {
		if (this.props.ctype !== nextProps.ctype || this.props.id !== nextProps.id) {
			this.setState({
				expanded: false,
			});
		}
	}

	componentDidMount() {
		if (this.props.hasOwnProperty('set_ref')) {
			this.props.set_ref(this);
		}
		if (this.props.hasOwnProperty('expand_on_open') && this.props.expand_on_open) {
			this.expand();
		}
		if (this.props.hasOwnProperty('onResize')){
			this.props.onResize();
		}
	}

	componentDidUpdate() {
		if (this.props.hasOwnProperty('onResize')){
			this.props.onResize();
		}
	}

	render()
	{
		return(
			<span className="list-item">
				{ this.renderItem() }
			</span>
		);
	}
}

export default ListItem;
