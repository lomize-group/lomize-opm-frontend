import * as React from 'react';
import * as PropTypes from 'prop-types';

import ListItem from '../list_item/ListItem.js';

import { Urls } from "../../config/Urls";
import Classification from "../../config/Classification";

import './Info.scss';

class Info extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {};

		this.renderInfo = () => {
			var items = [];

			if (this.props.hasOwnProperty('protein_types')) {
				items.push(
					<li key={0} className="info-item">
						<i className="info-name">Type:</i>{" "}
						<ListItem 
							ctype="protein_types"
							id={this.props.protein_types.id}
							name={this.props.protein_types.name}
							subclasses={this.props.protein_types.subclasses}
							base={this.props.base === "protein_types"}
						/>
					</li>
				);
			}

			if (this.props.hasOwnProperty('protein_classes')) {
				items.push(
					<li key={1} className="info-item">
						<i className="info-name">Class:</i>{" "}
						<ListItem 
							ctype="protein_classes"
							id={this.props.protein_classes.id}
							name={this.props.protein_classes.name}
							subclasses={this.props.protein_classes.subclasses}
							base={this.props.base === "protein_classes"}
						/>
					</li>
				);
			}

			if (this.props.hasOwnProperty('protein_superfamilies')) {
				items.push(
					<li key={2} className="info-item">
						<i className="info-name">Superfamily:</i>{" "}
						<ListItem 
							ctype="protein_superfamilies"
							id={this.props.protein_superfamilies.id}
							name={this.props.protein_superfamilies.name}
							subclasses={this.props.protein_superfamilies.subclasses}
							pfam={this.props.protein_superfamilies.pfam}
							tcdb={this.props.protein_superfamilies.tcdb}
							base={this.props.base === "protein_superfamilies"}
						/>
					</li>
				);
			}

			if (this.props.hasOwnProperty('protein_families')) {
				items.push(
					<li key={3} className="info-item">
						<i className="info-name">Family:</i>{" "}
						<ListItem 
							ctype="protein_families"
							id={this.props.protein_families.id}
							name={this.props.protein_families.name}
							subclasses={this.props.protein_families.subclasses}
							pfam={this.props.protein_families.pfam}
                            interpro={this.props.protein_families.interpro}
							tcdb={this.props.protein_families.tcdb}
							base={this.props.base === "protein_families"}
							expandable={this.props.base === "protein_families"}
							expand_url={Urls.api_url(Classification.definitions.protein_families.api.route + '/' + this.props.protein_families.id + Classification.definitions.proteins.api.route)}
							onResize={this.props.onResize}
						/>
					</li>
				);
			}

			if (this.props.hasOwnProperty('protein_species')) {
				items.push(
					<li key={4} className="info-item">
						<i className="info-name">Species:</i>{" "}
						<ListItem 
							ctype="protein_species"
							id={this.props.protein_species.id}
							name={this.props.protein_species.name}
							subclasses={this.props.protein_species.subclasses}
							base={this.props.base === "protein_species"}
							expandable={this.props.base === "protein_species"}
							expand_url={Urls.api_url(Classification.definitions.protein_species.api.route + '/' + this.props.protein_species.id + Classification.definitions.proteins.api.route)}
							onResize={this.props.onResize}
						/>
					</li>
				);
			}

			if (this.props.hasOwnProperty('protein_localizations')) {
				items.push(
					<li key={5} className="info-item">
						<i className="info-name">Localization:</i>{" "}
						<ListItem 
							ctype="protein_localizations"
							id={this.props.protein_localizations.id}
							name={this.props.protein_localizations.name}
							subclasses={this.props.protein_localizations.subclasses}
							base={this.props.base === "protein_localizations"}
							expandable={this.props.base === "protein_localizations"}
							expand_url={Urls.api_url(Classification.definitions.protein_localizations.api.route + '/' + this.props.protein_localizations.id + Classification.definitions.proteins.api.route)}
							onResize={this.props.onResize}
						/>
					</li>
				);
			}

			if (this.props.hasOwnProperty('assembly_localizations')) {
				items.push(
					<li key={5} className="info-item">
						<i className="info-name">Localization:</i>{" "}
						<ListItem 
							ctype="assembly_localizations"
							id={this.props.assembly_localizations.id}
							name={this.props.assembly_localizations.name}
							subclasses={this.props.assembly_localizations.subclasses}
							base={this.props.base === "assembly_localizations"}
							onResize={this.props.onResize}
						/>
					</li>
				);
			}

			if (this.props.hasOwnProperty('assembly_superfamilies')) {
				items.push(
					<li key={2} className="info-item">
						<i className="info-name">Superfamily:</i>{" "}
						<ListItem 
							ctype="assembly_superfamilies"
							id={this.props.assembly_superfamilies.id}
							name={this.props.assembly_superfamilies.name}
							subclasses={this.props.assembly_superfamilies.subclasses}
							pfam={this.props.assembly_superfamilies.pfam}
							tcdb={this.props.assembly_superfamilies.tcdb}
							base={this.props.base === "assembly_superfamilies"}
						/>
					</li>
				);
			}

			if (this.props.hasOwnProperty('assembly_families')) {
				items.push(
					<li key={3} className="info-item">
						<i className="info-name">Family:</i>{" "}
						<ListItem 
							ctype="assembly_families"
							id={this.props.assembly_families.id}
							name={this.props.assembly_families.name}
							subclasses={this.props.assembly_families.subclasses}
							pfam={this.props.assembly_families.pfam}
							tcdb={this.props.assembly_families.tcdb}
							base={this.props.base === "assembly_families"}
							onResize={this.props.onResize}
						/>
					</li>
				);
			}

			return items;
		}

	}


	render()
	{
		return(
			<div className="info">
				<ul className="info-data">
					{ this.renderInfo() }
				</ul>
			</div>
		);
	}
}

Info.propTypes = {
	protein_types: PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		subclasses: PropTypes.number,
	}),
	protein_classes: PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		subclasses: PropTypes.number,
	}),
	protein_superfamilies: PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		subclasses: PropTypes.number,
		pfam: PropTypes.string,
		tcdb: PropTypes.string,
	}),
	protein_families: PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		subclasses: PropTypes.number,
		pfam: PropTypes.string,
		tcdb: PropTypes.string,
	}),
	protein_species: PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		subclasses: PropTypes.number,
	}),
	protein_localizations: PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		subclasses: PropTypes.number,
	}),
	assembly_superfamilies: PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		subclasses: PropTypes.number,
	}),
	assembly_families: PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		subclasses: PropTypes.number,
	}),
	assembly_localizations: PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		subclasses: PropTypes.number,
	}),
	base: PropTypes.string,
	onResize: PropTypes.func
};

Info.defaultProps = {
	base: "",
	onResize: () => {}
}



export default Info;
