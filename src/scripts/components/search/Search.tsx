import * as React from "react";

import * as PropTypes from "prop-types";

import "./Search.scss";

import "bootstrap";

type Props = {
	onSearch: (search: string) => void,
	placeholder: string,
	initial?: string
};

export default class Search extends React.Component<Props> {

	private textInput!: HTMLInputElement;

	static defaultProps = {
		placeholder: "Search..."
	};

	handleSearch = () => {
		this.props.onSearch(this.textInput.value);
	}

	handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
		if (event.key === "Enter") {
			this.props.onSearch((event.target as HTMLInputElement).value);
		}
	}

	componentDidUpdate(nextProps: Props) {
		if (nextProps.initial !== undefined) {
			this.textInput.value = nextProps.initial;
		}
	}

	render() {
		return (
			<div className="search">
				<div className="input-group">
					<input
						ref={(ref: HTMLInputElement) => this.textInput = ref}
						type="text"
						className="search-bar form-control input-sm"
						onKeyPress={this.handleKeyPress}
						placeholder={this.props.placeholder}
					/>
					<span className="submit-button input-group-addon" onClick={this.handleSearch}>
						<i>search</i>
					</span>
				</div>
			</div>
		);
	}
}
