import * as React from "react";
import { connect } from "react-redux";
import axios from "axios";

import GLMol from "../visualizers/glmol/GLMOL.js";

import { Assets } from "../../config/Assets";
import { Urls } from "../../config/Urls";
import Classification from "../../config/Classification";
import ExternalLink from "../external_link/ExternalLink";

import "./Assembly.scss";

class Assembly extends React.Component
{
	static low_energy = -6.0;

	static parseHelixInteractions(interactions)
	{
		// remove whitespace, split by semicolon for each interaction
		const result = interactions.replace(/\s/g, "").split(";").map(item => {
			// split by "," for helix1, helix2, energy
			const triple = item.split(",");
			return {
				helix1: parseInt(triple[0]),
				helix2: parseInt(triple[1]),
				energy: parseFloat(triple[2])
			}
		});
		return result;
	}

	static parseSegments(segments)
	{
		if (!segments) {
			return {start: -1, end: -1};
		}

		const seg_re = /\d+-\d+/;
		const result = segments.replace(/\s/g, "").split(",").map((seg) => {
			let digits = seg_re.exec(seg)[0].split("-");

			// adjust by -1 for 0-indexing
			return {
				start: parseInt(digits[0]) - 1,
				end: parseInt(digits[1]) - 1
			};
		});
		return result;
	}

	static createHelixInteractionMatrix(interactions, matrix_size)
	{
		// make NxN array for matrix
		// have to use matrix_size+1 because of labels
		let result = new Array(matrix_size+1);
		for (let i = 0; i < result.length; ++i) {
			result[i] = new Array(matrix_size+1);
		}

		// fill labels of matrix
		result[0][0] = "TMH";
		for (let i = 1; i < result.length; ++i) {
			result[i][0] = i;
			result[0][i] = i;
		}

		// fill interaction values
		for (let i = 0; i < interactions.length; ++i) {
			const { helix1, helix2, energy } = interactions[i];
			result[helix1][helix2] = energy;
			result[helix2][helix1] = energy;
		}

		return result;
	}

	constructor(props)
	{
		super(props);
		this.state = {
			has_data: false,
			id: props.currentUrl.params.id
		};

		axios.get(Urls.api_url(Classification.definitions.assemblies.api.route + "/" + props.currentUrl.params.id))
		.then(res => {
			let newstate = res.data;
			const parsed_interactions = Assembly.parseHelixInteractions(newstate.helix_interaction);
			newstate.helix_interaction = Assembly.createHelixInteractionMatrix(parsed_interactions, newstate.transmembrane_alpha_helix_count);

			// parse all segments
			newstate.parsed_transmem_segment = Assembly.parseSegments(newstate.transmembrane_segment);
			newstate.transmembrane_alpha_helix = Assembly.parseSegments(newstate.transmembrane_alpha_helix);
			newstate.other_alpha_helix = Assembly.parseSegments(newstate.other_alpha_helix);
			newstate.beta_strand = Assembly.parseSegments(newstate.beta_strand);
			newstate.disordered_segment = Assembly.parseSegments(newstate.disordered_segment);
			newstate.has_data = true;

			this.setState(newstate);
		});
	}

	renderHelixInteractionTableRow(rowNumber)
	{
		let row = [];
		let bold = false;
		for (let i = 0; i < this.state.helix_interaction.length; ++i) {
			const isHelixNumber = (i === 0 || rowNumber == 0);
			let energy = this.state.helix_interaction[i][rowNumber];
			const cellClass = "helix-cell" + (isHelixNumber ? " blue-title" : "") + (energy <= Assembly.low_energy ? " bold" : "");
			if (!isHelixNumber && energy) {
				energy = energy.toFixed(1);
			}

			row.push(
				<td className={cellClass} key={i}>
					{energy}
				</td>
			);
		}
		return (<tr className={rowNumber%2 ? "dark" : "light"} key={rowNumber}>{row}</tr>);
	}

	renderHelixInteractionTable()
	{
		let rows = [];
		for (let i = 0; i < this.state.helix_interaction.length; ++i) {
			rows.push(this.renderHelixInteractionTableRow(i));
		}
		return(
			<div className="info-table full">
				<table>
					<tbody>
						<tr className="dark">
							<th colSpan={this.state.transmembrane_alpha_helix_count+1}>
								Association energies of TM helices (kcal/mol)
							</th>
						</tr>
						{rows}
					</tbody>
				</table>
			</div>
		);
	}

	renderGLMol()
	{
		return(
			<div className="glmol-viewer">
				<GLMol
					url={Classification.definitions.assemblies.pdb_file(this.state.pdbid, this.state.protein_letter)}
					type="subunit"
					width="100%"
					maxWidth="100%"
					height={`${screen.height/2}px`}
					useFullWidth={false}
					color_by_default={"polarity"}
					show_sidechains_default={true}
				/>
			</div>
		);
	}

	renderInfoTable(title, tableClasses, content, count=null)
	{
		tableClasses = "info-table " + tableClasses.join(" ");
		return(
			<div className={tableClasses}>
				<table>
					<tbody>
					<tr className="dark">
						<th><span className="green italic">{title}{count && ": "}</span>{count}</th>
					</tr>
					{content}
					</tbody>
				</table>
			</div>
		);
	}

	renderSequence() {
		let seq = [];
		let fragment = "";
		let color = "";
		let segs = {
			tm_seg: {},
			tm_alpha: {},
			other_alpha: {},
			beta: {},
			disordered: {}
		};
		const shouldColorSegment = (s, curr_idx) => s.seg && curr_idx >= s.seg.start && curr_idx <= s.seg.end;		

		for (let s in segs) {
			segs[s].index = 0;
			segs[s].seg = null;
		}

		for (let i = 0; i < this.state.aaseq.length; ++i) {
			if (i !== 0 && i % 10 === 0) {
				fragment += " ";
			}
			let next_color = "";
			segs.tm_seg.seg = this.state.parsed_transmem_segment[segs.tm_seg.index];
			segs.tm_alpha.seg = this.state.transmembrane_alpha_helix[segs.tm_alpha.index];
			segs.other_alpha.seg = this.state.other_alpha_helix[segs.other_alpha.index];
			segs.beta.seg = this.state.beta_strand[segs.beta.index];
			segs.disordered.seg = this.state.disordered_segment[segs.disordered.index];

			// find the next color
			if (shouldColorSegment(segs.tm_seg, i)) {
				next_color = "red";
			}
			else if (shouldColorSegment(segs.tm_alpha, i)) {
				next_color = "blue";
			}
			if (shouldColorSegment(segs.other_alpha, i)) {
				next_color = "blue";
			}
			if (shouldColorSegment(segs.beta, i)) {
				next_color = "lightgreen";
			}
			if (shouldColorSegment(segs.disordered, i)) {
				next_color = "grey";
			}

			// move to next segment if we passed a segment
			for (let s in segs) {
				if (segs[s].seg && i == segs[s].seg.end) {
					++segs[s].index;
				}
			}

			if (color !== next_color) {
				seq.push(<span key={i} className={color}>{fragment}</span>);
				fragment = "";
				color = next_color;
			}
			fragment += this.state.aaseq[i];
		}
		seq.push(<span key={this.state.aaseq.length} className={color}>{fragment}</span>);

		return seq;
	}

	renderSequenceLegend() {
		return(
			<div className="legend">
				<h5 className="legend-title">Amino Acid Sequence Legend:</h5>
				<ul>
					<li className="red">Red - TM segments</li>
					<li className="blue">Blue - alpha-helices</li>
					<li className="lightgreen">Green - beta-strands</li>
					<li className="grey">Grey - disordered segments</li>
				</ul>
			</div>
		);
	}

	renderLinks() {
		return(
			<tr>
				<td className="center">
					<ExternalLink href={Urls.uniprot(this.state.uniprotcode)}>UniProtKB</ExternalLink>
				</td>
			</tr>
		);
	}

	renderSegments() {
		return [
			<tr key={0}>
				<td className="left">PDB coordinates: {this.state.subunit_segment}</td>
			</tr>,
			<tr key={1}>
				<td className="left">PDB sequence: {this.state.transmembrane_segment}</td>
			</tr>
		];
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.currentUrl.params.id !== this.props.currentUrl.params.id) {
			this.get(nextProps.currentUrl.params.id);
			this.setState({
				has_data: false,
			});
		}
	}
	render()
	{
		if (!this.state.has_data) {
			return(
				<div className="info-loading">
					<img className="loading-image" src={Assets.images.loading}/>
				</div>
			);
		}
		return(
			<div className="assembly">
				<div className="page-header">
					{this.state.pdbid} » {this.state.protein_letter} {this.state.name}
				</div>
				<div className="assembly-glmol">
					{this.renderGLMol()}
				</div>
				<br/>
				{this.renderHelixInteractionTable()}
				{this.renderInfoTable("Assembly Equation", ["left"], <tr><td className="center">{this.state.equation}</td></tr>)}
				{
					this.state.uniprotcode && 
					this.renderInfoTable("Links", ["right"], this.renderLinks())
				}
				{this.renderInfoTable("TM Segments", ["full"], this.renderSegments())}
				{this.renderInfoTable("Amino Acid Sequence", ["full"], <tr><td className="mono">{this.renderSequence()}</td></tr>)}
				{this.renderSequenceLegend()}
			</div>
		);
	}
}

function mapStateToProps(state) {
  return {
		currentUrl: state.currentUrl,
	};
}

export default connect(mapStateToProps)(Assembly);
