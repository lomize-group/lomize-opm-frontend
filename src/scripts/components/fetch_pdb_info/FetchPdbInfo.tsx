import * as appendQuery from "append-query";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { RootState } from "../../store";
import UnformattedTab from "./components/Unformatted";
import PrimaryStructureTab from "./components/PrimaryStructure";
import "./FetchPdbInfo.scss";
import Submitted from "../submitted/Submitted";

function mapStateToProps(state: RootState) {
    return {
        currentUrl: state.currentUrl,
    };
}
const connector = connect(mapStateToProps);

type Props = ConnectedProps<typeof connector> & {
    submitted?: boolean;
};

type Tab = "unformatted" | "primary_structure";

type State = {
    tab: Tab;
};

class FetchPDBInfo extends React.Component<Props, State> {
    state: State = {
        tab: "primary_structure",
    };

    renderTab() {
        switch (this.state.tab) {
            case "primary_structure":
                return <PrimaryStructureTab afterSubmit={this.afterSubmit} />;
            case "unformatted":
                return <UnformattedTab afterSubmit={this.afterSubmit} />;
        }
    }

    afterSubmit = (data: any) => {
        const urlParams = {
            resultsUrl: data.resultsUrl || "",
            waitingUrl: data.waitingUrl || "",
        };
        const submitUrl = appendQuery(this.props.currentUrl.url + "/submitted", urlParams);
        this.props.currentUrl.history.push(submitUrl);
    };

    render() {
        if (this.props.submitted) {
            return <Submitted autoLoadResults={this.state.tab === "primary_structure"} />;
        }

        return (
            <div className="fetch-pdb-info">
                <div className="fetch-pdb-header">Fetch PDB Info</div>
                <label htmlFor="mode">Output:</label>{" "}
                <select
                    name="mode"
                    id="mode"
                    onChange={(e) => this.setState({ tab: e.target.value as Tab })}
                >
                    <option value="primary_structure">CSV format (primary structure)</option>
                    <option value="unformatted">Unformatted (old)</option>
                </select>
                {this.renderTab()}
            </div>
        );
    }
}

export default connector(FetchPDBInfo);
