import Axios from "axios";
import * as React from "react";
import { Assets } from "../../../config/Assets";
import { GenericObj } from "../../../config/Types";
import { Urls } from "../../../config/Urls";
import { readFile } from "../../../config/Util";
import InfoTable, { InfoTableColumn } from "../../info_table/InfoTable";
import "./PrimaryStructure.scss";

type Props = {
    afterSubmit: (data: any) => void;
};

type State = {
    loading: boolean;
    params?: GenericObj<ProteinParams>;
    dbData?: GenericObj<ProteinDBData>;
    combined?: CombinedData[];
    dbFileError: string;
    paramFileError: string;
};

type ProteinParams = {
    pdbCode: string;
    thickness: string;
    thicknessError: string;
    tilt: string;
    tiltError: string;
    gibbs: string;
};

type ProteinDBData = {
    pdbCode: string;
    refPdb?: string;
    subunit: string;
    topology: string;
    name: string;
};

type CombinedData = ProteinParams & ProteinDBData;

function getInitialState(): State {
    return {
        loading: false,
        params: undefined,
        dbData: undefined,
        combined: undefined,
        dbFileError: "",
        paramFileError: "",
    };
}

const columns: readonly InfoTableColumn[] = [
    {
        header: "PDB",
        renderCell: (data: CombinedData) => data.pdbCode,
        sortKey: (data: CombinedData) => data.pdbCode,
        defaultSort: true,
    },
    {
        header: "Reference PDB",
        renderCell: (data: CombinedData) => data.refPdb || "",
        sortKey: (data: CombinedData) => data.refPdb || "",
    },
    {
        header: "Subunit",
        renderCell: (data: CombinedData) => data.subunit,
        sortKey: (data: CombinedData) => data.subunit,
    },
    {
        header: "Topology",
        renderCell: (data: CombinedData) => data.topology,
        sortKey: (data: CombinedData) => data.topology,
    },
    {
        header: "Name",
        renderCell: (data: CombinedData) => data.name,
        sortKey: (data: CombinedData) => data.name,
    },
    {
        header: "Thickness",
        renderCell: (data: CombinedData) => data.thickness,
        sortKey: (data: CombinedData) => data.thickness,
    },
    {
        header: "Thickness Error",
        renderCell: (data: CombinedData) => data.thicknessError,
        sortKey: (data: CombinedData) => data.thicknessError,
    },
    {
        header: "Tilt",
        renderCell: (data: CombinedData) => data.tilt,
        sortKey: (data: CombinedData) => data.tilt,
    },
    {
        header: "Tilt Error",
        renderCell: (data: CombinedData) => data.tiltError,
        sortKey: (data: CombinedData) => data.tiltError,
    },
] as const;

export default class CSV extends React.Component<Props, State> {
    state = getInitialState();

    onDbFileUpload = async (evt: React.ChangeEvent<HTMLInputElement>) => {
        // read as string
        const lines = (await readFile(evt.target.files![0])).split("\n");

        // parse
        const PATTERN = /([\w\d]{4})\s+([\w\d]{4})?\s+([\w\d]+)\s+(in|out)\s+(.*)/i;
        const dbData: GenericObj<ProteinDBData> = {};
        for (let i = 0; i < lines.length; ++i) {
            const match = lines[i].match(PATTERN);
            if (match) {
                const pdbCode = match[1];
                if (pdbCode) {
                    dbData[pdbCode] = {
                        pdbCode,
                        refPdb: match[2],
                        subunit: match[3],
                        topology: match[4],
                        name: match[5],
                    };
                }
            }
        }

        if (Object.keys(dbData).length === 0) {
            this.setState({ dbData: undefined, dbFileError: "Could not parse input file 1." });
        } else if (this.state.params) {
            this.setState({
                dbData,
                combined: this.combineData(this.state.params, dbData),
                dbFileError: "",
            });
        } else {
            this.setState({ dbData, dbFileError: "" });
        }
    };

    combineData = (
        params: GenericObj<ProteinParams>,
        dbData: GenericObj<ProteinDBData>
    ): CombinedData[] => {
        const data: CombinedData[] = [];
        const keys = Object.keys(dbData);
        let errors: string[] = [];
        for (let i = 0; i < keys.length; ++i) {
            const key = keys[i];
            const d = dbData[key];
            const p = params[key];
            if (d && p) {
                data.push({ ...d, ...p });
            }
        }
        return data;
    };

    onParamFileUpload = async (evt: React.ChangeEvent<HTMLInputElement>) => {
        // read as string
        const lines = (await readFile(evt.target.files![0]))
            .replace(/;\s*\n/g, "\n") // replace ending semicolons
            .replace(/;\s*$/g, "") // replace final semicolon if there's no \n at the end of the input
            .split("\n"); // split into lines

        // parse
        const params: GenericObj<ProteinParams> = {};
        for (let i = 0; i < lines.length; ++i) {
            // split up row and remove whitespace
            const vals = lines[i].split(";").map((val) => val.trim());
            const pdbCode = vals[0].split(".")[0];
            if (vals.length > 1 && pdbCode) {
                params[pdbCode] = {
                    pdbCode,
                    thickness: vals[1],
                    thicknessError: vals[2],
                    tilt: vals[3],
                    tiltError: vals[4],
                    gibbs: vals[5],
                };
            }
        }

        if (Object.keys(params).length === 0) {
            this.setState({ params: undefined, paramFileError: "Could not parse input file 2." });
        } else if (this.state.dbData) {
            this.setState({
                params,
                combined: this.combineData(params, this.state.dbData),
                paramFileError: "",
            });
        } else {
            this.setState({ params, paramFileError: "" });
        }
    };

    submit = async () => {
        if (!this.state.combined) {
            return;
        }

        this.setState({ loading: true });
        const params = {
            data: this.state.combined,
        };

        Axios.post(Urls.fetchPdbInfoPrimaryStructure, params).then(({ data }) => {
            this.setState({ loading: false });
            this.props.afterSubmit(data);
        });
    };

    renderSubmit = () => {
        const { combined } = this.state;
        return combined ? (
            <div>
                <div>
                    <button onClick={this.submit}>Submit</button>
                </div>
                <br />
                <span>Input data - {combined.length} PDB entries uploaded</span>
                <InfoTable showHeaders columns={columns} data={combined} />
            </div>
        ) : null;
    };

    render() {
        if (this.state.loading) {
            return (
                <div className="info-loading">
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            );
        }

        return (
            <div>
                <div className="file-uploads">
                    <div>
                        <label htmlFor="dbFile">Input file 1 - database info list</label>{" "}
                        <input
                            name="dbFile"
                            id="dbFile"
                            type="file"
                            onChange={this.onDbFileUpload}
                        />{" "}
                        <span className="error-msg">{this.state.dbFileError}</span>
                    </div>
                    <div>
                        <label htmlFor="paramFile">
                            Input file 2 - geometric parameters and energy
                        </label>{" "}
                        <input
                            name="paramFile"
                            id="paramFile"
                            type="file"
                            onChange={this.onParamFileUpload}
                        />{" "}
                        <span className="error-msg">{this.state.paramFileError}</span>
                    </div>
                    {this.renderSubmit()}
                </div>
            </div>
        );
    }
}
