import Axios from "axios";
import * as React from "react";
import { Assets } from "../../../config/Assets";
import { Urls } from "../../../config/Urls";
import "./Unformatted.scss";

type Props = {
    afterSubmit: (data: any) => void;
};

type FormFields = {
    pdbCodeInput: string;
};

type State = FormFields & {
    errorText: string;
    loading: boolean;
};

function getInitialState(): State {
    return {
        pdbCodeInput: "",
        errorText: "",
        loading: false,
    };
}

export default class Unformatted extends React.Component<Props, State> {
    state = getInitialState();

    handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const newState: Pick<FormFields, any> = { [e.target.name]: e.target.value };
        this.setState(newState);
    };

    validatePdbCodeInput = () => {
        // allows arbitrary whitespace, commas, etc
        // expression length 4 because pdb code is length of 4:
        //		https://www.rcsb.org/pages/help/advancedsearch/pdbIDs
        // also allow line ending in comma for easy copy/paste into text box
        const PDB_LINE_PATTERN = /^\s*([\d\w]{4}(\s*[,;\s]\s*[\d\w]{4})*)*\s*,?$/;

        // error if nothing entered
        if (this.state.pdbCodeInput.length === 0) {
            this.setState({ errorText: "please enter PDB codes" });
            return false;
        }

        // split into lines so we can give feedback on which line is invalid
        const lines = this.state.pdbCodeInput.split("\n");

        // validate each line
        let errorLines = [];
        for (let i = 0; i < lines.length; ++i) {
            if (!PDB_LINE_PATTERN.test(lines[i])) {
                errorLines.push(i + 1);
            }
        }

        if (errorLines.length > 0) {
            let errorText = "please enter PDB codes in format specified (line";
            if (errorLines.length > 1) {
                errorText += "s";
            }
            errorText += " " + errorLines.join(", ") + ")";
            this.setState({ errorText });
            return false;
        }

        return true;
    };

    submit = () => {
        // clear error text
        this.setState({ errorText: "" });

        if (!this.validatePdbCodeInput()) {
            return;
        }

        // begin submit by setting to
        this.setState({ loading: true });

        // replace all punctuation with spaces, then match non-whitespaces
        const pdbCodes = this.state.pdbCodeInput.replace(/[,;]/g, " ").match(/\S+/g);

        // send requested PDB codes and wait for process to run
        const params = { pdbCodes };
        Axios.post(Urls.fetchPdbInfo, params).then(({ data }) => {
            this.setState({ loading: false });
            this.props.afterSubmit(data);
        });
    };

    reset = () => {
        this.setState(getInitialState());
    };

    render() {
        if (this.state.loading) {
            return (
                <div className="info-loading">
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            );
        }

        return (
            <div className="form">
                <div className="pdbcode-input-container">
                    <b>Input PDB codes separated by whitespace and/or commas:</b>
                    <textarea
                        name="pdbCodeInput"
                        id="pdbcode-input"
                        rows={10}
                        placeholder={"Input PDB codes separated by whitespace and/or commas"}
                        value={this.state.pdbCodeInput}
                        onChange={this.handleChange}
                    />
                </div>
                <button className="submit-button" onClick={this.submit}>
                    Submit
                </button>
                <br />
                <span className="error-msg">{this.state.errorText}</span>
                <br />
                <div className="form-footer"></div>
            </div>
        );
    }
}
