import * as React from "react";
import axios from "axios";

import { Assets } from "../../config/Assets";
import { Urls } from "../../config/Urls";

import SubmittedPage from "../tmpfold_server/tmpfold_server_ybrc/submitted_page/SubmittedPage.js";

import "./FetchPDBInfo.scss";

class FetchPDBInfo extends React.Component
{
	constructor(props)
	{
		super(props);
	
		this.get_initial_state = () => ({
			pdb_code_input: "",
			results_link: null,
			loading: false,
			submitted: false
		});

		this.state = this.get_initial_state();

		this.handle_change = (event) => {
			this.setState({[event.target.name]: event.target.value});
		}

		this.validate_pdb_code_input = () => {
			// allows arbitrary whitespace, commas, etc
			// expression length 4 because pdb code is length of 4:
			//		https://www.rcsb.org/pages/help/advancedsearch/pdbIDs
			// also allow line ending in comma for easy copy/paste into text box
			const pdb_line_re = /^\s*([\d\w]{4}(\s*[,;\s]\s*[\d\w]{4})*)*\s*,?$/;

			// error if nothing entered
			if (this.state.pdb_code_input.length === 0) {
				this.setState({ error_text: "please input PDB codes" });
				return false;
			}

			// split into lines so we can give feedback on which line is invalid
			const pdb_code_lines = this.state.pdb_code_input.split('\n');

			// validate each line
			let error_lines = [];
			for (let i = 0; i < pdb_code_lines.length; ++i) {
				if (!pdb_line_re.test(pdb_code_lines[i])) {
					error_lines.push(i+1);
				}
			}
			
			if (error_lines.length > 0) {
				let err_msg = "please input PDB codes in format specified (line";
				if (error_lines.length > 1) {
					err_msg += "s";
				}
				err_msg += " " + error_lines.join(", ") + ")"
				this.setState({ error_text: err_msg });
				return false;
			}

			return true;
		} 

		this.submit = () => {
			// clear old error text
			this.setState({error_text: ""});

			if (!this.validate_pdb_code_input()) {
				return;
			}

			this.setState({loading: true});

			// replace all punctuation with spaces, then match non-whitespaces (returns array)
			const pdb_codes = this.state.pdb_code_input.replace(/[,;]/g, " ").match(/\S+/g);

			// send requested PDB codes and wait for update script to run
			const params = {pdb_codes}
			axios.post(Urls.fetch_pdb_info, params)
			.then(res => {
				this.setState({
					results_link: res.data.waiting_link,
					loading: false,
					submitted: true
				});
			});

		}

		this.reset = () => {
			this.setState(this.get_initial_state());
		}
	}

	render()
	{
		if (this.state.loading) {
			return(
				<div className="info-loading">
					<img className="loading-image" src={Assets.images.loading}/>
				</div>
			);
		}

		if (this.state.submitted) {
			return(
				<div className="fetch-pdb-info">
					<div className="fetch-pdb-header">Fetch PDB Info</div>
					<SubmittedPage
						email={this.state.email}
						waiting_link={this.state.results_link}
						submit_again={this.reset}
					/>
				</div>
			);
		}

		return(
			<div className="fetch-pdb-info">
				<div className="fetch-pdb-header">Fetch PDB Info</div>
				<div className="form">
					<div className="pdbcode-input-container">
						<b>Input PDB codes separated by whitespace and/or commas:</b>
						<textarea
							name="pdb_code_input"
							id="pdbcode-input"
							rows={10}
							placeholder={"Input PDB codes separated by whitespace and/or commas"}
							value={this.state.pdb_code_input}
							onChange={this.handle_change}
						/>
					</div>
					<button
						className="submit-button"
						onClick={this.submit}
					>
						Submit
					</button>
					<br/>
					<span className="error-msg">
						{this.state.error_text}
					</span>
					<br/>
					<div className="form-footer">
					</div>
				</div>
				<div className="result">
					{this.state.result}
				</div>
			</div>
		);
	}
}

export default FetchPDBInfo;
