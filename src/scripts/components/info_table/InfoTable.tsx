import * as React from "react";
import { sortStrings } from "../../config/Util";
import "./InfoTable.scss";

type TableContent = JSX.Element | string | number;

type SortInfo = {
    column: number;
    reverse: boolean;
};

type TableData = {
    data: any[];
    sort?: SortInfo;
    hasSorted: boolean;
};

export type InfoTableColumn = {
    header?: TableContent;
    renderCell: (val: any) => TableContent;
    sortKey?: string | ((item: any) => string);
    defaultSort?: boolean;
};

export type InfoTableProps = {
    title?: TableContent;
    showHeaders: boolean;
    columns: InfoTableColumn[] | ReadonlyArray<InfoTableColumn>;
    data: any[];
};

const InfoTable: React.FC<InfoTableProps> = (props) => {
    const [data, setData] = React.useState<TableData>({
        data: [...props.data],
        hasSorted: false,
    });

    React.useEffect(() => {
        setData({
            data: [...props.data],
            hasSorted: false
        });
    }, [props.data]);

    let rows = [];
    if (props.title) {
        rows.push(
            <tr key={rows.length} className="dark">
                <th colSpan={props.columns.length}>{props.title}</th>
            </tr>
        );
    }

    if (props.showHeaders) {
        let headerCells: JSX.Element[] = [];
        for (let i = 0; i < props.columns.length; ++i) {
            const { sortKey, defaultSort, header } = props.columns[i];

            const sortData = () => {
                if (sortKey !== undefined) {
                    const reverse =
                        data.sort !== undefined && data.sort.column === i && !data.sort.reverse;
                    const sorted = sortStrings(data.data, sortKey, reverse);
                    setData({
                        data: sorted,
                        sort: {
                            reverse,
                            column: i,
                        },
                        hasSorted: true,
                    });
                }
            };

            if (defaultSort && !data.hasSorted) {
                sortData();
            }

            let className = "";
            if (sortKey) {
                className += "sortable";
            }
            if (data.sort && data.sort.column === i) {
                className += data.sort.reverse ? " desc" : " asc";
            }

            headerCells.push(
                <th key={i} onClick={sortData} className={className}>
                    {header || ""}
                </th>
            );
        }
        rows.push(
            <tr key={rows.length} className="dark">
                {headerCells}
            </tr>
        );
    }

    for (let i = 0; i < data.data.length; ++i) {
        let cells: JSX.Element[] = [];
        for (let j = 0; j < props.columns.length; ++j) {
            cells.push(<td key={j}>{props.columns[j].renderCell(data.data[i])}</td>);
        }
        rows.push(
            <tr key={rows.length} className={i % 2 === 0 ? "light" : "dark"}>
                {cells}
            </tr>
        );
    }

    return (
        <div className="info-table2">
            <table>
                <tbody>{rows}</tbody>
            </table>
        </div>
    );
};

export default InfoTable;
