import * as React from 'react';

import ImgWrapper from '../img_wrapper/ImgWrapper';

import { Assets } from "../../config/Assets";

import './Contact.scss';

class Contact extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {};
	}


	render()
	{
		return(
			<div className="contact">
				<div className="page-header">
					Contact Us
				</div>
				<div className="body-header">
					For OPM data related questions
				</div>
				<div className="container-fluid">
					<div className="row">
						<div className="col-sm-6 centered contact-sect">
							<ImgWrapper 
								src={Assets.images.andrei}
								className="contact-img"
							/>
							<p className="contact-info centered">
								<a href="https://pharmacy.umich.edu/lomize-group">
									Andrei L. Lomize
								</a>
								, PhD
							</p>
							<p className="contact-info centered">
								Research Scientist
							</p>
							<p className="contact-info centered">
								Room 4056
							</p>
							<p className="contact-info centered">
								College of Pharmacy
							</p>
							<p className="contact-info centered">
								University of Michigan
							</p>
							<p className="contact-info centered">
								Ann Arbor, MI 48109, USA
							</p>
							<p className="contact-info centered">
								7734-925-2370 (Office)
							</p>
							<p className="contact-info centered">
								lomizeal at gmail.com
							</p>
						</div>
						<div className="col-sm-6 centered contact-sect">
							<ImgWrapper 
								src={Assets.images.alexey}
								className="contact-img"
							/>
							<p className="contact-info centered">
								<a href="https://www.linkedin.com/in/alexey-kovalenko-3455a01b6/">
									Alexey Kovalenko
								</a>
							</p>
							<p className="contact-info centered">
								B.S. in Computer Science and Biochemistry
							</p>
							<p className="contact-info centered">
								Software Engineer
							</p>
							<p className="contact-info centered">
							College of Literature, Science, and the Arts
							</p>
							<p className="contact-info centered">
								University of Michigan
							</p>
							<p className="contact-info centered">
								Ann Arbor, MI 48109, USA
							</p>
							<p className="contact-info centered">
								424-217-9712
							</p>
							<p className="contact-info centered">
								alexeyk at umich.edu
							</p>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Contact;
