import * as React from "react";

import ExternalLink from "../external_link/ExternalLink";

import "./Footer.scss";

export default function Footer()  {
	return(
		<div className="footer-box">
			<div className="maintext">
				&copy; 2005 - 2019 The Regents of the University of Michigan
				{" | "}
				<ExternalLink href="https://creativecommons.org/licenses/by/3.0/">
					License
				</ExternalLink>
			</div>
		</div>
	);
}
