// this differs from Classification since Classification assumes a primary_structures-based table
import * as React from "react";
import axios from "axios";
import { Link } from "react-router-dom";

import { Urls } from "../../config/Urls";
import { TableColumn } from "../../config/Types";
import Table from "../table/Table.js";
import { create_header_cell } from "../table/Columns";

import "./SpeciesTable.scss"

const columns: ReadonlyArray<TableColumn> = [
    {
        Header: create_header_cell("Name", <div>Name</div>),
        id: "name",
        flexGrow: 2,
        minWidth: 150,
        Cell: (row: any) => <div className="cell" title={row.name}>
            <div>
                <Link to={`/protein_species/${row.id}`}>
                    {row.name}
                </Link>
            </div>
        </div>
    },
    {
        Header: create_header_cell("Primary Structures Count", <div>Primary Structures Count</div>),
        id: "primary_structures_count",
        flexGrow: 1,
        minWidth: 50,
        Cell: (row: any) => <div className="cell" title={row.primary_structures_count}>
            <div>{row.primary_structures_count}</div>
        </div>
    },
    {
        Header: create_header_cell("Taxonomic Lineage", <div>Taxonomic Lineage</div>),
        id: "description",
        flexGrow: 3,
        minWidth: 300,
        Cell: (row: any) => {
            const max = 200;
            let description = row.description;
            if (description !== null && description.length > max)
                description = `${description.slice(0, max)}...`;
            return (
                <div className="cell" title={row.description}>
                    <div>{description}</div>
                </div>
            );
        }
    },
];

export default () => (
    <div className="species-table">
        <Table
            url={Urls.api_url("species")}
            title="Species"
            columns={columns}
        />
    </div>
);
