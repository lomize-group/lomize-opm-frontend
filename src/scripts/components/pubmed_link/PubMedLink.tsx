import * as React from "react";

import ExternalLink from "../external_link/ExternalLink";


export interface PubMedLinkProps {
	id: string
};

export default class PubMedLink extends React.Component<PubMedLinkProps>
{
	static readonly PUBMED_BASE_URL: string = "https://www.ncbi.nlm.nih.gov/pubmed/";

	static defaultProps = {
		children: "PubMed"
	};

	render(): React.ReactElement
	{
		return(
			<ExternalLink href={PubMedLink.PUBMED_BASE_URL + this.props.id}>
				{this.props.children}
			</ExternalLink>
		);
	}
}
