import * as React from "react";
import * as PropTypes from "prop-types";
import axios from "axios";

import { Urls } from "../../config/Urls";

import "bootstrap";
import "./ApiSearch.scss";


type ApiSearchProps = {
	className: string,
	placeholder: string,
	searchButtonText: string,
	useEnterSubmit: boolean
	onSearch: (search: string) => boolean | void,
	onSearchComplete: (searchResponse: any) => void,
	apiEndpoint: string | ((search: string) => string),
};

export default class ApiSearch extends React.Component<ApiSearchProps>
{
	static defaultProps = {
		className: "",
		placeholder: "Search OPM",
		searchButtonText: "search",
		useEnterSubmit: false,
		onSearch: () => true
	};

	private search: string = "";

	handleSearch = async () => {
		const result = this.props.onSearch(this.search);
		if (result === false) {
			return;
		}

		let url: string;
		if (typeof this.props.apiEndpoint === "function") {
			url = Urls.api_url(this.props.apiEndpoint(this.search));
		}
		else {
			url = Urls.api_url(this.props.apiEndpoint, { search: this.search });
		}
		const { data } = await axios.get(url);
		this.props.onSearchComplete(data);
	}

	handleTextChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
		this.search = e.target.value;
	}

	handleKeyPress = (e: React.KeyboardEvent<HTMLInputElement>): void => {
		if (this.props.useEnterSubmit && e.key === "Enter") {
			this.handleSearch();
		}
	}

	render(): React.ReactElement
	{
		return(
			<div className="api-search">
				<div className="input-group">
					<input
						type="text"
						id="search-box"
						className={"search-bar form-control input-sm " + this.props.className}
						placeholder={this.props.placeholder}
						onKeyPress={this.handleKeyPress} 
						onChange={this.handleTextChange}
					/>
					<span className="submit-button input-group-addon" onClick={this.handleSearch}>
					    {this.props.searchButtonText}
					</span>
				</div>
			</div>
		);
	}
}
