import * as React from "react";
import { Link } from "react-router-dom";
import "./PPMDisambiguation.scss";

export default () => (
    <div className="ppm-disambiguation">
        <h1>PPM Web Server</h1>
        <h2>Positioning of proteins in membranes</h2>
        PPM Server functionality:
        <ul>
            <li>
                calculation of rotational and translational positions in membranes of transmembrane
                (polytopic and monotopic) and peripheral proteins and peptides using their 3D
                structures as input
            </li>
            <li>
                using optimization of transfer free energies of protein structures from water to the
                membrane-like environment with specified physico-chemical and mechanical properties
            </li>
            <li>
                application to proteins with experimentally obtained 3D structures and theoretical
                models
            </li>
            <li>
                download a protein structure (coordinate file) with calculated membrane boundaries
                (at the level of lipid carbonyls)
            </li>
            <li>
                visualization of calculated proteins structures in membranes using Jmol
                (platform-independent java-viewer) and iCn3D (a WebGL-based 3D viewer)
            </li>
            <li>
                membrane proteins from PDB pre-calculated by PPM 2.0 are available through the OPM
                database
            </li>
        </ul>
        <div>
            <Link to="/ppm_server2_cgopm">
                <button title="launch ppm2">
                    Launch PPM 2.0
                </button>
            </Link>
        </div>
        <div>
            <Link to="/ppm_server3_cgopm">
                <button title="launch ppm3">
                    Launch PPM 3.0
                </button>
            </Link>
        </div>
        <div>
            <div><b>References:</b></div>
            <div><b>PPM 2.0</b></div>
            <div>
                Lomize M.A., Pogozheva I,D, Joo H., Mosberg H.I., Lomize A.L. OPM database and PPM
                web server: resources for positioning of proteins in membranes. Nucleic Acids Res.,
                2012, 40 (Database issue):D370-376 [
                <a href="https://pubmed.ncbi.nlm.nih.gov/21890895/" target="_blank">
                    PubMed
                </a>
                ]
            </div>
            <div><b>PPM 3.0</b></div>
            <div>
                Lomize AL, Todd SC, Pogozheva ID. (2022) Spatial arrangement of proteins in planar and curved membranes by PPM 3.0. 
                <i> Protein Sci.</i>
                <b> 31</b>
                :209-220. [
                <a href="https://pubmed.ncbi.nlm.nih.gov/34716622/" target="_blank">
                    PubMed
                </a>
                ]
            </div>
        </div>
    </div>
);
