import * as appendQuery from "append-query";
import axios from "axios";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { ComputationServer, Urls } from "../../config/Urls";
import { RootState } from "../../store";
import Submitted from "../submitted/Submitted";
import "./Fold.scss";

const MAX_NUM_SEQUENCES = 10;
const FOLD_SERVER = Urls.computation_servers.cgopm;

const mapStateToProps = (state: RootState) => ({
    currentUrl: state.currentUrl,
});
const connector = connect(mapStateToProps);

type FoldProps = ConnectedProps<typeof connector> & {
    submitted?: boolean,
};

type TemplateMode = "NONE" | "PDB70" | "CUSTOM";
type MsaMode = "MMSEQS2_UNIREF_ENV" | "MMSEQS2_UNIREF" | "SINGLE_SEQUENCE" | "CUSTOM";
type PairMode = "UNPAIRED_PAIRED" | "PAIRED" | "UNPAIRED";
type NumRecycle = "_1" | "_3" | "_6" | "_12" | "_24" | "_48";
type RecycleEarlyStopTolerance = "AUTO" | "_0_0" | "_0_2" | "_0_5" | "_0_7" | "_1_0";
type MaxMsa = "AUTO" | "_512_1024" | "_256_512" | "_64_128" | "_32_64" | "_16_32";
type ModelType = "AUTO" | "ALPHAFOLD2_PTM" | "ALPHAFOLD2_MULTIMER_V1" | "ALPHAFOLD2_MULTIMER_V2" | "ALPHAFOLD2_MULTIMER_V3";

type TemplateFile = {
    name: string,
    contents: string,
};

type FoldForm = {
    sequences: string[],
    numRelax: number, // float
    templateMode: TemplateMode,
    templateFiles: TemplateFile[],
    msaMode: MsaMode,
    msaFile: string | null,
    pairMode: PairMode,
    numRecycles: NumRecycle,
    recycleEarlyStopTolerance: RecycleEarlyStopTolerance,
    randomSeed: number, // pos int
    numSeeds: number, // pos int
    useDropout: boolean,
    maxMsa: MaxMsa,
    disableClusterProfile: boolean,
    saveRecycles: boolean,
    modelType: ModelType,
    userEmail: string | null,
};

type FoldState = FoldForm & {
    loading: boolean,
    instructions: boolean,
    errorText: string,
};

const defaultFoldState: FoldState = {
    // form,
    sequences: [""],
    numRelax: 0,
    templateMode: "NONE",
    templateFiles: [],
    msaMode: "MMSEQS2_UNIREF_ENV",
    msaFile: null,
    pairMode: "UNPAIRED_PAIRED",
    numRecycles: "_3",
    recycleEarlyStopTolerance: "AUTO",
    randomSeed: 0,
    numSeeds: 1,
    useDropout: false,
    maxMsa: "AUTO",
    disableClusterProfile: false,
    saveRecycles: false,
    modelType: "AUTO",
    userEmail: null,
    // page state
    loading: false,
    instructions: false,
    errorText: "",
};

class Fold extends React.Component<FoldProps, FoldState> {
    constructor(props: FoldProps) {
        super(props);
        this.state = defaultFoldState;
        this.handleIntChange = this.handleIntChange.bind(this);
        this.handleBoolChange = this.handleBoolChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleTemplateFileChange = this.handleTemplateFileChange.bind(this);
        this.handleMsaFileChange = this.handleMsaFileChange.bind(this);
        this.reset = this.reset.bind(this);
        this.submit = this.submit.bind(this);
    }

    reset() {
        this.setState(defaultFoldState);
    }

    handleIntChange(event: React.ChangeEvent<HTMLInputElement>) {
        event.persist();
        this.setState((state: FoldState) => {
            let value = null;
            if (event.target.value !== "") {
                value = parseInt(event.target.value);
            }
            state = {...state, [event.target.name]: value};
            return state;
        });
    }

    handleBoolChange(event: React.ChangeEvent<HTMLInputElement>) {
        event.persist();

        this.setState((state: FoldState) => {
            const name = event.target.name;
            let value = null;
            if (name === "useDropout") {
                value = state.useDropout;
            } else if (name === "disableClusterProfile") {
                value = state.disableClusterProfile;
            } else if (name === "saveRecycles") {
                value = state.saveRecycles;
            }

            return {...state, [name]: !value};
        });
    }

    handleSequenceChange(index: number, event: React.ChangeEvent<HTMLTextAreaElement>) {
        event.persist();
        this.setState(({ sequences }: FoldState) => {
            sequences = [...sequences];
            sequences[index] = event.target.value;
            return { sequences };
        });
    }

    handleSequenceAmountChange(isAdd: boolean) {
        this.setState(({ sequences }: FoldState) => {
            sequences = [...sequences];

            if (isAdd) {
                if (sequences.length < MAX_NUM_SEQUENCES) {
                    sequences.push("");
                }
            } else if (sequences.length > 1) {
                sequences.pop();
            }

            return { sequences };
        });
    }

    handlePairModeChange(event: React.ChangeEvent<HTMLSelectElement>) {
        this.setState({
            pairMode: event.target.value as PairMode,
        });
    }

    handleChange(event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
        event.persist();
        this.setState((state: FoldState) => {
            state = {...state, [event.target.name]: event.target.value};
            if (event.target.name === "templateMode" && event.target.value !== "CUSTOM") {
                state.templateFiles = [];
            }
            return state;
        });
    }

    async handleTemplateFileChange(event: React.ChangeEvent<HTMLInputElement>) {
        const templateFiles: TemplateFile[] = [];
        const files = event.target.files as FileList;

        for (let i = 0; i < files.length; i++) {
            const file = files.item(i);
            if (file === null) {
                console.log(`null file #${i}`);
                continue;
            }
            templateFiles.push({
                name: file.name,
                contents: await file.text(),
            });
        }

        this.setState({ templateFiles });
    }

    async handleMsaFileChange(event: React.ChangeEvent<HTMLInputElement>) {
        const files = event.target.files as FileList;
        const file = files.item(0);
        let msaFile = null;
        if (file !== null) {
            msaFile = await file.text();
        }
        this.setState({ msaFile });
    }

    submit() {
        const errors = [];

        if (this.state.templateMode === "CUSTOM") {
            if (this.state.templateFiles.length === 0) {
                errors.push("must upload custom template files");
            } else {
                // we only need to perform validation here because the backend fixes up the filenames
                const filenames: string[] = [];

                for (let i = 0; i < this.state.templateFiles.length; i++) {
                    // turn filename into 4 lowercase letters
                    const file = this.state.templateFiles[i];
                    const filenameParts = file.name.split(".");
                    const extension = filenameParts.pop();
                    const name = filenameParts.join(".").substr(0, 4).toLowerCase();
                    const filename = `${name}.${extension}`;

                    if (filenames.includes(filename)) {
                        errors.push("duplicate filename detected: " + filename);
                    } else {
                        filenames.push(filename);
                    }
                }
            }
        }

        if (this.state.sequences.some(sequence => sequence === "")) {
            errors.push("must input sequence");
        }

        if (this.state.msaMode == "CUSTOM" && this.state.msaFile === null) {
            errors.push("must upload custom msa file (.a3m)");
        }

        if (this.state.numSeeds === null) {
            errors.push("must input number of seeds");
        }

        if (errors.length > 0) {
            this.setState({ errorText: errors.join("; ") });
            return;
        }

        const formData: FoldForm = {
            sequences: this.state.sequences,
            numRelax: this.state.numRelax,
            templateMode: this.state.templateMode,
            templateFiles: this.state.templateFiles,
            msaMode: this.state.msaMode,
            msaFile: this.state.msaFile,
            pairMode: this.state.pairMode,
            numRecycles: this.state.numRecycles,
            recycleEarlyStopTolerance: this.state.recycleEarlyStopTolerance,
            randomSeed: this.state.randomSeed !== null ? this.state.randomSeed : 0,
            numSeeds: this.state.numSeeds,
            useDropout: this.state.useDropout,
            maxMsa: this.state.maxMsa,
            disableClusterProfile: this.state.disableClusterProfile,
            saveRecycles: this.state.saveRecycles,
            modelType: this.state.modelType,
            userEmail: this.state.userEmail || null,
        };
        this.setState({ loading: true });
        console.log(formData);
        axios.post(FOLD_SERVER + "fold", formData)
            .then(res => {
                const urlParams = {
                    resultsUrl: res.data.resultsUrl || "",
                    waitingUrl: res.data.waitingUrl || "",
                    message: res.data.message,
                };
                let s = this.props.currentUrl.url.charAt(this.props.currentUrl.url.length - 1) === '/' ? "submitted" : "/submitted";
                const submitUrl = appendQuery(this.props.currentUrl.url + s, urlParams);
                this.props.currentUrl.history.push(submitUrl);
                this.setState({ loading: false, errorText: "" });
            })
            .catch(err => {
                let error: string;
                if (err.response) {
                    // Request made and server responded
                    error = "Request made and server responded with failure status code: " + err.response.status.toString();
                }
                else if (err.request) {
                    // Request made but no response received,
                    error = "Request was made to the server but no response was received, this may be because the server is down.";
                }
                else {
                    // Something happened in setting up the request that triggered an error
                    error = "Setting up the request triggered an error: " + err.message;
                }
                console.log("An error occurred: " + JSON.stringify(err))
                this.setState({
                    errorText: error,
                    loading: false
                });
            });
    }

    renderContent() {
        if (this.props.submitted) {
            return (
                <Submitted
                    submitAgain={this.reset}
                    userEmail={this.state.userEmail || undefined}
                />
            );
        }

        return (
            <>
                <div className="fold-introduction">
                    <h1>Fold Subtitle</h1>
                    Fold Introduction
                </div>
                <table className="table-form">
                    <thead>
                        <tr>
                            <td colSpan={2}>
                                ColabFold
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Sequences</td>
                            <td className="sequences-input">
                                <div>
                                    <input
                                        type="button"
                                        value="+"
                                        onClick={() => this.handleSequenceAmountChange(true /* isAdd */)}
                                    />
                                    <input
                                        type="button"
                                        value="-"
                                        onClick={() => this.handleSequenceAmountChange(false /* !isAdd */)}
                                    />
                                </div>
                                {this.state.sequences.map((sequence, index) => (<>
                                    <textarea
                                        placeholder={`sequence #${index+1}`}
                                        value={sequence}
                                        onChange={(event) => this.handleSequenceChange(index, event)}
                                    />
                                </>))}
                            </td>
                        </tr>
                        <tr className="dark">
                            <td>
                                Model type
                            </td>
                            <td>
                                <select
                                    name="modelType"
                                    value={this.state.modelType}
                                    onChange={this.handleChange}
                                >
                                    <option value="AUTO">auto</option>
                                    <option value="ALPHAFOLD2_PTM">alphafold2_ptm</option>
                                    <option value="ALPHAFOLD2_MULTIMER_V1">alphafold2_multimer_v1</option>
                                    <option value="ALPHAFOLD2_MULTIMER_V2">alphafold2_multimer_v2</option>
                                    <option value="ALPHAFOLD2_MULTIMER_V3">alphafold2_multimer_v3</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Num relax</td>
                            <td>
                                <input
                                    type="number"
                                    name="numRelax"
                                    value={this.state.numRelax}
                                    onChange={this.handleIntChange}
                                    min="0" max="5"
                                />
                            </td>
                        </tr>
                        <tr className="dark">
                            <td>Template mode</td>
                            <td>
                                <select
                                    name="templateMode"
                                    value={this.state.templateMode}
                                    onChange={this.handleChange}
                                >
                                    <option value="NONE">none</option>
                                    <option value="PDB70">pdb70</option>
                                    <option value="CUSTOM">custom</option>
                                </select>
                            </td>
                        </tr>
                        {this.state.templateMode === "CUSTOM"
                            ? (<tr className="dark">
                                    <td>Custom template files</td>
                                    <td>
                                        <input type="file" multiple onChange={this.handleTemplateFileChange} />
                                        Filenames must be at most 4 letters long, lowercase, and unique <br />
                                        Example: 7rsy.pdb
                                    </td>
                            </tr>)
                            : undefined
                        }
                        <tr>
                            <td>MSA mode</td>
                            <td>
                                <select
                                    name="msaMode"
                                    value={this.state.msaMode}
                                    onChange={this.handleChange}
                                >
                                    <option value="MMSEQS2_UNIREF_ENV">mmseqs2_uniref_env</option>
                                    <option value="MMSEQS2_UNIREF">mmseqs2_uniref</option>
                                    <option value="SINGLE_SEQUENCE">single_sequence</option>
                                    <option value="CUSTOM">custom</option>
                                </select>
                            </td>
                        </tr>
                        {this.state.msaMode === "CUSTOM"
                            ? (<tr>
                                <td>Custom MSA file</td>
                                <td>
                                    <input type="file" onChange={this.handleMsaFileChange} />
                                    Example: file.a3m
                                </td>
                            </tr>)
                            : undefined
                        }
                        <tr className="dark">
                            <td>Pair mode</td>
                            <td>
                                <select
                                    name="pairMode"
                                    value={this.state.pairMode}
                                    onChange={this.handleChange}
                                >
                                    <option value="UNPAIRED_PAIRED">unpaired+paired</option>
                                    <option value="PAIRED">paired</option>
                                    <option value="UNPAIRED">unpaired</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Number of recycles</td>
                            <td>
                                <select
                                    name="numRecycles"
                                    value={this.state.numRecycles}
                                    onChange={this.handleChange}
                                >
                                    <option value="_1">1</option>
                                    <option value="_3">3</option>
                                    <option value="_6">6</option>
                                    <option value="_12">12</option>
                                    <option value="_24">24</option>
                                    <option value="_48">48</option>
                                </select>
                            </td>
                        </tr>
                        <tr className="dark">
                            <td>
                                Recycle early stop tolerance
                            </td>
                            <td>
                                <select
                                    name="recycleEarlyStopTolerance"
                                    value={this.state.recycleEarlyStopTolerance}
                                    onChange={this.handleChange}
                                >
                                    <option value="AUTO">auto</option>
                                    <option value="_0_0">0.0</option>
                                    <option value="_0_2">0.2</option>
                                    <option value="_0_5">0.5</option>
                                    <option value="_0_7">0.7</option>
                                    <option value="_1_0">1.0</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Save recycles
                            </td>
                            <td>
                                <input
                                    type="checkbox"
                                    name="saveRecycles"
                                    checked={this.state.saveRecycles}
                                    onChange={this.handleBoolChange}
                                />
                            </td>
                        </tr>
                        <tr className="dark">
                            <td>
                                Max MSA
                            </td>
                            <td>
                                <select
                                    name="maxMsa"
                                    value={this.state.maxMsa}
                                    onChange={this.handleChange}
                                >
                                    <option value="AUTO">auto</option>
                                    <option value="_512_1024">512:1024</option>
                                    <option value="_256_512">256:512</option>
                                    <option value="_64_128">64:128</option>
                                    <option value="_32_64">32:64</option>
                                    <option value="_16_32">16:32</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Random seed
                            </td>
                            <td>
                                <input
                                    type="number"
                                    name="randomSeed"
                                    min="0"
                                    value={this.state.randomSeed}
                                    onChange={this.handleIntChange}
                                />
                            </td>
                        </tr>
                        <tr className="dark">
                            <td>
                                Number of seeds
                            </td>
                            <td>
                                <input
                                    type="number"
                                    name="numSeeds"
                                    min="1"
                                    value={this.state.numSeeds}
                                    onChange={this.handleIntChange}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Use dropout
                            </td>
                            <td>
                                <input
                                    type="checkbox"
                                    name="useDropout"
                                    checked={this.state.useDropout}
                                    onChange={this.handleBoolChange}
                                />
                            </td>
                        </tr>
                        <tr className="dark">
                            <td>
                                Disable cluster profile
                            </td>
                            <td>
                                <input
                                    type="checkbox"
                                    name="disableClusterProfile"
                                    checked={this.state.disableClusterProfile}
                                    onChange={this.handleBoolChange}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email
                                <br />
                                You will be notified by email when the calculations are complete.
                            </td>
                            <td>
                                <input
                                    type="text"
                                    name="userEmail"
                                    placeholder="email (optional)"
                                    value={this.state.userEmail || undefined}
                                    onChange={this.handleChange}
                                />
                            </td>
                        </tr>
                        <tr className="dark">
                            <td>
                                Please do not submit too many jobs at the same time.
                                If you would like to run ColabFold yourself, see{" "}
                                <a href="https://github.com/sokrypton/ColabFold" target="_blank">sokrypton/ColabFold</a>.
                            </td>
                            <td>
                                <input
                                    type="button"
                                    value="Reset"
                                    onClick={this.reset}
                                />
                                <input
                                    type="button"
                                    value="Submit"
                                    onClick={this.submit}
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div className="error">
                    {this.state.errorText}
                </div>
            </>
        );
    }

    render() {
        return (
            <div className="fold">
                <div className="server-header">Fold</div>
                {this.renderContent()}
            </div>
        );
    }
}

export default connector(Fold);
