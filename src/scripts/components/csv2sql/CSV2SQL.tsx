import * as React from 'react';
import axios from 'axios';
import { Urls } from "../../config/Urls";
import { Assets } from "../../config/Assets";
import "./CSV2SQL.scss";

// Describes the state of the Page
type CSV2SQLState = {
    csvFile: File | null,
    errors: string[],
    loading: boolean, 
    resultsUrl: string | null, // the result can be accesssed using a URL
    tablename: string| null, // this will take in the name of the table in the
    sql_filename: string | null,
    // database
}


const getInitialState = (): CSV2SQLState => ({
    csvFile: null,
    errors: [],
    loading: false,
    resultsUrl: null,
    tablename: null,
    sql_filename: null,
})


export default class CSV2SQL extends React.Component<{}, CSV2SQLState> {
    constructor(props: {}) {
        super(props);
        this.state = getInitialState();
        this.submit = this.submit.bind(this);
        this.handleCSVFileChange = this.handleCSVFileChange.bind(this);
        this.handleTablenameChange = this.handleTablenameChange.bind(this);
        this.handleSQLFilenameChange = this.handleSQLFilenameChange.bind(this);
    }

    handleCSVFileChange(event: React.ChangeEvent<HTMLInputElement>) {
        const files = event.target.files;
        if (files && files.length > 0) {
            const csvFilename = files[0].name;
            this.setState({
                csvFile: files[0],
                sql_filename: csvFilename.split(".")[0] + ".sql"
            });
        }
    }

    handleTablenameChange(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({ tablename: event.target.value})
    }

    handleSQLFilenameChange(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({ sql_filename: event.target.value})
    }

    submit() {
        const { csvFile, errors} = this.state;

        // Ensure that all the information is filled out
        if (csvFile === null) {
            this.setState({ errors: ["Must upload a CSV file. Please select a file."] });
            return;
        }
        if (this.state.tablename === null) {
            this.setState({ errors: ["Must enter a tablename. Please enter a tablename."] });
            return;
        }
        if (this.state.sql_filename === null) {
            this.setState({ errors: ["Must enter a filename. Please enter a filename."] });
            return;
        }

        if (errors.length !== 0) {
            this.setState({ errors: []});
        }

        this.setState({ loading: true});
        const form = new FormData();
        form.append('csv_file', csvFile);
        form.append('tablename', this.state.tablename as string)
        form.append('sql_filename', this.state.sql_filename as string)
        axios.post(Urls.api_base + "/csv2sql", form)
        .then( response => {
            if (response.status !== 200 && response.status !== 202) {
                throw new Error("Unexpected status code when submitting csv2sql: " + response.status);
            }
            return response.data;
        })
        .then (data => {
            // this.setState({ resultsUrl: data.results,
            //                 loading: false
            // });
            var blob = new Blob([data], {type: 'text/plain'});

            // make a blob url for the file
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);

            // set the filename
            link.download = this.state.sql_filename as string;

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        })
        .catch(err => {
            console.error("Error when submitting csv2sql: " + err);
            this.setState({ errors: ["Error when submitting csv2sql: " + err],
                            loading: false});
        })

    }


    render() {
        const { errors, loading, csvFile, resultsUrl } = this.state;
        let errorsHtml = null;
        if (errors.length !== 0) {
            errorsHtml = (<div>
                {errors.map(error => <div>{error}</div>)}
            </div>);
        }

        const formHtml = (
        <div>
                <label htmlFor="csv_file"> Choose CSV with data to convert to sql.</label>
                <input
                    type="file"
                    name="csv_file"
                    accept="text/csv"
                    required
                    onChange={this.handleCSVFileChange}
                    className='buttons'
                />
                <label htmlFor="tablename"> Enter the tablename in the database: </label> <br />
                <input
                    type="text"
                    name="tablename"
                    required
                    placeholder="Enter Name"
                    onChange={this.handleTablenameChange}
                    style={{ width: "50%",
                    maxWidth: "400px",
            }}
                />
                <br /> <label htmlFor="sql_filename"> Name your SQL File: </label> <br />
                <input
                    type="text"
                    name="sql_filename"
                    value={ this.state.sql_filename || ""}
                    required
                    placeholder="Enter Name"
                    onChange={this.handleSQLFilenameChange}
                    style={{ width: "50%",
                            maxWidth: "400px",
                    }}
                />
                <br />

            <button onClick={this.submit} className='buttons'>Submit</button>
        </div>)
        const loadingHtml = <img className="loading-image" src={Assets.images.loading} alt="loading" />;
        const contentHtml = loading ? loadingHtml : formHtml;

        // let downloadHtml = null;

        // if (!loading && resultsUrl !== null) {
        //     downloadHtml = (
        //         <a href={Urls.api_base + resultsUrl}>Download SQL File</a>
        //     );
        // }

        return (
            <div className='page'>
                <h1>CSV to SQL</h1>
                <p> This will help you to turn your CSV formatted data into a list of SQL commands
                    that will try to insert the data into a database. In order to do this, you will
                    need to indicate the name of the table that you want to insert the data into. Keep in mind that 
                    if you already have the pdbid in the table, executing the sql script will create duplicates.<br />
                    IMPORTANT NOTE: If your CSV file has a special column "Id" then the script will interpret
                    this command as the primary key for the table. In this case, the script will insert a new row only
                    if the Id is not already in the table. If the Id is already in the table, then the script will update
                    the row with the new data.
                </p>
                {formHtml}
                {/* {downloadHtml} */}
                {errorsHtml}
            </div>
        );
    }
}