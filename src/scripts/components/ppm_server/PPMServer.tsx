import * as zlib from "zlib";
import * as appendQuery from "append-query";
import axios from "axios";
import "bootstrap";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Assets } from "../../config/Assets";
import { GenericObj } from "../../config/Types"
import { ComputationServer, Urls } from "../../config/Urls";
import { validate } from "../../config/Util";
import { RootState } from "../../store";
import ApiSearch from "../api_search/ApiSearch";
import PdbSearch, { PdbSearchTypes } from "../pdb_search/PdbSearch";
import Instructions from "./Instructions";
import Submitted from "../submitted/Submitted";
import "./PPMServer.scss";
import { Link } from "react-router-dom";

function mapStateToProps(state: RootState) {
	return {
		currentUrl: state.currentUrl
	};
}

const connector = connect(mapStateToProps)

export type PPMProps = ConnectedProps<typeof connector> & {
    computationServer: ComputationServer,
	submitted?: boolean
};

type FileInputMode = "upload" | "searchOPM" | "searchPDB";

export type PPMForm = {
    fileInputMode: FileInputMode,
    fileSearchResults: GenericObj[],
    fileSearchPdbId: string,
    pdbSearchType: PdbSearchTypes,
    biologicalAssemblyNumber: number | null,
    hasSearched: boolean,
    pdbFile?: File,
    pdbSearchError: string,
	topologyIn: boolean,
	heteroatoms: boolean,
	userEmail?: string
};

type PPMState = PPMForm & {
    loading: boolean,
    fileSearchLoading: boolean,
	instructions: boolean,
	error: boolean,
	errorText: string
};

function getInitialState(): PPMState {
	return {
        // page state
        loading: false,
        fileSearchLoading: false,
		instructions: false,
		error: false,
        errorText: "",
        // form
        pdbFile: undefined,
        fileInputMode: "upload",
        fileSearchResults: [],
        fileSearchPdbId: "",
        pdbSearchType: "asymmetric_unit",
        biologicalAssemblyNumber: 1,
        pdbSearchError: "",
        hasSearched: false,
		topologyIn: true,
		heteroatoms: false,
		userEmail: undefined
	};
}

class PPMServer extends React.Component<PPMProps, PPMState> {

    state = getInitialState();
    
    handlePdbFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const files = e.target.files;
        this.setState({ pdbFile: files ? files[0] : undefined });
    }
    
    handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => {
		let newState: Pick<PPMForm, any> = {
			[e.target.name]: e.target.value
		};
        const {
            error, errorText,
            fileSearchResults, fileSearchPdbId, hasSearched,
            pdbSearchType, biologicalAssemblyNumber, pdbSearchError,
        } = getInitialState();

		this.setState({
            error, errorText,
            fileSearchResults, fileSearchPdbId, hasSearched,
            pdbSearchType, biologicalAssemblyNumber, pdbSearchError,
			...newState
		});
	}

    handlePdbSearchTypeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        const value: any = e.target.value;
        this.setState({
            pdbSearchType: value
        });
    }

    handleBiologicalAssemblyNumberChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        let value: number | null = null;
        if (e.target.value) value = parseInt(e.target.value);
        this.setState({
            biologicalAssemblyNumber: value
        });
    }

	filenameValid = (filename: string) => {
		if (filename.indexOf(".") < 0) {
			return false;
		}
		let parts = filename.split(".");
		if (parts[parts.length - 1].toLowerCase() !== "pdb") {
			return false;
		}
		return true
	}

	handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const files = e?.target?.files;
		this.setState({
			pdbFile: files ? files[0] : undefined,
			error: false,
			errorText: ""
		});
    }

    disableSubmit = () => {
		if (this.state.fileInputMode === "upload" && this.state.pdbFile === undefined) {
			return true;
		}
		if (this.state.fileInputMode === "searchOPM" && this.state.fileSearchPdbId === undefined) {
			return true;
		}
		if (this.state.fileInputMode === "searchPDB") {
            if (!this.state.fileSearchPdbId) {
                return true;
            }

            if (this.state.pdbSearchType === "biological_assembly" && this.state.biologicalAssemblyNumber === null) {
                return true;
            }
		}
		if (this.state.errorText) {
			return true;
		}
		return false;
	}
    
    sendRequest = (params: FormData) => {
        const { computationServer } = this.props;
        axios.post(Urls.computation_servers[computationServer] + "ppm", params)
        .then(res => {
            // push url for submitted page
            const urlParams = {
                resultsUrl: res.data.resultsUrl || "",
                waitingUrl: res.data.waitingUrl || "",
                message: res.data.message,
            };
            let s = this.props.currentUrl.url.charAt(this.props.currentUrl.url.length - 1) === '/' ? "submitted" : "/submitted";
            const submitUrl = appendQuery(this.props.currentUrl.url + s, urlParams);
            this.props.currentUrl.history.push(submitUrl);
            this.setState({ loading: false });
        })
        .catch(err => {
            let error: string;
            if (err.response) {
                // Request made and server responded
                error = "Request made and server responded with failure status code: " + err.response.status.toString();
            }
            else if (err.request) {
                // Request made but no response received,
                error = "Request was made to the server but no response was received, this may be because the server is down.";
            }
            else {
                // Something happened in setting up the request that triggered an error
                error = "Setting up the request triggered an error: " + err.message;
            }
            console.log("An error occurred: " + JSON.stringify(err))
            this.setState({
                error: true,
                errorText: error,
                loading: false
            });
        });
    }
    
    validForm = (): string[] => {
        let errors: string[] = [];
        if (this.state.fileInputMode === "upload" && !validate.pdbFilename(this.state.pdbFile?.name)) {
			errors.push("invalid filename");
		}
		else if (this.state.fileInputMode === "searchOPM" && !validate.pdbFilename(this.state.fileSearchPdbId + ".pdb")) {
			errors.push("invalid filename");
        }

		if (!validate.email(this.state.userEmail)) {
			errors.push("invalid email");
        }
        return errors;
    }

	submit = () => {
        // check for errors
        const errors = this.validForm();

		// don't submit if errors
		if (errors.length > 0) {
			this.setState({
				error: true,
				errorText: errors.join("; ")
			});
			return;
		}

		let formData = new FormData();
		formData.append("pdbFile", this.state.pdbFile!)
		formData.append("topologyIn", this.state.topologyIn.toString());
		formData.append("heteroatoms", this.state.heteroatoms.toString());

		if (this.state.userEmail) {
			formData.append("userEmail", this.state.userEmail);
		}

        if (this.state.fileInputMode === "upload") {
            formData.append("pdbFile", this.state.pdbFile!);
            this.sendRequest(formData)
        }
        else if (this.state.fileInputMode === "searchOPM") {
            const url = Urls.pdb_source_file(this.state.fileSearchPdbId);
			axios.get(url, { responseType: "arraybuffer" })
				.then(({ data }) => {
					const fileBlob = new Blob([data]);
					formData.append("pdbFile", fileBlob, this.state.fileSearchPdbId + ".pdb")
                    this.sendRequest(formData);
                })
                .catch((err) => {
                    this.setState({ loading: false })
                    this.setState({
                        error: true,
                        errorText: `The coordinate file ${this.state.fileSearchPdbId} was not included to the OPM database.`
                    });
                })
        }
        else if (this.state.fileInputMode === "searchPDB") {
            this.submitPdb(formData);
        }

        this.setState({ loading: true });
	}

    submitPdb = async (formData: FormData) => {
        let data;

        try {
            if (this.state.pdbSearchType === "asymmetric_unit") {
                const url = Urls.rcsb.asymmetric_unit(this.state.fileSearchPdbId);
                const response = await axios.get(url, { responseType: "arraybuffer" });
                data = response.data;
            } else {
                // pdbSearchType === "biological_assembly"
                if (this.state.biologicalAssemblyNumber == null) {
                    // this should already be checked
                    return;
                }
                const url = Urls.rcsb.biological_assembly(this.state.fileSearchPdbId, this.state.biologicalAssemblyNumber);
                const response = await axios.get(url, { responseType: "arraybuffer" });
                data = zlib.gunzipSync(new Buffer(response.data));
            }
        } catch (error) {
            this.setState({
                loading: false,
                error: true,
                errorText: `The coordinate file ${this.state.fileSearchPdbId} was not included to the PDB database.`
            });
            throw error;
        }

        formData.append("pdbFile", new Blob([data]), this.state.fileSearchPdbId + ".pdb")
        this.sendRequest(formData);
    }

	reset = () => {
		this.setState(getInitialState());
    }

    renderFileSearchResults() {
        if (this.state.fileInputMode === "searchOPM") {
            if (this.state.fileSearchLoading) {
                return <img src={Assets.images.loading_small} alt="Loading" />;
            }

            if (!this.state.fileSearchResults) {
                return null;
            }

            if (this.state.fileSearchResults.length === 0) {
                if (this.state.errorText) {
                    return (
                        <span className="error-msg">
                            No results found.
                        </span>
                    );
                }
                if (this.state.hasSearched) {
                    return (
                        <span className="error-msg">
                            The PDB entry was not found in the OPM database.
                        </span>
                    );
                }
                return null;
            }

            const items = this.state.fileSearchResults.map((val, idx) =>
                <option value={val.pdbid} key={idx}>{val.pdbid}.pdb</option>
            );
            return (
                <select
                    className="pdb-search-results"
                    name="fileSearchPdbId"
                    onChange={this.handleChange}
                >
                    {items}
                </select>
            );
        }

        if (this.state.fileInputMode === "searchPDB") {
            if (!this.state.hasSearched) {
                return null;
            }

            if (this.state.fileSearchLoading) {
                return <img src={Assets.images.loading_small} alt="Loading" />;
            }

            const pdbId = this.state.fileSearchPdbId

            if (pdbId === null) {
                return (
                    <span className="error-msg">
                        {this.state.pdbSearchError}
                    </span>
                );
            }

            return (
                <select
                    className="pdb-search-results"
                    name="fileSearchPdbId"
                    onChange={this.handleChange}
                >
                    <option value={pdbId}>
                        {pdbId}.pdb
                    </option>
                </select>
            );
        }

        return null;
	}
    
    renderFileInput() {
		let content: JSX.Element = <div />;
		if (this.state.fileInputMode === "upload") {
			content = (
				<input
					name="pdbFile"
					className="file-upload"
					type="file"
					onChange={this.handlePdbFileChange}
				/>
			);
		}
		else if (this.state.fileInputMode === "searchOPM") {
			const onComplete = (data: any) => {
				this.setState({
					fileSearchLoading: false,
					fileSearchResults: data.objects,
					fileSearchPdbId: data.objects.length > 0 ? data.objects[0].pdbid : ""
                });
			}
			content = (
				<div className="form-search">
					<ApiSearch
						apiEndpoint="primary_structures"
						onSearch={() => this.setState({ fileSearchLoading: true, hasSearched: true })}
						onSearchComplete={onComplete}
					/>
					{this.renderFileSearchResults()}
				</div>
			);
		}
        else if (this.state.fileInputMode === "searchPDB") {
            const onComplete = (pdb_id: any, errorMsg: string) => {
                this.setState({
                    fileSearchLoading: false,
                    fileSearchPdbId: pdb_id,
                    pdbSearchError: errorMsg,
                });
            }
            content = (
                <div className="form-search">
                    <PdbSearch
                        onSearch={() => this.setState({ fileSearchLoading: true, hasSearched: true })}
                        onSearchComplete={onComplete}
                        pdbSearchType={this.state.pdbSearchType}
                        biologicalAssemblyNumber={this.state.biologicalAssemblyNumber}
                    />
                    <select
                        name="pdbSearchType"
                        onChange={this.handlePdbSearchTypeChange}
                    >
                        <option value="asymmetric_unit">
                            Asymmetric Unit
                        </option>
                        <option value="biological_assembly">
                            Biological Assembly
                        </option>
                    </select>
                    {this.renderBiologicalAssemblyNumberInput()}
                    <br />
                    {this.renderFileSearchResults()}
                </div>
            );
        }

		return (
			<td className="file" colSpan={2}>
				<input
					type="radio"
					name="fileInputMode"
					id="fileUploadChoice"
					value="upload"
					checked={this.state.fileInputMode === "upload"}
					onChange={this.handleChange}
				/>
				{" "}
				<label className="description" htmlFor="fileUploadChoice">
                    Coordinate file
				</label>
				<br />
                (ex: xxxx.pdb; avoid additional periods, special symbols, and empty spaces in file
                name)
				<br />
				<input
					type="radio"
					name="fileInputMode"
					id="opmSearchChoice"
					value="searchOPM"
					checked={this.state.fileInputMode === "searchOPM"}
					onChange={this.handleChange}
				/>{" "}
				<label className="description" htmlFor="opmSearchChoice">
					Search for OPM entry
				</label>
				<br />
				<input
					type="radio"
					name="fileInputMode"
					id="pdbSearchChoice"
					value="searchPDB"
					checked={this.state.fileInputMode === "searchPDB"}
					onChange={this.handleChange}
				/>{" "}
				<label className="description" htmlFor="pdbSearchChoice">
					Search for PDB entry
				</label>
				<br />
				{content}
			</td>
		)
	}

    renderBiologicalAssemblyNumberInput() {
        if (this.state.fileInputMode != "searchPDB" || this.state.pdbSearchType != "biological_assembly") {
            return null;
        }

        let value: number | string = "";

        if (this.state.biologicalAssemblyNumber != null) {
            value = this.state.biologicalAssemblyNumber;
        }

        return (
            <input
                type="number"
                min="1"
                max="99"
                onChange={this.handleBiologicalAssemblyNumberChange}
                value={value}
            />
        );
    }

	renderContent() {
        const { computationServer, submitted } = this.props;
		if (submitted) {
			return (
				<Submitted
					submitAgain={this.reset}
					userEmail={this.state.userEmail}
				/>
			);
        }

        const useOtherServer = computationServer == "memprot"
            ? <div className="use-cgopm-server">
                <strong>Having trouble submitting?</strong> Try out the{" "}
                <Link to="/ppm_server2_cgopm">
                    cgopm-based version
                </Link>
            </div>
            : <div className="use-cgopm-server">
                <strong>Having trouble submitting?</strong> Try out the{" "}
                <Link to="/ppm_server2">
                    AWS-based version
                </Link>
            </div>;

        const submitDisabled = this.disableSubmit();
		return (
			<>
				<div className="ppm-introduction">
                    <h1>Positioning of proteins in a flat lipid bilayer</h1>
                    PPM 2.0 server features:
                    <ul>
                        <li>
                            calculations for flat lipid bilayers
                        </li>
                        <li>
                            using polarity profiles along the membrane normal for the DOPC
                            (1,2-dioleoyl-sn-glycero-3-phosphocholine) bilayer [
                            <a href="https://pubmed.ncbi.nlm.nih.gov/21438606/" target="_blank">
                                PubMed
                            </a>
                            ]
                        </li>
                        <li>
                            using adjustable hydrophobic thicknesses (30 ± 5 Å for α-helical
                            proteins, 25 ± 5 Å for β-barrels)
                        </li>
                        <li>
                            uploading coordinate file from your computer, from the Protein Data
                            Bank (PDB) or the OPM database.
                        </li>
                    </ul>
                    <p>The source code for the computational 
                        server can be found on <a href="https://cggit.cc.lehigh.edu/biomembhub/ppm2_server_code">GitLab</a>.
                    </p>
				</div>

				<table className="table-form">
					<thead className="table-header">
						<tr>
							<td colSpan={2}>
								Find Orientations of Proteins in Membranes (PPM 2.0)
							</td>
						</tr>
					</thead>
					<tbody>
						<tr className="radios">
							<td className="topology">
								<b>Topology (N-ter)</b>
								<br />
								<input
									type="radio"
									checked={this.state.topologyIn}
									onChange={() => this.setState({ topologyIn: true })}
								/>
								{" "}in
								<br />
								<input
									type="radio"
									checked={!this.state.topologyIn}
									onChange={() => this.setState({ topologyIn: false })}
								/>
								{" "}out
								<br />
                                (indicate topology of the N-terminus of the first chain in the PDB
                                file)
							</td>
							<td className="heteroatoms">
								<b>Include heteroatoms</b>
								<br />
								<input
									type="radio"
									checked={this.state.heteroatoms}
									onChange={() => this.setState({ heteroatoms: !this.state.heteroatoms })}
								/>
								{" "}yes
								<br />
								<input
									type="radio"
									checked={!this.state.heteroatoms}
									onChange={() => this.setState({ heteroatoms: !this.state.heteroatoms })}
								/>
								{" "}no
								<br />
                                (include heteroatoms for non-standard amino acid residues; water and
                                detergents are automatically excluded)
							</td>
						</tr>
                        <tr>
                            {this.renderFileInput()}
                        </tr>
						<tr>
							<td className="instructions" colSpan={2}>
								<div className="input-group email-input-div">
									<input
										type="text"
										className="search-bar form-control input-sm email-input"
										placeholder="email (optional)"
										value={this.state.userEmail}
										onChange={(e) => this.setState({ userEmail: e.target.value, error: false, errorText: "" })}
									/>
								</div>
								<p className="helper">
                                    (you will be notified by email when the calculations are
                                    complete)
							    </p>
                                <button
                                    title="reset"
                                    className="submit-button"
                                    onClick={() => {
                                        this.setState(getInitialState());
                                    }}
                                >
                                    Reset
                                </button>
                                <button
									title={submitDisabled ? "" : "submit"}
									className={submitDisabled ? "disabled-button" : "submit-button"}
									onClick={this.submit}
									disabled={submitDisabled}
								>
									Submit your query
								</button>
								<br />
								{this.state.error &&
									<span className="error-msg">
										{this.state.errorText}
									</span>
								}
							</td>
						</tr>
					</tbody>
				</table>
                <div className="bottom-buttons">
                    <button
                        title="instructions"
                        className="submit-button"
                        onClick={() => this.setState({instructions: !this.state.instructions})}
                    >
                        Instructions
                    </button>
                    <br />
                    <Link to="/ppm_server">
                        <button title="home">
                            Home
                        </button>
                    </Link>
                </div>
				{useOtherServer}
                {this.state.instructions ? <Instructions /> : undefined}
			</>
		);
	}

	render() {
        const { computationServer } = this.props;
		if (this.state.loading) {
			return (
				<div className="info-loading">
					<img className="loading-image" src={Assets.images.loading} />
				</div>
			);
		}

        const serverType = computationServer == "memprot"
            ? "AWS"
            : computationServer;

		return (
			<div className="ppm-server">
				<div className="server-header">PPM 2.0 Web Server ({serverType})</div>
				{this.renderContent()}
			</div>
		);
	}
}

export default connector(PPMServer);
