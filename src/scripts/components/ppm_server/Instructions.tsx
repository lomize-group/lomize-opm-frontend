import * as React from 'react';
import { Link } from 'react-router-dom';
import './Instructions.scss';



const Instructions: React.FC = () => {
	return (
		<div className="instructions-paragraphs">
			<div>
				<h1>Method</h1>
				<p>
					The server implements our new PPM 2.0 method, which defines spatial positions of
					molecules in membranes by optimizing their transfer energy from water to the lipid
					bilayer (Lomize et al., <em>J. Chem. Inf. Model.,</em> 2011, <b>51:</b> 930-946
					<a href="http://www.ncbi.nlm.nih.gov/pubmed/21438606" target="_blank">
						{" "}PubMed
					</a>
					).
					It can be used for alpha-helical and beta-barrel transmembrane proteins, peripheral
					proteins, and peptides, including those with non-standard amino acids. The method
					accounts for long-range electrostatic interactions, first-shell solvation energy
					(van der Waals, hydrophobic and hydrogen bond interactions), the gradual polarity
					changes along the bilayer normal, the preferential solvation of protein groups by
					water, and the hydrophobic mismatch for TM proteins.
					<br />
					<br />
					The method was tested on several dozens of experimentally studied proteins.
					The spatial positions of TM proteins are determined with precision of spatial
					positions of TM proteins of ~1 &#x01FA; for the hydrophobic thickness, ~2&deg;
					for the tilt angle relative to the membrane normal, and ~1 kcal/mol for the membrane
					binding energy of peripheral proteins.
				</p>
			</div>

			<div>
				<h1>Input information</h1>
				<p>
					Topology information ("in" or "out", see
					<Link to="/about#topology">
						{" "}topology definitions
					</Link>
					) serves only to color DUMMY atoms and define the sign of Z coordinates of atoms
					("-" corresponds to "in" side).
					<br />
					<br />
					Heteroatoms must be included for peptides with non-standard amino acid residues
					and may be included for complexes with lipids or large nonpolar cofactors.
				</p>
			</div>

			<div>
				<h1>Tips and notes</h1>
				<div>
					This version of the server does not provide reconstruction of missing side-chain
					atoms, optimization of side-chain conformers at the lipid-water interface, or
					prediction of protein topology. A user is advised to:
					<ul>
						<li>
							provide structure in a biologically relevant oligomeric state
						</li>
						<li>
							reconstruct missing side-chain atoms that may interact with lipids
						</li>
						<li>
							remove His tags or other artificial chemical modifications
						</li>
						<li>
							remove disordered loops with undefined spatial positions in NMR models
						</li>
					</ul>
					Results of calculations for peripheral proteins with transfer energies smaller than
					-5 kcal/mol must be interpreted with caution. In this case results would indicate the
					presence of a hydrophobic surface that may be involved in either protein-lipid or
					protein-protein interaction. The results for original PDB files may differ from those
					in the OPM database due to different oligomeric states, choice of an NMR model,
					reconstructions of missing side-chain atoms and side-chain optimization in many OPM
					entries, etc.
				</div>
			</div>

			<div>
				<h1>Output</h1>
				<p>
					<b>Depth or hydrophobic thickness.</b> This parameter indicates the calculated
					hydrophobic thickness (for TM proteins) or maximal penetration depth of protein
					atoms into the lipid hydrocarbon core (for peripheral/monotopic) proteins. The
					&plusmn; values for the depth and tilt angle show fluctuations of the corresponding
					parameters within 1 kcal/mol around the global minimum of transfer energy.
					<br />
					<br />
					<b>Tilt angle</b> is calculated between membrane normal (Z axis) and protein axis.
					The protein axis is calculated as the sum of TM secondary structure segment vectors
					(for TM proteins) or as the principal inertia axis (for peripheral proteins).
					<br />
					<br />
					<b>Transfer energy of the protein from water to lipid bilayer.</b> This energy
					roughly corresponds to the actual membrane binding energy for water-soluble
					peripheral proteins, unless: (a) some membrane-anchoring elements are missing or
					disordered in the crystal structure; (b) there is strong specific binding of lipids
					(e.g. in PH domains and other "lipid clamps"), or (c) membrane binding is coupled
					with significant structural changes of the protein (e.g. helix-coil transition for
					amphiphilic &alpha;-helices). In situations (a) and (b), the calculated membrane
					binding free energy is underestimated. In situation (c) it is usually overestimated.
					<br />
					<br />
					<b>Table of membrane-embedded residues</b> consists of two parts: (a) list of
					residues penetrating into the hydrocarbon core of the lipid bilayer for each subunit
					(tilt angles of individual subunits in this part are calculated based on the
					principal inertia axis), and (b) parts of transmembrane alpha-helices or beta-strands
					that are embedded into the hydrocarbon core (the entire secondary structures can be
					longer; tilt angles of individual subunits in this part are calculated as vector
					averages of TM secondary structure segment vectors).
					<br />
					<br />
					<b>Output coordinate file for a protein positioned in the lipid bilayer.</b> The origin
					of coordinates corresponds to the center of lipid bilayer. Z axis coincides with membrane
					normal; atoms with the positive sign of Z coordinate are arranged in the "outer" leaflet
					as defined by the user-specified topology. Positions of DUMMY atoms correspond to
					locations of lipid carbonyl groups.
					<br />
					<br />
					<b>Diagnostic messages.</b> The server produces diagnostic messages in a separate
					window. Please report these messages to developer if program aborts. If the program
					does not abort, you can ignore the messages because the results of calculations were
					not affected. These are typically warnings about certain inconsistencies in the
					input PDB files.
				</p>
			</div>

			<div id="contact">
				<h1>Contact</h1>
				<p>
					Please send any questions or requests to Andrei Lomize (
					<a href="mailto:lomizeal@gmail.com">lomizeal@gmail.com</a>
					)
				</p>
			</div>

            <div>
                <h1>Source code</h1>
				<p>PPM&nbsp;
                    <a href="https://console.cloud.google.com/storage/browser/opm-assets/ppm_code">
                        source code
                    </a> for multiple input PDB files
                </p>
            </div>
		</div>
	);
}

export default Instructions;
