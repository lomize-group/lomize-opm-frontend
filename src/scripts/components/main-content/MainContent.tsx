import * as React from "react";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import "./MainContent.scss"
import { RootState } from "../../store";

class MainContent extends React.Component
{
	render(): React.ReactElement
	{
		return(
			<div className="main-content">
				{this.props.children}
			</div>
		);
	}
}

function mapStateToProps(state: RootState) {
  return {};
}

function mapDispatchToProps(dispatch: any) {
	return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MainContent);
