import * as React from "react";
import { Link } from "react-router-dom";
import "./Introduction.scss";

const Introduction: React.FC = () => (
    <div className="introduction normal-info-page">
        <h1>Introduction</h1>
        <hr />
        <p>
            There are several thousand PDB entries with structures of transmembrane, integral
            monotopic and peripheral peptides and proteins. However, the exact orientations of these
            peptides and proteins in biological membranes is unknown. A computational approach for
            optimizing the spatial arrangement of protein structures in lipid bilayers has been
            developed and applied here to all unique structures of transmembrane proteins and a
            significant number of peripheral proteins from the PDB.
        </p>

        <p>
            We hope that OPM will be helpful for planning future experimental and computational
            studies of membrane proteins and for interpreting their structural, functional and
            evolutionary features.
        </p>

        <p>
            Please feel free to <Link to="/contact">contact us</Link>.
        </p>
    </div>
);

export default Introduction;
