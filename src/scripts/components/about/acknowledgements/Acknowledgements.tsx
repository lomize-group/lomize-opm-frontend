import * as React from "react";
import ExternalLink from "../../external_link/ExternalLink";
import "./Acknowledgements.scss";

const Acknowledgements: React.FC = () => (
    <div className="acknowledgements normal-info-page">
        <h1>Acknowledgements</h1>
        <hr />
        <p>OPM was developed using the following databases, servers and software:</p>
        <ul>
            <li>
                <ExternalLink href="http://www.rcsb.org/pdb/">PDB</ExternalLink>, Protein Data Bank
            </li>
            <li>
                <ExternalLink href="http://scop.mrc-lmb.cam.ac.uk/scop/">SCOP</ExternalLink>,
                Structural Classification Of Proteins
            </li>
            <li>
                <ExternalLink href="http://pqs.ebi.ac.uk/">PQS</ExternalLink>, Protein Quaternary
                Structure server{" "}
            </li>
            <li>
                <ExternalLink href="http://www.ebi.ac.uk/msd-srv/ssm/">SSM</ExternalLink>, Secondary
                Structure Matching server
            </li>
            <li>
                <ExternalLink href="http://www.pir.uniprot.org/">UniProt</ExternalLink>, Universal
                Protein resource
            </li>
            <li>
                <ExternalLink href="http://pfam.wustl.edu/">Pfam</ExternalLink>, Protein Families
                database
            </li>
            <li>
                <ExternalLink href="http://blanco.biomol.uci.edu/mptopo/">MPtopo</ExternalLink>,
                Membrane Protein Topology database
            </li>
            <li>
                <ExternalLink href="http://bioinfo.si.hirosaki-u.ac.jp/~TMPDB/">TMPDB</ExternalLink>
                , collection of TM proteins in PDB
            </li>
            <li>
                <ExternalLink href="http://pdbtm.enzim.hu/">PDBTM</ExternalLink>, collection of TM
                proteins in PDB
            </li>
            <li>
                <ExternalLink href="http://www.tcdb.org/">TCDB</ExternalLink>, Transport
                Classification Database
            </li>
            <li>
                NACCESS, a program for calculating solvent accessible surface areas of proteins,
                Hubbard S.J., Thornton J.M. (1993), Department of Biochemistry and Molecular Biology
                , University College London, UK.
            </li>
        </ul>
        <p>
            We are grateful to Dr. Henry I. Mosberg for scientific and administrative support, Drs.
            Aleksey Murzin and Kim Henrick for discussion of protein evolution and quaternary
            structure, Drs. Eugene Krissinel and Gabor Tusnady for helpful explanations of SSM and
            PDB_TM, and Drs. Simon Hubbard, Vladimir Maiorov, and Simon Sherman for provided
            software.
        </p>
    </div>
);

export default Acknowledgements;
