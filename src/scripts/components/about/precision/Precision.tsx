import * as React from "react";
import { Assets } from "../../../config/Assets";
import ImgWrapper from "../../img_wrapper/ImgWrapper";
import "./Precision.scss";

const Precision: React.FC = () => (
    <div className="precision normal-info-page">
        <h1>Precision</h1>
        <hr />
        <p>
            The precision of calculated hydrophobic thicknesses and tilt angles are ~1 &Aring; and
            2&deg;, respectively, as judged from their deviations in different crystal forms of the
            same proteins. The fluctuations of these parameters calculated within 1 kcal/mol around
            the global minimum of transfer energy are usually smaller than 2 &Aring; and 4&deg;,
            respectively (&plusmn; values in all Tables of the database). The calculated tilt angles
            in homologous proteins differ by 2-16&deg; depending on the size of the protein, its
            oligomeric state and percentage of sequence identity.
        </p>
        <ImgWrapper
            src={Assets.images.directories.bio + "precision.jpg"}
            className="orientation-img"
        />
        <p>
            <b>Figure 2</b>. Deviations of calculated tilt angles in different crystal forms of the
            photosynthetic reaction centers from <i>Rhodobacter sphaeroides</i> (A), or in
            homologous photoreaction centers from different bacteria (B).
        </p>
    </div>
);
export default Precision;
