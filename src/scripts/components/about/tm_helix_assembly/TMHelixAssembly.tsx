import * as React from "react";
import { Assets } from "../../../config/Assets";
import ImgWrapper from "../../img_wrapper/ImgWrapper";
import "./TMHelixAssembly.scss";

const TMHelixAssembly: React.FC = () => (
    <div className="tm-helix-assembly normal-info-page">
        <h1>TM Helix Assembly</h1>
        <hr />
        <p>
            Each OPM assembly page for a transmembrane (TM) protein or a subunit of protein complex
            includes the following:
        </p>
        <p>
            (a) a table with calculated free energies of pairwise association of TM α-helices in the
            experimental 3D structure (at the bottom of the page); (b) a tentative pathway of
            assembly of TM α-helices into the native structure (all TM helices are numbered
            sequentially and included in “assembly equations” to show the order of TM helix
            association); (c) a downloadable coordinate file with probable folding intermediates
            included as multiple MODELs. The structures of probable folding intermediates are
            visualized by GLMol.
        </p>
        <p>
            Helix association energy,{" "}
            <i>
                ΔG<sub>asc</sub>
            </i>
            , is calculated as a sum of the following contributions: (a) environment- dependent
            energies of van der Waals interactions and H-bonds; (b) solvation energy changes during
            transfer of different protein groups from membrane to the protein interior; (c) changes
            in the conformational entropy of side chains during helix association; and (d) energy of
            electrostatic interactions of α-helical “macrodipoles” in the nonpolar environment.
        </p>
        <p>
            Based on the calculated energies, TMPfold produces the tentative assembly path of TM
            helices during co-translational protein folding. The “assembly equation” is produced
            based on the following rules: (a) TM α-helices insert into the membrane sequentially;
            (b) addition of each helix represents a separate step of the process; (c) the newly
            synthesized TM α-helix associates stably with the previously inserted individual TM
            α-helices or larger nuclei if the gain in{" "}
            <i>
                ΔG<sub>asc</sub>
            </i>{" "}
            value exceeds a cutoff of several kcal/mol; and (d) sequentially adjacent structures
            associate first if several alternative folding events satisfy the cutoff.
        </p>
        <p>
            The TM protein folding usually starts from the formation of nuclei of sequentially
            adjacent strongly interacting TM α-helices (e.g. α-hairpins) and followed by their
            growth (attachment of single TM α- helices or α-hairpins) and association.
        </p>
        <div className="tm-folds-img">
            <ImgWrapper src={Assets.images.directories.bio + "Folds7TMnew.jpg"} />
            <p>
                Different folds of 7 TM-helical membrane proteins. Helix arrangements (from the
                extracellular membrane side) are shown for representatives of 10 families of 7
                TM-helical membrane proteins. Numbers near bars indicate pairwise helix association
                energies, “folding pathway equations” are shown below each picture of TM helix
                arrangement (represented by cartoons with rainbow colors).
            </p>
        </div>
    </div>
);

export default TMHelixAssembly;
