import * as React from "react";
import { Assets } from "../../../config/Assets";
import ImgWrapper from "../../img_wrapper/ImgWrapper";
import "./MethodsDefinition.scss";

const MethodsDefinition: React.FC = () => (
    <div className="methods-definition normal-info-page">
        <h1>Methods and Definitions</h1>
        <hr />
        <h2>Transmembrane proteins</h2>
        <p>
            Each protein is considered as rigid body that freely floats in a hydrophobic slab of
            adjustable thickness.
        </p>
        <p>
            Orientation of the protein was determined by minimizing its transfer energy,{" "}
            <b>
                ΔG<sub>transfer</sub>
            </b>
            , with respect to <b>d</b>,
            <b>
                z<sub>0</sub>
            </b>
            , <b>Θ</b> and <b>φ</b> variables in a coordinate system whose axis Z coincides with the
            bilayer normal:
        </p>
        <ImgWrapper
            src={Assets.images.directories.bio + "orientation.jpg"}
            className="orientation-img"
        />
        <div className="caption">
            <b>Figure 1</b>. Schematic representation of a transmembrane protein in a hydrophobic
            slab.
            <b>
                <br />
                <i>d</i>
            </b>
            , shift along the bilayer normal;
            <b>
                {" "}
                <i>D</i>
            </b>
            , hydrophobic thickness (
            <i>
                D=2z<sub>0</sub>
            </i>
            ); <b>φ</b>, rotation angle;
            <b>τ</b>, tilt angle.
        </div>
        <p>
            Longitudinal axes of TM proteins are calculated as vector averages of TM segment
            vectors.
        </p>
        <h2>Peripheral/Monotopic proteins</h2>
        <p>
            These proteins do not span the membrane. Maximal membrane penetration depths (D) is used
            instead of the hydrophobic thickness. The longitudinal axis is defined as an axis that
            provides minimal moment of inertia. Transfer energies of peripheral proteins are
            calculated without considering ligands, except for lipid clamps, photosynthetic reaction
            centers and light-harvesting complexes.
        </p>
        <h2>Anisotropic Solvent Model of the Lipid Bilayer</h2>
        <ImgWrapper src={Assets.images.directories.math + "Fig2a.gif"} className="fig-img" />
        <br />
        <ImgWrapper src={Assets.images.directories.math + "Fig2b.gif"} className="fig-img" />
        <br />
        <p>
            <b>Figure 2.</b> <b>(A)</b> Profiles of hydrogen bonding donor (&alpha;) and acceptor
            (&beta;) capacity, solvatochromic dipolarity/polarizability parameter (&pi;*),
            dielectric constant (&epsilon;), dielectric function F(&epsilon;), and volume fraction
            of non-polar media (HDC) along the membrane normal. Polarity parameters were calculated
            using distributions along the membrane normal of volume fractions of lipid components
            obtained by X-ray and neutron scattering for fluid DOPC bilayer (<b>B</b>, Kucerka et
            al., 2008 Biophys. J. 95: 2356-2367).
            <br /> <br />
        </p>
        <p>
            Membrane interface or "midpolar region" (8-20&#197; from the membrane center) is
            characterized by the steep gradient of all polarity parameters whose values depend on
            lipid composition.
        </p>
        <p>
            Transfer free energy of the molecule from water to the lipid bilayer is calculated in
            accordance with a new solvation model developed for the anisotropic environment (i.e.
            the lipid bilayer):
        </p>
        <ImgWrapper src={Assets.images.directories.math + "Eq1.gif"} className="fig-img" />
        <br />
        where &sigma;<sub>i</sub>
        <sup>wat&rarr;bil</sup> is an atomic solvation parameter of atom type i (expressed in cal
        mol<sup>-1</sup>&#197;<sup>-2</sup>), ASA<sub>i</sub> is the solvent accessible surface area
        of atom i, &eta;<sup>S&rarr;wat</sup> is a dipolar solvation parameter (expressed in cal mol
        <sup>-1</sup>D<sup>-1</sup>), &eta;<sub>l</sub>
        is a dipole moment of a group <i>l</i>. ASA-dependent first-shell contribution to the
        transfer free energy (vdW, H-bonds, solvent entropy) is described by the dependence of
        atomic solvation parameters &sigma;<sub>i</sub> on macroscopic dielectric constant of a
        solvent (&epsilon;), solvent hydrogen bonding acidity (&alpha;) and basicity (&beta;)
        parameters:
        <br />
        <ImgWrapper src={Assets.images.directories.math + "Eq2.gif"} className="fig-img" />
        <br />
        Long-range electrostatic contribution to the transfer free energy is described by the
        dependence of dipolar solvation parameters &eta;
        <sup>
            S&rarr;wat<sub>i</sub>
        </sup>{" "}
        on macroscopic solvatochromic dipolarity/polarizability parameter &pi;*:
        <br />
        <ImgWrapper src={Assets.images.directories.math + "Eq3.gif"} className="fig-img" />
        Energy of an ionizable group in neutral state is described as:
        <br />
        <ImgWrapper src={Assets.images.directories.math + "Eq4.gif"} className="fig-img" />
        <br />
        <br />
        <ImgWrapper src={Assets.images.directories.math + "Eq5.gif"} className="fig-img" />
        <br />
        Born electrostatic energy for ions:
        <br />
        <ImgWrapper src={Assets.images.directories.math + "Eq6.gif"} className="fig-img" />
        <br />
        <br />
        <ImgWrapper src={Assets.images.directories.math + "Eq7.gif"} className="fig-img" />
        <br />
        The preferential solvation of protein groups in water-lipid mixture was also taken into
        account.
        <h2>For more information on peripheral proteins please refer to</h2>
        <p>Lomize AL, Pogozheva ID, Lomize MA, Mosberg HI (2007) The role of hydrophobic interactions in positioning of peripheral proteins in membranes.
            <i>BMC Struct Biol.</i>
            <b>7</b>, 44
            [
            <a href="https://pubmed.ncbi.nlm.nih.gov/17603894/" target="_blank">
                PubMed
            </a>
            ]
        </p>
        <p>
            Lomize AL, Todd SC, Pogozheva ID. (2022) Spatial arrangement of proteins in planar and curved membranes by PPM 3.0.
            <i> Protein Sci.</i>
            <b> 31</b>
            :209-220. [
            <a href="https://pubmed.ncbi.nlm.nih.gov/34716622/" target="_blank">
                PubMed
            </a>
            ]
        </p>
    </div>
);

export default MethodsDefinition;
