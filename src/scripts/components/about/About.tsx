import * as React from "react";
import "./About.scss";
import Acknowledgements from "./acknowledgements/Acknowledgements";
import Classification from "./classification/Classification";
import Features from "./features/Features";
import Introduction from "./introduction/Introduction";
import MethodsDefinition from "./methods_definition/MethodsDefinition";
import Precision from "./precision/Precision";
import TMHelixAssembly from "./tm_helix_assembly/TMHelixAssembly";
import Topology from "./topology/Topology";

function getComponentFromUrlHash(): string {
    const hash = window.location.hash.replace("#", "");
    if (hash === "") {
        return "introduction";
    }
    return hash === "" ? "introduction" : hash;
}

function renderComponent(): JSX.Element {
    switch (getComponentFromUrlHash()) {
        case "introduction":
            return <Introduction />;
        case "features":
            return <Features />;
        case "methods_and_definitions":
            return <MethodsDefinition />;
        case "precision":
            return <Precision />;
        case "classification":
            return <Classification />;
        case "topology_definitions":
            return <Topology />;
        case "tm_helix_assembly":
            return <TMHelixAssembly />;
        case "acknowledgements":
            return <Acknowledgements />;
        default:
            return <Introduction />;
    }
}

const About: React.FC = () => (
    <div className="about">
        <div className="nav-panel">
            <a href="#introduction">Introduction</a>
            <hr />
            <a href="#features">Features</a>
            <hr />
            <a href="#methods_and_definitions">Methods and Definitions</a>
            <hr />
            <a href="#precision">Precision</a>
            <hr />
            <a href="#classification">Classification</a>
            <hr />
            <a href="#topology_definitions">Topology Definitions</a>
            <hr />
            <a href="#tm_helix_assembly">TM Helix Assembly</a>
            <hr />
            <a href="#acknowledgements">Acknowledgements</a>
        </div>
        {renderComponent()}
    </div>
);

export default About;
