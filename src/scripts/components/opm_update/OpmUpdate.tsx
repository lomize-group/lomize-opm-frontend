import * as React from "react";
import axios from "axios";
import { Urls } from "../../config/Urls";
import { Assets } from "../../config/Assets";

type OpmUpdateState = {
    pdbList: File | null,
    errors: string[],
    loading: boolean,
    resultsUrl: string | null,
};

const getInitialState = (): OpmUpdateState => ({
    pdbList: null,
    errors: [],
    loading: false,
    resultsUrl: null,
});

export default class OpmUpdate extends React.Component<{}, OpmUpdateState> {
    constructor(props: {}) {
        super(props);
        this.state = getInitialState();
        this.submit = this.submit.bind(this);
        this.handlePdbListFileChange = this.handlePdbListFileChange.bind(this);
    }

    handlePdbListFileChange(e: React.ChangeEvent<HTMLInputElement>) {
        const files = e.target.files;
        this.setState({
            pdbList: files ? files[0] : null,
        });
    }

    submit() {
        const { pdbList, errors } = this.state;

        if (pdbList === null) {
            this.setState({
                errors: ["Must upload a pdb list"],
            });
            return;
        }

        if (errors.length !== 0) {
            this.setState({ errors: [] });
        }

        this.setState({ loading: true });
        const form = new FormData();
        form.append('pdb_list', pdbList);
        axios.post(Urls.api_base + "/opm_update", form)
        .then(res => {
            this.setState({ resultsUrl: res.data.results });
            this.waitForResults();
        });
    }

    waitForResults() {
        const timeout = 5 * 1000; // 5 seconds
        const { resultsUrl } = this.state;
        if (resultsUrl === null) {
            console.error("results url was null when waiting for results");
            this.setState({ errors: ["Results url was null when waiting for results"] });
            setTimeout(this.waitForResults, timeout);
        }
        axios.head(Urls.api_base + resultsUrl!)
        .then(res => {
            if (res.status === 202) {
                // retry
                this.setState({ errors: [] });
                setTimeout(() => this.waitForResults(), timeout);
            } else if (res.status === 200) {
                this.setState({
                    errors: [],
                    loading: false,
                });
                alert("Your OPM update job has finished");
            } else {
                console.error(`unknown status: ${res.status}`);
                this.setState({ errors: [`unknown status: ${res.status}`] });
                return;
            }
        })
    }

    render() {
        const { errors, pdbList, loading, resultsUrl } = this.state;
        let errorsHtml = null;
        if (errors.length !== 0) {
            errorsHtml = (<div>
                {errors.map(error => <div>{error}</div>)}
            </div>);
        }
        const formHtml = (<div>
            <div>
                <label htmlFor="pdb-list">PDB List</label>
                <input
                    type="file"
                    name="pdb_list"
                    id="pdb-list"
                    required
                    onChange={this.handlePdbListFileChange}
                />
            </div>
            <div>
                <input
                    type="submit"
                    name="submit"
                    value="submit"
                    onClick={this.submit}
                />
            </div>
        </div>);
        const loadingHtml = <img className="loading-image" src={Assets.images.loading}/>;
        const contentHtml = loading ? loadingHtml : formHtml;
        let downloadHtml = null;
        if (!loading && resultsUrl !== null) {
            downloadHtml = (<a
                href={Urls.api_base + resultsUrl}
            >
                Download results
            </a>);
        }
        return (
            <div>
                <div className="opm-update">OPM Update</div>
                {contentHtml}
                {downloadHtml}
                {errorsHtml}
            </div>
        );
    }
}
