import * as React from 'react';
import * as PropTypes from 'prop-types';
import axios from 'axios';

import { Assets } from "../../config/Assets";
import { Urls } from "../../config/Urls";
import ClassificationDef from "../../config/Classification";
import Columns from "../table/Columns";

import { capitalize } from '../../config/Util';

import { connect } from 'react-redux';

import Info from '../info/Info.js';
import List from '../list/List.js';
import Table from '../table/Table.js';

import './Classification.scss';

class Classification extends React.Component
{
	static propTypes = {
		has_list: PropTypes.bool,
		has_table: PropTypes.bool,
		ctype: PropTypes.string.isRequired,
	};

	static defaultProps = {
		has_list: false,
		has_table: false
	};

	constructor(props)
	{
		super(props);

		this.state = {
			resize: false
		};

		this.onResize = () => {
			if (this.hasOwnProperty('table') && this.table !== undefined && this.table !== null){
				this.table.updateSize();
			}
		}

		const createParentInfo = cur_props => {
			const { ctype, currentUrl } = cur_props
			const ctype_def = ClassificationDef.definitions[ctype];

			// fetch data here
			const fetch_url = Urls.api_url(ctype_def.api.route + '/' + currentUrl.params.id);
			axios.get(fetch_url)
			.then(res => {
				let newstate = {
					loading: false,
					header: res.data.name,
					info: {
						base: ctype,
						[ctype]: {
							id: res.data.id,
							name: res.data.name,
							subclasses: res.data[ClassificationDef.definitions[ctype_def.child].api.accessor.count],
							pfam: res.data.pfam,
                            interpro: res.data.interpro,
							tcdb: res.data.tcdb
						}
					}
				}

				// recursively create info area for all parents of the selected classification
				function createInfo(cur_type, cur_obj) {
					const cur_ctype_def = ClassificationDef.definitions[cur_type];
					if (cur_ctype_def.hasOwnProperty('parents')) {
						for (let i = 0; i < cur_ctype_def.parents.length; i++) {
							const parent = cur_ctype_def.parents[i];
							const parent_obj = cur_obj[ClassificationDef.definitions[parent].api.accessor.object];
							newstate.info[parent] = {
								id: parent_obj.id,
								name: parent_obj.name,
								subclasses: parent_obj[cur_ctype_def.api.accessor.count],
								pfam: parent_obj.pfam,
                                interpro: parent_obj.interpro,
								tcdb: parent_obj.tcdb
							};
							// recurse with cur_type = parent, cur_obj = parent_obj
							createInfo(parent, parent_obj);
						}
					}
				}

				createInfo(ctype, res.data);
		        this.setState(newstate);
            }).catch((error) => {
                console.log(error);
                this.setState({
                    loading: false,
                    header: 'Error',
                    error,
                });
            });

		    let return_state = {
				loading: true,
				header: 'Loading...'
			};
		    return return_state;
		}

		const fetchForTable = (cur_props, category, is_singular) => {
			const { ctype, currentUrl } = cur_props;
			const ctype_def = ClassificationDef.definitions[ctype];
			let return_state = {};

			if (is_singular) {
				const category_def = ClassificationDef.definitions[category];
				const cols = category === "proteins" ?
							Columns.proteins.without(ctype_def.column) :
							Columns.assemblies.filterByPage(ctype);

				return_state.tableData = {
					title: capitalize(category_def.name),
					url: Urls.api_url(ctype_def.api.route + "/" + currentUrl.params.id + category_def.api.route),
					columns: cols.columns,
					redirect: false
				};
			}
			else {
				return_state.tableData = {
					title: "All " + ctype + " in OPM",
					url: Urls.api_url(ctype_def.api.route),
					columns: Columns[ctype].filterByPage(ctype).columns,
					search: currentUrl.query.search,
				};
			}

			return return_state;
		}

		const fetchForList = (cur_props, is_singular) => {
			const { ctype, currentUrl } = cur_props;
			const ctype_def =  ClassificationDef.definitions[ctype];
			let return_state = {};

			if (is_singular) {
				// list uses child of current ctype
				const ctype_child_def = ClassificationDef.definitions[ctype_def.child];
				const subType = ctype_child_def.child;
				
				// list shows the child, so the title will be the child's name
				return_state.listHeader = capitalize(ctype_child_def.name);
				return_state.listData = {
					url: Urls.api_url(ctype_def.api.route + "/" + currentUrl.params.id + ctype_child_def.api.route),
					ctype: ctype_def.child,
					sub_type: subType,
					expandable_items: subType !== "assemblies"
				};
			}
			else {
				return_state.listHeader = capitalize(ctype_def.name);
				return_state.listData = {
					url: Urls.api_url(ctype_def.api.route),
					ctype: ctype,
					sub_type: ctype_def.child
				};
			}
			return return_state;
		}

		this.fetch = cur_props => {
			let return_state = {};
			const category = ClassificationDef.proteins.includes(cur_props.ctype) ? "proteins" : "assemblies";
			const is_singular = cur_props.currentUrl.params.hasOwnProperty("id");

			// a parent-type classification type has been selected
			if (is_singular) {
				return_state = createParentInfo(cur_props);
			}

			// only a table
			if (cur_props.has_table) {
				return_state = Object.assign(return_state, fetchForTable(cur_props, category, is_singular));

				// for assemblies - sort by 1st column by default
				if (category === "assemblies") {
					return_state.tableData.sort = return_state.tableData.columns[0].id
				}
			}

			// only a list
			if (cur_props.has_list) {
				return_state = Object.assign(return_state, fetchForList(cur_props, is_singular));
			}

			return return_state;
		}

		this.state = this.fetch(props);
	}

	componentWillReceiveProps(nextProps)
	{
		this.setState(this.fetch(nextProps));
	}

	renderClassification()
	{
		let items = [];

		// add the header/listheader to the page if there is one
		if (this.state.hasOwnProperty('header')){
			items.push(
				<div key={0} className="page-header">
					{ this.state.header }
				</div>
			);
		}
		else if (this.state.hasOwnProperty('listHeader')) {
			items.push(
				<div key={0} className="page-header">
					{ this.state.listHeader }
				</div>
			);
		}


		// add parent info to page if parent info should be displayed
		if (this.state.hasOwnProperty('info')) {
			items.push(
				<div key={1}>
					<Info onResize={this.onResize} {...this.state.info} />
				</div>
			);
		}
		else if (this.state.loading) {
			items.push(
				<div className="info-loading" key={1}>
					<img className="loading-image" src={Assets.images.loading} />
				</div>
			);
		}
        else if (this.state.error !== undefined) {
            items.push(
                <div key={1}>
                    {`There was an error getting ${this.state.error.config.url}`}
                </div>
            );
        }

		// add list to page if there should be one
		if (this.state.hasOwnProperty('listData')) {
			if (this.state.hasOwnProperty('header')) {
				items.push(
					<div key={2} className="list-section">
						<div className="list-header">
							{ this.state.listHeader }:
						</div>
						<List
							has_table={this.state.hasOwnProperty('tableData')}
							onResize={this.onResize}
							{...this.state.listData} />
					</div>
				);
			}
			else {
				items.push(
					<div key={2} className="list-section">
						<List
							has_table={this.state.hasOwnProperty('tableData')}
							{...this.state.listData}
						/>
					</div>
				);
			}
		}

		// add table to page if there should be one
		if (this.state.hasOwnProperty('tableData')) {
			items.push(
				<div key={3} className="table-section">
					<Table ref={ref => this.table = ref } {...this.state.tableData} />
				</div>
			);
		}

		return items;
	}

	render()
	{
		return(
			<div className="classification">
				{this.renderClassification()}
			</div>
		);
	}
}


function mapStateToProps(state) {
  return {
		currentUrl: state.currentUrl,
	};
}

export default connect(mapStateToProps)(Classification);
