import * as React from "react";

import "./ExternalLink.scss";


interface OptionalExternalLinkProps {
	href: string
};

export interface ExternalLinkProps extends OptionalExternalLinkProps {};

export default class ExternalLink extends React.Component<ExternalLinkProps>
{
	static defaultProps: OptionalExternalLinkProps = {
		href: ""
	};

	render(): React.ReactElement
	{
		return(
			<a className="external-link" target="_blank" href={this.props.href}>
				{this.props.children}
			</a>
		);
	}
}
