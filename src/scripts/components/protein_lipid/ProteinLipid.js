import * as React from "react";
import * as PropTypes from "prop-types";
import { connect } from "react-redux";
import axios from "axios";
import * as appendQuery from "append-query";

import GLmolViewer from "../visualizers/glmol/GLMOL.js";
import LiteMolViewer from "../visualizers/litemol/LiteMol.js";
import Table from "../table/Table.js";

import { Urls } from "../../config/Urls";
import { Assets } from "../../config/Assets";
import Classification from "../../config/Classification";
import Columns from "../table/Columns";

import PubMedLink from "../pubmed_link/PubMedLink";

import "./ProteinLipid.scss";

class ProteinLipid extends React.Component
{
	constructor(props)
	{
		super(props);

		this.state = {
			pdbid: undefined,
			name: undefined,
			membrane: undefined,
			pdb_lipid_url: undefined,
			protein_id: props.currentUrl.params.id,
			viewer: "litemol",
			loaded: false
		};

		this.handle_change = event => this.setState({ [event.target.name]: event.target.value });

		this.fetch = () => {
			axios.get(`${Urls.api_base}/primary_structures/${this.state.protein_id}`)
			.then(res => {
				let { membrane } = res.data;
				membrane.lipid_pubmed = membrane.lipid_pubmed.split(";");

				this.setState({
					pdbid: res.data.pdbid,
					name: res.data.name,
					membrane,
					pdb_lipid_url: Urls.pdb_lipid_file(res.data.pdbid),
					loaded: true
				});
			});
		}

		this.fetch();
	}

	renderViewer()
	{
		switch(this.state.viewer) {
			case "litemol":
				return <LiteMolViewer url={this.state.pdb_lipid_url} />;
			case "glmol":
				return <GLmolViewer
						url={this.state.pdb_lipid_url}
						type="protein_lipid"
						width="100%"
						maxWidth="100%"
						useFullWidth={true}
						color_by_default="chain"
						show_sidechains_default={false}
						show_header={false}
					/>;
			default:
				break;
		}

		return null;
	}

	renderLipidComposition(title, inner_outer)
	{
		const url = Urls.api_base + Classification.definitions.proteins.api.route + "/" + this.state.protein_id + "/membrane_lipids";
		const params = {
			leaflet: inner_outer
		};

		return(
			<div className="composition-table">
				<Table
					url={appendQuery(url, params)}
					title={title}
					title_size={"small"}
					show_row_count={false}
					show_search={false}
					redirect={false}
					columns={Columns.lipids.columns}
					header_height={45}
					row_height={35}
					sort="id"
				/>
			</div>
		);
	}

	renderLipidPubmed()
	{
		let items = [];
		for (let i = 0; i < this.state.membrane.lipid_pubmed.length; ++i) {
			const pmid = this.state.membrane.lipid_pubmed[i];
			items.push(
				<span key={i}>
					<PubMedLink id={pmid}>{pmid}</PubMedLink>
					{i < this.state.membrane.lipid_pubmed.length - 1 && ", "}
				</span>
			);
		}
		return items;
	}

	renderTableRow(right, left, attr)
	{
		return(
			<tr className={attr}>
				<td className="right"><b>{right}</b></td>
				<td className="left">{left}</td>
			</tr>
		);
	}

	render()
	{
		if (!this.state.loaded) {
			return(
				<div className="info-loading">
					<img className="loading-image" src={Assets.images.loading}/>
				</div>
			);
		}

		return(
			<div className="protein-lipid">
				<div className="page-header">
					{this.state.pdbid} with explicit lipids
				</div>
				Visualize with:
				<div className="visualizer-choices">
					<input
						type="radio"
						id="litemol_choice"
						name="viewer"
						value="litemol"
						checked={this.state.viewer === "litemol"}
						onChange={this.handle_change}
					/>{" "}
					<label htmlFor="litemol_choice">LiteMol</label>
					<br/>
					<input
						type="radio"
						id="glmol_choice"
						name="viewer"
						value="glmol"
						checked={this.state.viewer === "glmol"}
						onChange={this.handle_change}
					/>{" "}
					<label htmlFor="glmol_choice">GLmol</label>
				</div>
				{this.renderViewer()}
				<div className="download-link">
					<a href={this.state.pdb_lipid_url} download>Download File: {this.state.pdbid}.pdb</a>
				</div>
				<div className="download-link">
					<a href={Urls.md_input_tar_file(this.state.pdbid)} download>Download File: {this.state.pdbid}.tgz</a>
				</div>
				{this.renderLipidComposition("Outer Leaflet Lipid Composition", "outer")}
				{this.renderLipidComposition("Inner Leaflet Lipid Composition", "inner")}
				<div className="info-table full">
					<table>
						<tbody>
							<tr className="dark">
								<th colSpan={2}>{this.state.membrane.name}</th>
							</tr>
							{this.renderTableRow("References", this.state.membrane.lipid_references, "light")}
							{this.renderTableRow("Pubmed", this.renderLipidPubmed(), "dark")}
						</tbody>
					</table>
				</div>
				<p className="lipid-footer-text">
					The files with explicit lipids are currently provided only as an illustration.
					All solvent molecules, ions and hydrogen atoms are excluded.
				</p>
			</div>
		);
	}
}

function mapStateToProps(state) {
  return {
		currentUrl: state.currentUrl,
	};
}

export default connect(mapStateToProps)(ProteinLipid);
