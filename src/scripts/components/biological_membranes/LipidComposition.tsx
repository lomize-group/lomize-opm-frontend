import * as React from "react";
import PubMedLink from "../pubmed_link/PubMedLink";
import "./LipidComposition.scss";

const LipidComposition: React.FC = () => {
    return (
        <div className="LipidComposition">
            <div className="references-table">
                <a id="refTable1"></a>
                <h1>
                    <b>Table 1.</b> Complex lipid compositions of 18 types of simulated realistic biomembranes [<PubMedLink id=" 35167752" />].
                </h1>
                <table>
                    <thead>
                        <tr>
                            <th><b>Membrane type</b> </th>
                            <th><b>Organisms</b> </th>
                            <th><b>Leaflet</b> </th>
                            <th><b>Head groups</b> </th>
                            <th><b>Acyl chains</b> </th>
                            <th><b>Sterol%</b> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowSpan={2}><b>Plasma</b> </td>
                            <td rowSpan={2} >mammals</td>
                            <td>inner</td>
                            <td>
                                PC:PE:PI:PS:PA:SM:CHOL
                                (18:26:5:11:1:10:29)
                            </td>
                            <td rowSpan={2}>16:0, 18:0, 18:1, 18:2, 20:4, 24:1</td>
                            <td>29.0</td>
                        </tr>
                        <tr>
                            <td>outer</td>
                            <td>
                                PC:PE:SM:CMH:CHOL
                                (38:6:22:4:37)
                            </td>
                            <td>34.6</td>
                        </tr>
                        <tr>
                            <td rowSpan={2}><b>Plasma</b></td>
                            <td rowSpan={2} >fungi</td>
                            <td>inner</td>
                            <td>
                                PC:PE:PI:PS:PA:ERG
                                (16:21:20:31:8:4)

                            </td>
                            <td rowSpan={2}>16:0, 16:1, 18:1</td>
                            <td>4.0</td>
                        </tr>
                        <tr>
                            <td>outer</td>
                            <td>
                                PC:PE:PI:PS:PA:ERG:MIPC
                                (11:7:5:7:3:49:54)

                            </td>
                            <td>36.0</td>
                        </tr>
                        <tr>
                            <td rowSpan={2}><b>Plasma</b></td>
                            <td rowSpan={2} >plants</td>
                            <td>inner</td>
                            <td>
                                PC:PE:PA:PI:PS:PG:DGDG:STEROL
                                (20:24:11:5:2:4:2:32)
                            </td>
                            <td rowSpan={2}>16:0, 18:1, 18:2, 18:3</td>
                            <td>32.0</td>
                        </tr>
                        <tr>
                            <td>outer</td>
                            <td>
                                PC:PE:PA:PS:CMH:STEROL
                                (13:16:5:1:15:64)
                            </td>
                            <td>56.1</td>
                        </tr>
                        <tr>
                            <td><b>Endoplasmic reticulum</b> </td>
                            <td>mammals</td>
                            <td>both</td>
                            <td>PC:PE:PI:PS:SM:PA:CHOL
                                (61:20:6:3:4:1:5)
                            </td>
                            <td>16:0, 18:0, 18:1, 18:2, 20:4</td>
                            <td>5.0</td>
                        </tr>
                        <tr>
                            <td><b>Endoplasmic reticulum</b></td>
                            <td>fungi</td>
                            <td>both</td>
                            <td>
                                PC:PE:PI:PS:PA:ERG
                                (47:14:23:8:4:4)
                            </td>
                            <td>16:0, 16:1, 18:1</td>
                            <td>4.0</td>
                        </tr>
                        <tr>
                            <td><b>Golgi apparatus</b> </td>
                            <td>mammals</td>
                            <td>both</td>
                            <td>PC:PE:PI:PS:SM:LPC16: CHOL
                                (45:17:9:4:12:5:8)
                            </td>
                            <td>16:0, 18:0, 18:1, 18:2, 20:4, 22:0</td>
                            <td>8.0</td>
                        </tr>
                        <tr>
                            <td><b>Golgi apparatus</b></td>
                            <td>fungi</td>
                            <td>both</td>
                            <td>
                                PC:PE:PI:PS:PA:ERG
                                (34:11:37:3:3:12)
                            </td>
                            <td>16:0, 16:1, 18:1</td>
                            <td>12.0</td>
                        </tr>
                        <tr>
                            <td><b>Endosome</b> </td>
                            <td>mammals</td>
                            <td>both</td>
                            <td>
                                PC:PE:PI:SM:CHOL
                                (33:13:8:16:32)
                            </td>
                            <td>16:0, 18:0, 18:2, 20:4, 22:6, 24:0</td>
                            <td>31.4</td>
                        </tr>
                        <tr>
                            <td><b>Lysosome</b> </td>
                            <td>mammals</td>
                            <td>both</td>
                            <td>
                                PC:PE:PI:PS:SM:CHOL:BMP
                                (35:25:8:3:6:18:7)
                            </td>
                            <td>16:0, 18:0, 18:1, 18:2, 20:4, 22:6, 24:0</td>
                            <td>17.6</td>
                        </tr>
                        <tr>
                            <td rowSpan={2}><b>Mitochondrial outer</b> </td>
                            <td rowSpan={2} >mammals</td>
                            <td>inner</td>
                            <td>
                                PC:PE:PI:PS:PA
                                (54:20:22:3:)
                            </td>
                            <td rowSpan={2}>16:0, 18:0, 18:1, 18:2, 20:4</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>outer</td>
                            <td>
                                PC:PE:PI:PS:PA:CL
                                (54:38:4:1:1:2)
                            </td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td rowSpan={2}><b>Mitochondrial inner</b> </td>
                            <td rowSpan={2} >mammals</td>
                            <td>inner</td>
                            <td>
                                PC:PE:PI:PS:CL
                                (29:36:6:3:26)
                            </td>
                            <td rowSpan={2}>16:0, 18:0, 18:1, 18:2, 20:4</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>outer</td>
                            <td>
                                PC:PE:PI:PS:CL
                                (58:37:5:3:11)
                            </td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td><b>Vacuole</b> </td>
                            <td>plants</td>
                            <td>both</td>
                            <td>
                                PC:PE:PI:PS:PG:CMH:DGDG:MGDG:SQDG: STEROL
                                (14:21:7:1:1:13:3:2:2:51)
                            </td>
                            <td>12:0, 16:0, 18:1, 18:2, 18:3</td>
                            <td>44.3</td>
                        </tr>
                        <tr>
                            <td><b>Thylakoid</b> </td>
                            <td>plants</td>
                            <td>both</td>
                            <td>
                                PG:DGDG:MGDG:SQDG
                                (15:30:40:15)
                            </td>
                            <td>16:0, 16:1, 18:3</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td><b>Thylakoid</b></td>
                            <td>cyanobacteria</td>
                            <td>both</td>
                            <td>
                                PG:DGDG:MGDG:SQDG
                                (10:25:45:25)
                            </td>
                            <td>16:0, 18:1</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td><b>Plasma</b> </td>
                            <td>archaebacteria</td>
                            <td>both</td>
                            <td>
                                PG:CL:AROL:MEN8
                                (64:7:8:21)
                            </td>
                            <td>C20, C25</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td rowSpan={2}><b>Outer cell</b> </td>
                            <td rowSpan={2} >Gram-neg. bacteria</td>
                            <td>inner</td>
                            <td>
                                PE:PG:CL
                                (75:20:5)
                            </td>
                            <td>16:0, 16:1, 18:1</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>outer</td>
                            <td>LPS(1)</td>
                            <td>C12,C14</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td><b>Inner cell</b> </td>
                            <td>Gram-neg. bacteria</td>
                            <td>both</td>
                            <td>
                                PE:PG:CL
                                (79:19:2)
                            </td>
                            <td>15:0, 16:0, 16:1, 18:1, cy17:0</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td><b>Plasma</b> </td>
                            <td>Gram-pos. bacteria</td>
                            <td>both</td>
                            <td>
                                PG:PE:CL
                                (65:27:8)
                            </td>
                            <td>C14:0, aC15:0, iC15:0, C16:0</td>
                            <td>0</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default LipidComposition;