import * as React from "react";
import "./BiologicalMembranes.scss";

import BiologicalMembraneDescription from "./BiologicalMembraneDescription";
import LipidComposition from "./LipidComposition";
import MembraneProperties from "./MembraneProperties";

type MembraneTypeSubsections =
    | "lipid_composition"
    | "membrane_properties"
    | "biological_descriptions";

type MembraneTypeProps = {
    subsection: MembraneTypeSubsections;
};

const BiologicalMembranes: React.FC<MembraneTypeProps> = ({ subsection }) => {
    let content;
    switch (subsection) {
        case "lipid_composition":
            content = <LipidComposition />;
            break;
        case "membrane_properties":
            content = <MembraneProperties />;
            break;
        case "biological_descriptions":
            content = <BiologicalMembraneDescription />;
            break;
        default:
            content = "error";
            break;
    }
    return <div className="membrane-types">{content}</div>;
}

export default BiologicalMembranes;
