import * as React from "react";
import { Assets } from "../../config/Assets";
import ImgWrapper from "../img_wrapper/ImgWrapper";
import PubMedLink from "../pubmed_link/PubMedLink";
import ExternalLink from "../external_link/ExternalLink";
import "./BiologicalMembranes.scss";

const D0 = <>D<sub>0</sub></>;
const Dpp = <>D<sub>p-p</sub></>;
const Scd = <>S<sub>CD</sub></>;
const Ka = <>K<sub>A</sub></>;
const Dh2o = <>D<sub>H2O</sub></>;

const BiologicalMembraneDescription: React.FC = () => {
    return (
        <div className="biological-description">
            <h1 className="page-header centered">Lipid Compositions of Biological Membranes</h1>

            <ul className="page-nav">
                <li><a href="#eukaryote">Eukaryotic Plasma Membrane</a></li>
                <li><a href="#er">Endoplasmic reticulum membrane </a></li>
                <li><a href="#golgi">Membranes of Golgi apparatus</a></li>
                <li><a href="#lysosome">Late endosome and lysosome membranes</a></li>
                <li><a href="#extraves">Membranes of extracellular vesicles</a></li>
                <li><a href="#lipid">Lipid droplets</a></li>
                <li><a href="#perox">Peroxisomal membrane</a></li>
                <li><a href="#nuclear">Nuclear membranes</a></li>
                <li><a href="#mitoouter">Mitochondrial outer membrane</a></li>
                <li><a href="#mitoinner">Mitochondrial inner membrane</a></li>
                <li><a href="#thyla">Thylakoid membrane</a></li>
                <li><a href="#gram-neg">Membranes of Gram-negative bacteria</a></li>
                <li><a href="#gram-pos">Cell membrane of Gram-positive bacteria</a></li>
                <li><a href="#archaea">Archaebacterial cell membrane</a></li>
            </ul>
            <ImgWrapper
                className="cell-anatomy-img"
                src={Assets.image_asset("CellAnatomy.png")}
            />
            <br />

            <h2 id="eukaryote" className="page-header centered">Eukaryotic plasma membranes</h2>
            <p>
                Eukaryotic cells are highly compartmentalized. Plasma membranes (PM) and membranes of intracellular
                organelles are characterized by morphological complexity, structural asymmetry, and high lipid diversity.
                The compositional diversity of lipids in different intracellular membranes is vital, therefore its
                maintenance consumes considerable amount of ATP and requires proteins encoded by up to ~5% genome [<a href="#ref1">1</a>].
            </p>
            <div className="centered">
                <ImgWrapper
                    className="cell-membrane-img"
                    src={Assets.image_asset("Cell_membrane_detailed_diagram.png")}
                />
            </div>
            <p>
                Membranes of higher eukaryotes are composed of more than 1,000 chemically different types of lipid
                molecules, which include glycerophospholipids with diverse head groups and acyl chains,
                tissue-specific sphingolipids (SLs) with more than 500 carbohydrate structures as head groups,
                and sterols. The main eukaryotic glycerophospholipids are: phosphatidylcholine (PC),
                phosphatidylethanolamine (PE), phosphatidylserine (PS), cardiolipin (CL), phosphatidyc acid (PA),
                and phosphatidylinositol (PI) [<a href="#ref1">1</a>][<a href="#ref2">2</a>]. The major SLs are sphingomyelin (SM)
                and glycosphingolipids  with mono-, di- or oligosaccharide chains
                (e.g. sialic acids in gangliosides) [<a href="#ref3">3</a>]. In low eukaryotes, such as yeast, the
                lipid diversity is lower: membranes contain different glycerophospholipids, but only four types of
                sphingolipids and distinct sterols [<a href="#ref4">4</a>]. Sterols and sphingolipids are the
                exclusive lipids of eukaryotes that facilitate lipid and protein segregation and formation of
                internal membranes with distinct lipid and protein composition [<a href="#ref5">5-7</a>]. In
                mammalian cells, PMs are covered by the fuzzy coat of glycocalyx composed by carbohydrate moieties
                of glycoproteins and membrane glycolipids [<a href="#ref8">8</a>], which replaces the rigid cell wall
                structure of bacteria, plants, and fungi.
            </p>
            <p>
                Eukaryotic membranes contain distinct phosphoinositides which selectively map cellular and intracellular
                membranes. For example, PtdIns(4)P is specific for the (trans)-Golgi network, PtdIns(3)P is present on
                early endosomes, PtdIns(3,5)P<sub>2</sub> on late endososmes, PtdIns(4,5)P<sub>2</sub> is the most
                abundant on the PM [<a href="#ref22">22</a>]. Phosophoinositides participate in signaling and protein
                sorting by recruiting membrane-targeting protein domains [<a href="#ref23">23</a>]. For example, PH domains
                bind various phosphoinositides, FYVE domains selectively bind PtdIns(3)P, ANTH/ENTH domains bind PtdIns(4)P,
                PtdIns(5)P, PtdIns(4,5)P<sub>2</sub>, or PtdIns(3,5)P<sub>2</sub>, and PX domains bind PtdIns(3)P,
                PtdIns(3,4)P<sub>2</sub>, or PtdIns(4,5)P<sub>2</sub>.
            </p>
            <b><a href="#top">Back to Membranes</a></b>
            <br /><br />

            <h2 id="er" className="page-header centered">Endoplasmic reticulum membrane</h2>
            <p>
                Endoplasmic reticulum (ER) is the main organelle where lipid and protein biosynthesis occurs. ER form
                network of interconnected sacs or cisternae held together by cytoskeleton. There are two types of ER:
                rough ER and smooth ER. Cisternal space of rough ER is continuous of the perinuclear space, while membranes
                of rough ER are covered by ribosomes bound to translocon complex that perform synthesis and translocation
                across membranes of transmembrane and secretory proteins. Smooth ER participates in lipid and carbohydrate
                metabolism and detoxification. In contrast to other cellular membranes, ER membranes have a nearly symmetric
                lipid distribution between both leaflets and a relatively loose packing of lipids that are mainly composed
                of PC (50-70%), PE (15-30%), PI (~10%), PS (~7%) with a minor level of other lipids and cholesterol
                [<a href="#ref1">1</a>, <a href="#ref7">7</a>, <a href="#ref9">9</a>]. These properties of the lipid bilayer
                facilitate insertion of newly synthesized lipid and protein molecules into ER membranes. Lipids and proteins
                are then distributed from ER to PM, endosomes, lysosome, and mitochondria.
            </p>
            <span className="centered">
                <ImgWrapper
                    className="endoplasmic-reticulum-img"
                    src={Assets.image_asset("EndoplasmicReticulum.png")}
                />
            </span>
            <b><a href="#top">Back to Membranes</a></b>
            <br /><br />

            <h2 id="golgi" className="page-header centered">Membranes of Golgi apparatus</h2>
            <span className="centered">
                <ImgWrapper
                    className="golgi-img"
                    src={Assets.image_asset("GolgiApparatus1.png")}
                />
                <ImgWrapper
                    className="golgi-img2"
                    src={Assets.image_asset("GolgiApparatus2.png")}
                />
            </span>
            <p>
                Golgi apparatus is made of stacks of vesicular cisternae with tubular connections that participate in vesicular transport in the secretory, lysosomal, and endocytic pathways. Glycosylation enzymes from Golgi complex attach sugar moieties to proteins moving from ER to the plasma membrane via Golgi apparatus in a direction from the cis-Golgi network (CGN) to the trans-Golgi network (TGN). From cisternae of TGN, proteins are packaging into vesicles destined to lysosomes, secretory vesicles, or plasma membrane. TGN is a main sorting station in the secretory pathway which confers 5 to 10-fold enrichment of sphingolipids and sterols in PM as compared to ER [1]. TNG have highly asymmetric lipid distribution between leaflets with PS and PE accumulated in the inner leaflet at the cytoplasmic side, and PC and SM in the outer leaflet, similar to PM of mammalian cells. TGC membranes of yeast are primarily composed of PC (20-30%), PI (25-30%), glycosylinositol-phosphoceramide (11%), and ergosterol (10-22%) [<a href="#ref24">24</a>].
            </p>
            <b><a href="#top">Back to Membranes</a></b>
            <br /><br />

            <h2 id="lysosome" className="page-header centered">Late endosome and lysosome membranes</h2>
            <p>
                The lipid composition of early endosomes resembles those of PM. Multivesicular bodies (MVBs), which are spherical vesicles with a diameter of ~400-500 nm, originate from early endososmes by the inward invagination of the limiting membrane into the endosomal lumen followed by bud scission [<a href="#ref25">25</a>]. MVBs fuse with either PM, where their content is released into extracellular space (as exosomes), or with late endosomes that transform into lysosomes, where their content undergoes degradation. MVBs targeted to degradation are presumed to undergo the monoubiquitination of proteins and release into the lysosome lumen. Late endosomes are more pleiotropic in shape with cysternal, tubular and multivesicular regions. Internal membranes of late endosomes are characterized by the high amount (up to 70% BMP) of non-bilayer phospholipid, bis-(monacylglycero)-phosphate (BMP) [<a href="#ref26">26</a>]. BMP promotes membrane fusion during acidification of maturing endosomes [<a href="#ref27">27</a>]. In addition, ceramide, which is present in raft of late endosome membranes, due to its cone-shaped structure spontaneously induces negative curvature of the lipid bilayer, thus promoting vesicle budding and fusion [<a href="#ref28">28</a>].
            </p>
            <div className="centered">
                <ImgWrapper
                    className="golgi-img"
                    src={Assets.image_asset("lysosome1.jpg")}
                />
            </div>
            <p>
                Lysosomes are spherical (diameter of 100-1200 nm) acidic (pH ~4.5 in lumen) vesicles that contain &gt;50 hydrolytic enzymes capable of breaking protein, lipids, carbohydrates, and nucleic acids. Lysosomes are not only “waste disposal system” but are also involved in secretion, repair, signaling, and energy metabolism. The limiting membrane of lysosomes might be protected against hydrolytic degradation by the glycocalix formed by glycosylated transmembrane proteins on the lumenal surface of the lysosomal membrane, while the major lipid of internal membranes, BMP, is a poorly degradable phospholipid because it has an unusual sn-1-glycerophospho-sn-1’-glycerol stereo configuration [<a href="#ref26">26</a>].
            </p>
            <b><a href="#top">Back to Membranes</a></b>
            <br /><br />

            <h2 id="extraves" className="page-header centered">Membranes of extracellular vesicles</h2>
            <p>
                Exosomes (30-100 nm in diameter) and secreted extracellular microvesicles (150-1000 nm in diameter) are involved in genetic communication between cells (contain RNAs), antigen presentation (contain MHC I, MHC II, CD86), neuronal communication, tissue physiology and regeneration [<a href="#ref29">29</a>]. Microvesicles are generated by blebbing of the PM, while exosomes are formed by fusion of multivesicular endosomes or MVBs with PM [<a href="#ref30">30</a>, <a href="#ref31">31</a>]. The lipid composition of the exosomal membrane has features reminiscent of PM raft domains with the increased ratio of certain lipids, such as SM, PS, PC, PI, ganglioside GM3, ceramide, and cholesterol [<a href="#ref32">32</a>]. However, content of BMP, a specific lipid of MVB, is not enriched.
            </p>
            <b><a href="#top">Back to Membranes</a></b>
            <br /><br />

            <h2 id="lipid" className="page-header centered">Lipid droplets</h2>
            <p>
                Lipid droplets (LDs) are subcellular structures (300-1000 nm in diameter) that function not only as storage depots, but also as metabolically dynamic organelles [<a href="#ref33">33</a>]. LDs harbor only a monolayer of phospholipids derived from ER that surrounds the hydrophobic core composed of triacylglycerols and steryl esters. LD surface has numerous proteins that are also present in ER membranes. Lipids of LDs are enriched in the anionic phospholipids and PI, as double-unsaturated lipid species, as compared to ER lipids. The protein content is rather low with most proteins involved in lipid metabolism.
            </p>
            <b><a href="#top">Back to Membranes</a></b>
            <br /><br />

            <h2 id="perox" className="page-header centered">Peroxisomal membrane</h2>
            <div className="centered">
                <ImgWrapper
                    className="golgi-img_PeroxisomalMembrane"
                    src={Assets.image_asset("PeroxisomalMembrane.png")}
                />
            </div>
            <p>
                Peroxisomes are involved in metabolism of hydrogen peroxide and various catabolic reactions (e.g. β-oxidation of fatty acids), biosynthetic reactions (e.g. biosynthesis of plasmalogens), and in the glyoxylate cycle. Peroxisome have a single membrane that enclose matrix rich in diverse enzymatic proteins [<a href="#ref33">33</a>]. Membrane of peroxisome from budding yeast is composed of PC (48.2%), PE(22.9%), PI (15.8%), CL (7%), and a few membrane proteins involved in protein transport [<a href="#ref34">34</a>]. Membranes of liver peroxisomes contain phospholipids: PC (36-45%), PE (47-50%), and PI(4-6%); free fatty acids, triglycerides and cholesterol [<a href="#ref35">35</a>]. Peroxisomes are considered to be autonomous organelles that can grow and divide. In yeast most peroxisomes are formed by fission of existing peroxisomes with help of dynamin-related proteins [<a href="#ref33">33</a>].
            </p>
            <b><a href="#top">Back to Membranes</a></b>
            <br /><br />

            <h2 id="nuclear" className="page-header centered">Nuclear membranes</h2>
            <div className="centered">
                <ImgWrapper
                    className="golgi-img_nuclear1"
                    src={Assets.image_asset("nuclear1.png")}
                />
            </div>
            <p>
                A nuclear membrane or nuclear envelope (NE) is the double-layered membrane which separates the genetic material and nucleolus from the cytoplasm in eukaryotic cells. Inner nuclear membrane (INM) and outer nuclear membrane (ONM) are continuous with the rough ER membrane and both are covered in ribosomes [<a href="#ref36">36</a>]. Membranes of NE are composed of phospholipids at proportions close to those in ER (e.g. ~45% PC, ~27%PE, ~15%PI, 6% PS, 2% PA in budding yeast) [<a href="#ref34">34</a>].
            </p>
            <b><a href="#top">Back to Membranes</a></b>
            <br /><br />

            <h2 id="mitoouter" className="page-header centered">Mitochondrial outer membrane</h2>
            <p>
                Lipid composition of the MOM differ from that in the outer membranes of Gram-negative bacteria: MOM contain phospholipids in both leaflets, lacks LPS, and are composed of PC (46-55%), PE (28-33 mol%), PI (9-13 mol%), and small amount of PS (ə-2 mol%), and CL (1-6%) [<a href="#ref37">37</a>, <a href="#ref38">38</a>].
            </p>
            <div className="centered">
                <ImgWrapper
                    className="golgi-img-mitochondria1"
                    src={Assets.image_asset("mitochondria1.png")}
                />
            </div>
            <b><a href="#top">Back to Membranes</a></b>
            <br /><br />

            <h2 id="mitoinner" className="page-header centered">Mitochondrial inner membrane</h2>
            <p>
                Mitochondrial inner membranes (MIM) form highly folded structure with cristae, the internal mitochondrial compartments. The lipid composition of MIM is similar to that of the bacterial inner membranes, with the large amount of non-bilayer forming lipids (PE and CL). MIM contains PC (~40 mol%), PE (25-40 mol%), PI (~16%), and CL (10-23%) as major phospholipids [<a href="#ref37">37-39</a>]. Unlike the variable lipid composition of membranes in diverse eukaryotic cells, the phospholipid levels of the MIM is rather constant in different tissues, and its alterations are not tolerated but cause diseases [<a href="#ref40">40</a>]. Negatively charged CL is an essential component that regulates various mitochondrial functions [<a href="#ref41">41</a>]. In particular, CL is critically required for biogenesis and stabilization of respiratory chain supercomplexes [<a href="#ref42">42</a>, <a href="#ref43">43</a>], as well as for mobilization of cytochrome C on IMM, thus preventing the appoptosis [<a href="#ref44">44</a>]. Depletion of CL due to absence of tafazzin, an enzyme essential for the formation of CL, causes the severe mitochondrial dysfunction with defective ATP formation, which is manifested as Barth syndrome [<a href="#ref45">45</a>].
            </p>
            <b><a href="#top">Back to Membranes</a></b>
            <br /><br />

            <h2 id="thyla" className="page-header centered">Thylakoid membrane</h2>
            <div className="centered">
                <ImgWrapper
                    className="golgi-img"
                    src={Assets.image_asset("thylakoid1.png")}
                />
            </div>
            <p>
                Thylakoid membranes form compartments inside chloroplasts and their postulated ancestors, cyanobacteria. This is the site of all light-dependent processes of phytosynthesis, i.e. the conversion of light energy into chemical energy. Thylakoid membranes are organized in grana with stacked architecture and sacs connecting neighboring grana. Such structural organization of folded membranes has functional significance for light adaptation, optimal holding of antenna assembly, and spatial organization and separation of two photosystems and ATP synthase [<a href="#ref46">46</a>].
            </p>
            <p>
                The characteristic features of photosynthetic membranes of thylakoids and cyanobacteria are: the unusual abundance of non-phosphorus galactoglycerolipids, the underrepresentation of phospholipids and the presence of anionic sulfolipids [<a href="#ref47">47</a>]. Thylakoid membrane lipids include a sole phospholipid, PG, and three glycolipids: monogalactosyl diacylglycerol (MGDG), digalactosyl diacylglycerol (DGDG), and sulfoquinovosyl diacylglycerol (SQDG). The composition of lipids co-crystallized with cyanobacteria Photosystem II (3bz1) (11 MGDG, 7 DGDG, 5 SQDG and 2 PG) and the preferential location of acidic lipids at the stroma leaflet reflects the relative contents (~45%, ~25%, ~15-25%, 5-15%, respectively) and asymmetric distribution of these lipids in thylakoid membranes [<a href="#ref48">48</a>].
            </p>
            <p>
                Thylakoid membranes are crowded by protein complexes with limited regions of free phospholipids (lipid:protein ration is ~1:4), so proteins account for up to ~80% of the dry mass [<a href="#ref49">49</a>]. High content of proteins that bind non-bilayer MGDG lipid stabilizes the structure of thylakoid membranes and prevents the formation of a non-lamellar phase [<a href="#ref49">49</a>].
            </p>
            <b><a href="#top">Back to Membranes</a></b>
            <br /><br />

            <h2 id="gram-neg" className="page-header centered">Membranes of Gram-negative bacteria</h2>
            <p>
                The cell envelope of Gram-negative bacteria is composed of two membranes with quite different composition and properties, the inner membrane (IM) and the outer membrane (OM). OM and IM are separated by a periplasmic space containing a thin layer (2-3 nm) of peptidoglycans, also known as mureins.
            </p>
            <div className="centered">
                <ImgWrapper
                    className="golgi-img2"
                    src={Assets.image_asset("grambacteria.png")}
                />
            </div>
            <h2 className="page-header centered">Outer Membrane</h2>
            <p>
                Unlike a typical phospholipid bilayer, the bacterial OM has extremely asymmetric structure where the inner leaflet is composed of glycerophospholipids, primarily PE and PG, and the outer leaflet contains a unique glycolipid, lipopolysaccaride (LPS) [<a href="#ref50">50</a>, <a href="#ref51">51</a>]. LPS is composed of lipid A, and long polysaccharide chain subdivided into the acidic inner core, outer core, and O-antigen region. The structure of the LPS leaflet is stabilized by ionic interactions between anionic groups of lipid A and divalent cations that bridge and neutralizes negative charges, as well as by H-bonding interactions between numerous donor and acceptor groups of lipids and core oligosaccharides [<a href="#ref50">50</a>]. Lipid A phosphate groups forms ionic pairs with basic amino acids of outer membrane proteins, which also stabilize the membrane structure. The LPS leaflet of OMs has a gel-like structure with a highly ordered, nearly crystalline arrangement of saturated acyl chains of the lipid A (usually 6 chains per lipid) [<a href="#ref52">52</a>]. It is also characterized by increased rigidity and reduced penetrability as the consequence of the stronger lateral intermolecular interactions between lipids and numerous H-bonds and ionic pairs between lipid A and proteins [<a href="#ref50">50</a>]. In thermophilic bacteria Thermus thermophilus, the OM consists of glycolipids and phosphoglycolipids, not LPS, which may increase membrane stability at high temperatures [<a href="#ref53">53</a>]. The extracellular surface of Gram-negative bacteria is covered by high-molecular-weight (capsular) polysaccharides and exopolysaccarides firmly attached to the cell. Some bacteria have additional protective S-layer on the cell surface formed by self-assembling glycoproteins that contact with LPS [<a href="#ref54">54</a>].
            </p>
            <h2 className="page-header centered">Inner Membrane</h2>
            <p>
                The IM of Gram-negative bacteria are less complex than PM of eukaryotic cells, having around a hundred lipid species. Lipids of many Gram-negative bacteria are composed mainly of PE and PG with some amount of PI and CL. The lipid composition of the IM from E. coli is one of the simplest found in nature. It consists of two major phospholipids, PE and PG, and traces of CL (PE/PG/CL ratio of 71.39:23.36:5.25 mol/mol%) [<a href="#ref55">55</a>] with only three major fatty acids (16:0, 18:1, 16:1) [<a href="#ref56">56</a>]. Anionic lipids are known to influence the membrane protein topology in E. coli [<a href="#ref57">57-60</a>]. To adapt to different environmental conditions and to maintain the optimum fluidity of membranes, bacteria produce a large variety of membrane lipids with different fatty acids. In addition to adjusting the level of desaturation of lipid acyl chains in response to temperature, bacteria can synthesize lipids that incorporate methyl, hydroxyl and cyclic groups [<a href="#ref56">56</a>, <a href="#ref61">61</a>]. Unlike eukaryotic membranes, bacterial IMs generally do not contain conventional sterols. However, as an adaptation to extreme environmental conditions, some bacteria and blue-green algae produce hopanoids, hydrophobic pentacyclic triterpenoids that are structurally related to cholesterol. Bacterial hopanoids supposedly regulate fluidity and permeability of the lipid bilayer, as well as cell sensitivity to pH, detergents and antibiotics [<a href="#ref62">62</a>].
            </p>
            <b><a href="#top">Back to Membranes</a></b>
            <br /><br />

            <h2 id="gram-pos" className="page-header centered">Cell membrane of Gram-positive bacteria</h2>
            <p>
                The cell envelope of Gram-positive bacteria is composed of a cytoplasmic membrane surrounded by a cell wall. The cell wall consists of: (a) peptidoglycans, linear polysaccharides cross-linked by short peptides; (b) secondary cell wall polymers, such as teichuronic acids, teichoic acids, other neutral or acidic polysaccharides (e.g. lipoglycans); (c) S-layer formed by proteins covalently or non-covalently attached to peptidoglycans [<a href="#ref54">54</a>, <a href="#ref63">63</a>]. Teichoic acids are charged anionic polyol phosphates, which confer a negative charge to the cell wall. Most monoderm bacteria have a thick peptidoglycan layer (20-80 nm) that retains the Gram (crystal violet) staining [<a href="#ref63">63</a>].
            </p>
            <p>
                Lipid composition of bacterial membranes is variable and depends on the environmental conditions [<a href="#ref61">61</a>]. Bacteria from genus Bacillus are rich in PE (40-60%) and also have anionic lipids, PG (5-40%) and cardiolipin, CL (8-17%). Bacteria from Staphylococcus genus and most other Gram-positive bacteria lack PE, but instead have large amount of anionic phospholipids, PG (30-90%) and CL (up to 19%), which attribute a high negative charge to their membranes [<a href="#ref64">64</a>]. In bacterium E. fecalis, the negative charge of the cytoplasmic membrane is partially neutralized by the presence of 20% lysyl-PG, a cationic lipid, and 26% of phosphatidylkojibiosyl diglycerol, in uncharged lipid covalently bound to lipotechoic acid [<a href="#ref65">65</a>]. In contrast to archaeal membranes that keep liquid crystalline phase and low permeability at a wide range of temperatures, the permeability of bacterial fatty acid ester membranes is low only just above their phase-transition temperature and significantly increases at rising temperatures. This requires the existence of control mechanisms for temperature adaptation of bacteria, for example by regulating the lipid fatty acid iso/anteiso composition [<a href="#ref61">61</a>] or unsaturation level [<a href="#ref66">66</a>]. Cell membrane isolated from several Gram-positive bacteria (e.g. Baccilus) contain large amounts of proteins (53‐75%) and less lipid (20‐30%) [<a href="#ref67">67</a>].
            </p>
            <div className="centered">
                <ImgWrapper
                    className="endoplasmic-reticulum-img"
                    src={Assets.image_asset("grampositive1.png")}
                />
            </div>
            <b><a href="#top">Back to Membranes</a></b>
            <br /><br />

            <h2 id="archaea" className="page-header centered">Archaebacterial cell membrane</h2>
            <p>
                Archaea are the most abundant microorganisms on the Earth that can thrive under extreme conditions [<a href="#ref68">68</a>, <a href="#ref69">69</a>]. In contrast to bacteria and eukaryotes, whose membranes have sn-glycerol-3-phosphate backbone, ester linkages and fatty acid chains, archaeabacteria membranes are characterized by sn-glycerol-1-phosphate backbone, ether linkages, and isoprenoid hydrocarbon chains containing 20, 25 or 40 carbon atoms [<a href="#ref70">70</a>]. All halophiles and some methanoges have lipids based on archaeol (2,3,-di-phytanyl-sn-glycerol) with glycerol moiety linked to different polar head groups. In many methanogens and all thermophiles, membrane lipids are based on caldarchaeol (di-biphytanyl-diglycerol-tetraethers) with two long C40 chains that traverse the membrane core and have two polar head groups located at the opposing ends [<a href="#ref71">71</a>]. Polar lipids account for 80-90% of all membrane lipids, while the remainder are neutral squalenes and other isoprenoids [<a href="#ref54">54</a>].
            </p>
            <p>
                Microorganisms that are able to thrive at extreme environmental conditions have developed a number of mechanisms of adaptation of their protein and membrane components. An essential feature of archaeal lipids is the low phase-transition temperature (between -20 and -15° C). This enable membranes to preserve the fluid liquid crystalline state over the wide temperature range (from 0° to 100° C), which is essential for optimal functioning of membrane proteins [<a href="#ref61">61</a>]. The presence of ether linkages, which are more heat resistant than ester linkages, enhance chemical stability of membrane lipids at high temperatures [<a href="#ref72">72</a>]. The C40 hydrocarbon chains of tetraether lipids span the entire membrane and stabilize it by linking the opposite leaflets. In thermophiles, the C40 isoprenoid moieties may have cyclopropane and cyclopentane rings, and the degree of cyclization is increasing at higher temperatures of the environment [<a href="#ref70">70</a>]. In the extremely thermophilic methanoarchaea (Methanocaldococcus jannaschii) the rise of the growth temperature leads to the increased ratio of caldarchaeol/cyclic archaeol/archaeol [<a href="#ref73">73</a>]. The adaptive traits of extreme haloalkaliphiles from the genus Natronococcus include synthesis of lipids with longer C25 isopropanoid chains and the enrichment in acidic lipids, such as ether cardiolipin [<a href="#ref74">74</a>].
            </p>
            <table>
                <tbody>
                    <tr>
                        <td className="archaebacteria-img">
                            <ImgWrapper
                                src={Assets.image_asset("archbact.png")}
                            />
                        </td>
                        <td>
                            <b>Membrane structures.</b>
                            <p>Top: an archaeal phospholipid: 1, isoprene chains; 2, ether linkages; 3, L-glycerol moiety; 4, phosphate group.</p>
                            <p>Middle: a bacterial or eukaryotic phospholipid: 5, fatty acid chains; 6, ester linkages; 7, D-glycerol moiety; 8, phosphate group.</p>
                            <p>Bottom: 9, lipid bilayer of bacteria and eukaryotes; 10, lipid monolayer of some archaea.</p>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p>
                All these adaptations results in more densely packed, more rigid, and stable membranes. At given temperature the membrane composed of tetraether lipids with saturated biphytanyl chains are more ordered and less flexible than phospholipid membranes [<a href="#ref75">75</a>]. The reduced segmental motion of methyl-branched phytanyl chains is regarded as a major cause of the low permeability of archaebacterial membranes to ions and solutes [<a href="#ref72">72</a>]. The cyclization of the chains in tetraether lipids may either decrease (cyclopropane and cyclopentane rings) or increase (cyclohexane rings) lipid motions and contributes to adequate membrane fluidity and low proton leakage rate at various temperatures [<a href="#ref71">71</a>, <a href="#ref72">72</a>]. The increased membrane thickness due to the presence of long-chain lipids also decreases dissipation of a proton gradient across the membrane [<a href="#ref74">74</a>].
            </p>
            <p>
                Archaea usually lack bacteria-type peptidoglicans that cover cell membranes, but some may have pseudo-peptidoglicans, chondroitin-like polymers, or an additional external layer of regularly arranged glycoproteins inserted into the outer leaflet of the membrane, so called crystalline surface layer (S-layer) [<a href="#ref54">54</a>, <a href="#ref76">76</a>]. Negatively charged residues of surface glycoproteins may counteract with sodium ions, facilitate protein hydration, and prevent protein precipitation at increased salinity [<a href="#ref69">69</a>]. The coating of acidophiles by OH-rich sugar moieties of tetraether lipids may have a strong proton-sheltering effect preventing the proton from penetrating the cell membrane [<a href="#ref77">77</a>].
            </p>
            <p>
                The adaptive features of proteins in hyperthermophilic archaea to heat stress involve the prevalence of smaller and mostly basic proteins with higher stability, as well as the enrichment of protein sequences by residues with larger volume and increased hydrophobicity, by aromatic residues (Trp, Tyr), and by charged residues that may form ionic bridges [<a href="#ref68">68</a>, <a href="#ref69">69</a>, <a href="#ref78">78</a>].
            </p>
            <b><a href="#top">Back to Membranes</a></b>
            <br /><br />

            <div className="references">
                <h3>References</h3>
                <ol>
                    <a id="ref1"></a>
                    <li>van Meer G, Voelker DR, Feigenson GW: <b>Membrane lipids: where they are and how they behave.</b> Nat. Rev. Mol. Cell Biol. 2008, <b>9</b>(2):112-124. [<PubMedLink id="18216768" />]</li>

                    <a id="ref2"></a>
                    <li>Harayama, T.; Riezman, H., <b>Understanding the Diversity of Membrane Lipid Composition.</b> Nat. Rev. Mol. Cell Biol. <b> 2018,</b> <i>19,</i> 281, DOI: 10.1038/nrm.2017.138 [<PubMedLink id="29410529" />]</li>

                    <a id="ref3"></a>
                    <li>Cummings RD: <b>The repertoire of glycan determinants in the human glycome.</b> Mol. Biosyst. 2009, <b>5</b>(10):1087-1104.[<PubMedLink id="19756298" />]</li>

                    <a id="ref4"></a>
                    <li>Bailey RB, Parks LW: <b>Yeast sterol esters and their relationship to the growth of yeast.</b> J. Bacteriol. 1975, <b>124</b>(2):606-612.[<PubMedLink id="1102523" />]</li>

                    <a id="ref5"></a>
                    <li>Desmond E, Gribaldo S: <b>Phylogenomics of sterol synthesis: insights into the origin, evolution, and diversity of a key eukaryotic feature.</b> Genome Biol. Evol. 2009, <b>1</b>:364-381.[<PubMedLink id="20333205" />]</li>

                    <a id="ref6"></a>
                    <li>Maxfield FR, van Meer G: <b>Cholesterol, the central lipid of mammalian cells.</b> Curr. Opin. in Cell Biol. 2010, <b>22</b>(4):422-429.[<PubMedLink id="20627678" />]</li>

                    <a id="ref7"></a>
                    <li>Sprong H, van der Sluijs P, van Meer G: <b>How proteins move lipids and lipids move proteins.</b> Nat. Rev. Mol. Cell Biol. 2001, <b>2</b>(7):504-513.[<PubMedLink id="11433364" />]</li>

                    <a id="ref8"></a>
                    <li>Reitsma S, Slaaf DW, Vink H, van Zandvoort MA, oude Egbrink MG: <b>The endothelial glycocalyx: composition, functions, and visualization.</b> Pflugers Arch. 2007, <b>454</b>(3):345-359.[<PubMedLink id="17256154" />]</li>

                    <a id="ref9"></a>
                    <li>Escriba PV, Gonzalez-Ros JM, Goni FM, Kinnunen PK, Vigh L, Sanchez-Magraner L, Fernandez AM, Busquets X, Horvath I, Barcelo-Coblijn G: <b>Membranes: a meeting point for lipids, proteins and therapies.</b> J. Cell. Mol. Med. 2008, <b>12</b>(3):829-875.[<PubMedLink id="18266954" />]</li>

                    <a id="ref10"></a>
                    <li>Daleke DL: <b>Regulation of transbilayer plasma membrane phospholipid asymmetry.</b> Journal of Lipid Research 2003, <b>44</b>(2):233-242.[<PubMedLink id="12576505" />]</li>

                    <a id="ref11"></a>
                    <li>Epand RM: <b>Lipid polymorphism and protein-lipid interactions.</b> Biochim. Biophys. Acta 1998, <b>1376</b>(3):353-368.[<PubMedLink id="9804988" />]</li>

                    <a id="ref12"></a>
                    <li>Denich TJ, Beaudette LA, Lee H, Trevors JT: <b>Effect of selected environmental and physico-chemical factors on bacterial cytoplasmic membranes.</b> Journal of Microbiological Methods 2003, <b>52</b>(2):149-182.[<PubMedLink id="12459238" />]</li>

                    <a id="ref13"></a>
                    <li>Marsh D: <b>Lateral pressure profile, spontaneous curvature frustration, and the incorporation and conformation of proteins in membranes.</b> Biophys. J. 2007, <b>93</b>(11):3884-3899.[<PubMedLink id="17704167" />]</li>

                    <a id="ref14"></a>
                    <li>McMahon HT, Gallop JL: <b>Membrane curvature and mechanisms of dynamic cell membrane remodelling.</b> Nature 2005, <b>438</b>(7068):590-596.[<PubMedLink id="16319878" />]</li>

                    <a id="ref15"></a>
                    <li>Tonnesen A, Christensen SM, Tkach V, Stamou D: <b>Geometrical membrane curvature as an allosteric regulator of membrane protein structure and function.</b> Biophys. J. 2014, <b>106</b>(1):201-209.[<PubMedLink id="24411252" />]</li>

                    <a id="ref16"></a>
                    <li>London E: <b>Insights into lipid raft structure and formation from experiments in model membranes.</b> Curr. Opin. Struct. Biol. 2002, <b>12</b>(4):480-486.[<PubMedLink id="12163071" />]</li>

                    <a id="ref17"></a>
                    <li>Simons K, Sampaio JL: <b>Membrane organization and lipid rafts.</b> Cold Spring Harb. Perspect. Biol. 2011, <b>3</b>(10):a004697.[<PubMedLink id="21628426" />]</li>

                    <a id="ref18"></a>
                    <li>Anderson RG, Jacobson K: <b>A role for lipid shells in targeting proteins to caveolae, rafts, and other lipid domains.</b> Science 2002, <b>296</b>(5574):1821-1825.[<PubMedLink id="12052946" />]</li>

                    <a id="ref19"></a>
                    <li>Machta BB, Papanikolaou S, Sethna JP, Veatch SL: <b>Minimal model of plasma membrane heterogeneity requires coupling cortical actin to criticality.</b> Biophys. J. 2011, <b>100</b>(7):1668-1677.[<PubMedLink id="21463580" />]</li>

                    <a id="ref20"></a>
                    <li>Cao XW, Surma MA, Simons K: <b>Polarized sorting and trafficking in epithelial cells.</b> Cell Res. 2012, <b>22</b>(5):793-805.[<PubMedLink id="22525333" />]</li>

                    <a id="ref21"></a>
                    <li>Surma MA, Klose C, Simons K: <b>Lipid-dependent protein sorting at the trans-Golgi network.</b> Biochim. Biophys. Acta 2012, <b>1821</b>(8):1059-1067.[<PubMedLink id="22230596" />]</li>

                    <a id="ref22"></a>
                    <li>Mayinger P: <b>Phosphoinositides and vesicular membrane traffic.</b> Biochim. Biophys. Acta 2012, <b>1821</b>(8):1104-1113.[<PubMedLink id="22281700" />]</li>

                    <a id="ref23"></a>
                    <li>Lemmon MA: <b>Phosphoinositide recognition domains.</b> Traffic 2003, <b>4</b>(4):201-213.[<PubMedLink id="12694559" />]</li>

                    <a id="ref24"></a>
                    <li>Klemm RW, Ejsing CS, Surma MA, Kaiser HJ, Gerl MJ, Sampaio JL, de Robillard Q, Ferguson C, Proszynski TJ, Shevchenko A et al: <b>Segregation of sphingolipids and sterols during formation of secretory vesicles at the trans-Golgi network.</b> J. Cell. Biol. 2009, <b>185</b>(4):601-612.[<PubMedLink id="19433450" />]</li>

                    <a id="ref25"></a>
                    <li>Gruenberg J, Stenmark H: <b>The biogenesis of multivesicular endosomes.</b> Nat. Rev. Mol. Cell Biol. 2004, <b>5</b>(4):317-323.[<PubMedLink id="15071556" />]</li>

                    <a id="ref26"></a>
                    <li>Hamer I, Van Beersel G, Arnould T, Jadot M: <b>Lipids and lysosomes.</b> Curr. Drug Metab. 2012, <b>13</b>(10):1371-1387.[<PubMedLink id="22978393" />]</li>

                    <a id="ref27"></a>
                    <li>Frederick TE, Chebukati JN, Mair CE, Goff PC, Fanucci GE: <b>Bis(monoacylglycero)phosphate forms stable small lamellar vesicle structures: insights into vesicular body formation in endosomes.</b> Biophys. J. 2009, <b>96</b>(5):1847-1855.[<PubMedLink id="19254543" />]</li>

                    <a id="ref28"></a>
                    <li>Trajkovic K, Hsu C, Chiantia S, Rajendran L, Wenzel D, Wieland F, Schwille P, Brugger B, Simons M: <b>Ceramide triggers budding of exosome vesicles into multivesicular endosomes.</b> Science 2008, <b>319</b>(5867):1244-1247.[<PubMedLink id="18309083" />]</li>

                    <a id="ref29"></a>
                    <li>Johnstone RM: <b>Exosomes biological significance: A concise review.</b> Blood Cells Mol. Dis. 2006, <b>36</b>(2):315-321.[<PubMedLink id="16487731" />]</li>

                    <a id="ref30"></a>
                    <li>Lo Cicero A, Stahl PD, Raposo G: <b>Extracellular vesicles shuffling intercellular messages: for good or for bad.</b> Curr. Opin. Cell. Biol. 2015, <b>35</b>:69-77.[<PubMedLink id="26001269" />]</li>

                    <a id="ref31"></a>
                    <li>Colombo M, Raposo G, Thery C: <b>Biogenesis, secretion, and intercellular interactions of exosomes and other extracellular vesicles.</b> Annu. Rev. Cell. Dev. Biol. 2014, <b>30</b>:255-289.[<PubMedLink id="25288114" />]</li>

                    <a id="ref32"></a>
                    <li>Frydrychowicz M, Kolecka-Bednarczyk A, Madejczyk M, Yasar S, Dworacki G: <b>Exosomes - structure, biogenesis and biological role in non-small-cell lung cancer.</b> Scand. J. Immunol. 2015, <b>81</b>(1):2-10.[<PubMedLink id="25359529" />]</li>

                    <a id="ref33"></a>
                    <li>Kohlwein SD, Veenhuis M, van der Klei IJ: <b>Lipid Droplets and Peroxisomes: Key Players in Cellular Lipid Homeostasis or A Matter of Fat—Store ’em Up or Burn ’em Down.</b> Genetics 2013, <b>193</b>(1):1-50.[<PubMedLink id="23275493" />]</li>

                    <a id="ref34"></a>
                    <li>Zinser E, Sperka-Gottlieb CD, Fasch EV, Kohlwein SD, Paltauf F, Daum G: <b>Phospholipid synthesis and lipid composition of subcellular membranes in the unicellular eukaryote Saccharomyces cerevisiae.</b> J. Bacteriol. 1991, <b>173</b>(6):2026-2034.[<PubMedLink id="2002005" />]</li>

                    <a id="ref35"></a>
                    <li>Kyrklund T, Meijer J: <b>Lipid composition of liver peroxisomes isolated from untreated and clofibrate-treated mice and rats.</b> Comp. Biochem. Physiol. B. Biochem. Mol. Biol. 1994, <b>109</b>(4):665-673.[<PubMedLink id="7881828" />]</li>

                    <a id="ref36"></a>
                    <li>Hetzer MW: <b>The nuclear envelope.</b> Cold. Spring. Harb. Perspect. Biol. 2010, <b>2</b>(3):a000539.[<PubMedLink id="20300205" />]</li>

                    <a id="ref37"></a>
                    <li>de Kroon AI, Dolis D, Mayer A, Lill R, de Kruijff B: <b>Phospholipid composition of highly purified mitochondrial outer membranes of rat liver and Neurospora crassa. Is cardiolipin present in the mitochondrial outer membrane?</b> Biochim. Biophys. Acta. 1997, <b>1325</b>(1):108-116.[<PubMedLink id="9106488" />]</li>

                    <a id="ref38"></a>
                    <li>Simbeni R, Pon L, Zinser E, Paltauf F, Daum G: <b>Mitochondrial membrane contact sites of yeast. Characterization of lipid components and possible involvement in intramitochondrial translocation of phospholipids.</b> J. Biol. Chem. 1991, <b>266</b>(16):10047-10049.[<PubMedLink id="2037561" />]</li>

                    <a id="ref39"></a>
                    <li>Cosentino K, Garcia-Saez AJ: <b>Mitochondrial alterations in apoptosis.</b> Chem. Phys. Lipids 2014, <b>181</b>:62-75.[<PubMedLink id="24732580" />]</li>

                    <a id="ref40"></a>
                    <li>Chicco AJ, Sparagna GC: <b>Role of cardiolipin alterations in mitochondrial dysfunction and disease.</b> Am. J. Physiol. 2007, <b>292</b>(1):C33-C44.[<PubMedLink id="16899548" />]</li>

                    <a id="ref41"></a>
                    <li>Schlame M, Rua D, Greenberg ML: <b>The biosynthesis and functional role of cardiolipin.</b> Prog. Lipid Res. 2000, <b>39</b>(3):257-288.[<PubMedLink id="10799718" />]</li>

                    <a id="ref42"></a>
                    <li>Zhang M, Mileykovskaya E, Dowhan W: <b>Gluing the respiratory chain together. Cardiolipin is required for supercomplex formation in the inner mitochondrial membrane.</b> J. Biol. Chem. 2002, <b>277</b>(46):43553-43556.[<PubMedLink id="12364341" />]</li>

                    <a id="ref43"></a>
                    <li>Schagger H: <b>Respiratory chain supercomplexes of mitochondria and bacteria.</b> Biochim. Biophys. Acta 2002, <b>1555</b>(1-3):154-159.[<PubMedLink id="12206908" />]</li>

                    <a id="ref44"></a>
                    <li>Iverson SL, Orrenius S: <b>The cardiolipin-cytochrome c interaction and the mitochondrial regulation of apoptosis.</b> Arch. Biochem. Biophys. 2004, <b>423</b>(1):37-46.[<PubMedLink id="14989263" />]</li>

                    <a id="ref45"></a>
                    <li>Schlame M, Towbin JA, Heerdt PM, Jehle R, DiMauro S, Blanck TJJ: <b>Deficiency of tetralinoleoyl-cardiolipin in Barth syndrome.</b> Ann. Neurol. 2002, <b>51</b>(5):634-637.[<PubMedLink id="12112112" />]</li>

                    <a id="ref46"></a>
                    <li>Sasaki DY, Stevens MJ: <b>Stacked, folded, and bent lipid membranes.</b> MRS. Bull. 2006, <b>31</b>(7):521-526.[<ExternalLink href="https://link.springer.com/article/10.1557/mrs2006.136">DOI</ExternalLink>]</li>

                    <a id="ref47"></a>
                    <li>Benning C: <b>Mechanisms of Lipid Transport Involved in Organelle Biogenesis in Plant Cells.</b> Ann. Rev. Cell Dev. Biol. 2009, <b>25</b>:71-91.[<PubMedLink id="19572810" />]</li>

                    <a id="ref48"></a>
                    <li>Kern J, Guskov A: <b>Lipids in photosystem II: Multifunctional cofactors.</b> J. Photochem. Photobiol. B 2011, <b>104</b>(1-2):19-34.[<PubMedLink id="21481601" />]</li>

                    <a id="ref49"></a>
                    <li>Garab G, Lohner K, Laggner P, Farkas T: <b>Self-regulation of the lipid content of membranes by non-bilayer lipids: a hypothesis.</b> Trends Plant Sci. 2000, <b>5</b>(11):489-494.[<PubMedLink id="11077258" />]</li>

                    <a id="ref50"></a>
                    <li>Nikaido H: <b>Molecular basis of bacterial outer membrane permeability revisited.</b> Microbiol. Mol. Biol. Rev. 2003, <b>67</b>(4):593-656.[<PubMedLink id="14665678" />]</li>

                    <a id="ref51"></a>
                    <li>Nikaido H: <b>Outer membranes, Gram-negative bacteria.</b> In: Encyclopedia of Microbiology. Ed. Schaechter M, vol. 2, 3d edn. Oxford, UK: Academic Press; 2009: 439-452.</li>

                    <a id="ref52"></a>
                    <li>Labischinski H, Barnickel G, Bradaczek H, Naumann D, Rietschel ET, Giesbrecht P: <b>High state of order of isolated bacterial lipopolysaccharide and its possible contribution to the permeation barrier property of the outer membrane.</b> J. Bacteriol. 1985, <b>162</b>(1):9-20.[<PubMedLink id="3980449" />]</li>

                    <a id="ref53"></a>
                    <li>Buchanan SK, Yamashita Y, Fleming KG: <b>Structure and folding of outer membrane proteins.</b> In: Comprehensive Biophysics Eds. Engelman EH, Tamm LK, vol. 5. Membranes. Oxford: Academic Press; 2012: 139-163.[<ExternalLink href="https://link.springer.com/article/10.1557/mrs2006.136">DOI</ExternalLink>]</li>

                    <a id="ref54"></a>
                    <li>Nanninga N: <b>Cell structure, organization, bacteria and archaea.</b> In: The desk encyclopedia of microbiology. Ed. Schaechter M, 2d edn. Oxford, UK: Academic Press; 2010: 357-374.[<ExternalLink href="https://doi.org/10.1016/B978-012373944-5.00256-X">DOI</ExternalLink>]</li>

                    <a id="ref55"></a>
                    <li>Ilg&#252; H, Jeckelmann J-M, Gachet Mar&#237;a S, Boggavarapu R, Ucurum Z, Gertsch J, Fotiadis D: <b>Variation of the Detergent-Binding Capacity and Phospholipid Content of Membrane Proteins When Purified in Different Detergents.</b> Biophys. J. 2014, <b>106</b>(8):1660-1670.[<PubMedLink id="24739165" />]</li>

                    <a id="ref56"></a>
                    <li>Mansilla MC, Cybulski LE, Albanesi D, de Mendoza D: <b>Control of membrane lipid fluidity by molecular thermosensors.</b> J. Bacteriol. 2004, <b>186</b>(20):6681-6688.[<PubMedLink id="15466018" />]</li>

                    <a id="ref57"></a>
                    <li>van Klompenburg W, Nilsson I, von Heijne G, de Kruijff B: <b>Anionic phospholipids are determinants of membrane protein topology.</b> The EMBO journal 1997, <b>16</b>(14):4261-4266.[<PubMedLink id="9250669" />]</li>

                    <a id="ref58"></a>
                    <li>Kiefer D, Hu X, Dalbey R, Kuhn A: <b>Negatively charged amino acid residues play an active role in orienting the Sec-independent Pf3 coat protein in the Escherichia coli inner membrane.</b> EMBO. J. 1997, <b>16</b>(9):2197-2204.[<PubMedLink id="9171335" />]</li>

                    <a id="ref59"></a>
                    <li>Delgado-Partin VM, Dalbey RE: <b>The proton motive force, acting on acidic residues, promotes translocation of amino-terminal domains of membrane proteins when the hydrophobicity of the translocation signal is low.</b> J. Biol. Chem. 1998, <b>273</b>(16):9927-9934.[<PubMedLink id="9545336" />]</li>

                    <a id="ref60"></a>
                    <li>Schuenemann TA, Delgado-Nixon VM, Dalbey RE: <b>Direct evidence that the proton motive force inhibits membrane translocation of positively charged residues within membrane proteins.</b> J. Biol. Chem. 1999, <b>274</b>(11):6855-6864.[<PubMedLink id="10066738" />]</li>

                    <a id="ref61"></a>
                    <li>Koga Y: <b>Thermal adaptation of the archaeal and bacterial lipid membranes.</b> Archaea 2012, 2012:769652.[<PubMedLink id="22927779" />]</li>

                    <a id="ref62"></a>
                    <li>Schmerk CL, Bernards MA, Valvano MA: <b>Hopanoid production is required for low-pH tolerance, antimicrobial resistance, and motility in Burkholderia cenocepacia.</b> J. Bacteriol. 2011, <b>193</b>(23):6712-6723.[<PubMedLink id="21965564" />]</li>

                    <a id="ref63"></a>
                    <li>Desvaux M, Dumas E, Chafsey I, Hebraud M: <b>Protein cell surface display in Gram-positive bacteria: from single protein to macromolecular protein structure.</b> FEMS Microbiol. Lett. 2006, <b>256</b>(1):1-15.[<PubMedLink id="16487313" />]</li>

                    <a id="ref64"></a>
                    <li>Epand RF, Pollard JE, Wright JO, Savage PB, Epand RM: <b>Depolarization, bacterial membrane composition, and the antimicrobial action of ceragenins.</b> Antimicrob. Agents Chemother. 2010, <b>54</b>(9):3708-3713.[<PubMedLink id="20585129" />]</li>

                    <a id="ref65"></a>
                    <li>Ganfield MC, Pieringer RA: <b>Phosphatidylkojibiosyl diglyceride. The covalently linked lipid constituent of the membrane lipoteichoic acid from Streptococcus faecalis (faecium) ATCC 9790.</b> J. Biol. Chem. 1975, <b>250</b>(2):702-709.[<PubMedLink id="803497" />]</li>

                    <a id="ref66"></a>
                    <li>Mansilla MC, de Mendoza D: <b>The Bacillus subtilis desaturase: a model to understand phospholipid modification and temperature sensing.</b> Arch. Microbiol. 2005, <b>183</b>(4):229-235.[<PubMedLink id="15711796" />]</li>

                    <a id="ref67"></a>
                    <li>Salton MR, Freer JH: <b>Composition of the membranes isolated from several Gram-positive bacteria.</b> Biochim. Biophys. Acta 1965, <b>107</b>(3):531-538.[<PubMedLink id="5867308" />]</li>

                    <a id="ref68"></a>
                    <li>Mesbah NM, Wiegel J: <b>Life at extreme limits: The anaerobic halophilic alkalithermophiles.</b> Ann. N. Y. Acad. Sci 2008, <b>1125</b>:44-57.[<PubMedLink id="18378586" />]</li>

                    <a id="ref69"></a>
                    <li>dasSarma S, Coker JA, dasSarma P: <b>Archaea - Overview.</b> In: Encyclopedia of Microbiology. Ed. Schaechter M, vol. 2, 3d edn. Oxford, UK: Academic Press; 2009: 1-23.[<ExternalLink href="https://doi.org/10.1016/B978-012373944-5.00108-5">DOI</ExternalLink>]</li>

                    <a id="ref70"></a>
                    <li>De Rosa M, Gambacorta A, Gliozzi A: <b>Structure, biosynthesis, and physicochemical properties of archaebacterial lipids.</b> Microbiol. Rev. 1986, <b>50</b>(1):70-80.[<PubMedLink id="3083222" />]</li>

                    <a id="ref71"></a>
                    <li>Damste JSS: <b>Crenarchaeol: the characteristic core glycerol dibiphytanyl glycerol tetraether membrane lipid of cosmopolitan pelagic crenarchaeota.</b> J. Lipid Res. 2002, <b>43</b>(10):1641-1651.[<PubMedLink id="12364548" />]</li>

                    <a id="ref72"></a>
                    <li>Albers SV, van de Vossenberg JL, Driessen AJ, Konings WN: <b>Adaptations of the archaeal cell membrane to heat stress.</b> Frontiers Biosci. 2000, <b>5</b>:D813-820.[<PubMedLink id="10966867" />]</li>

                    <a id="ref73"></a>
                    <li>Sprott GD, Meloche M, Richards JC: <b>Proportions of diether, macrocyclic diether, and tetraether lipids in Methanococcus jannaschii grown at different temperatures.</b> J. Bacteriol. 1991, <b>173</b>(12):3907-3910.[<PubMedLink id="2050642" />]</li>

                    <a id="ref74"></a>
                    <li>Angelini R, Corral P, Lopalco P, Ventosa A, Corcelli A: <b>Novel ether lipid cardiolipins in archaeal membranes of extreme haloalkaliphiles.</b> Biochim. Biophys. Acta 2012, <b>1818</b>(5):1365-1373.[<PubMedLink id="22366205" />]</li>

                    <a id="ref75"></a>
                    <li>Bartucci R, Gambacorta A, Gliozzi A, Marsh D, Sportelli L: <b>Bipolar tetraether lipids: Chain flexibility and membrane polarity gradients from spin-label electron spin resonance.</b> Biochemistry 2005, <b>44</b>(45):15017-15023.[<PubMedLink id="16274248" />]</li>

                    <a id="ref76"></a>
                    <li>Calo D, Kaminski L, Eichler J: <b>Protein glycosylation in Archaea: sweet and extreme.</b> Glycobiology 2010, <b>20</b>(9):1065-1076.[<PubMedLink id="20371512" />]</li>

                    <a id="ref77"></a>
                    <li>Wang X, Lv B, Cai G, Fu L, Wu Y, Wang X, Ren B, Ma H: <b>A proton shelter inspired by the sugar coating of acidophilic archaea.</b> Sci. Rep. 2012, <b>2</b>:892.[<PubMedLink id="23189241" />]</li>

                    <a id="ref78"></a>
                    <li>Haney PJ, Badger JH, Buldak GL, Reich CI, Woese CR, Olsen GJ: <b>Thermal adaptation analyzed by comparison of protein sequences from mesophilic and extremely thermophilic Methanococcus species.</b> Proc. Natl. Acad. Sci. U. S. A. 1999, <b>96</b>(7):3578-3583. [<PubMedLink id="10097079" />]</li>
                </ol>
                <b><a href="#top">Back to Membranes</a></b>
            </div>
        </div>
    );
};

export default BiologicalMembraneDescription;
