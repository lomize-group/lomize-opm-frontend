import * as React from "react";
import PubMedLink from "../pubmed_link/PubMedLink";
import "./MembraneProperties.scss";

const D0 = <>D<sub>0</sub></>;
const Dpp = <>D<sub>p-p</sub></>;
const Scd = <>S<sub>CD</sub></>;
const Ka = <>K<sub>A</sub></>;
const Dh2o = <>D<sub>H2O</sub></>;

const MembraneProperties: React.FC = () => {
    return (
        <div className="MembraneProperties">
            <div className="references-table">
                <a id="refTable2"></a>
                <h1>
                    <b>Table 2.</b>
                    Comparative molecular dynamics simulations of realistic eukaryotic, prokaryotic, and archaeal membranes [<PubMedLink id=" 35167752" />].
                    Calculated physico-chemical properties of membranes: distance between phosphate groups (<b>{Dpp}</b>), area per lipid (<b><i>APL</i></b>),
                    sterol tilt angle ( <b>&Theta;</b> ), deuterium order parameter (<b>{Scd}</b> ), area compressibility modulus (<b>{Ka}</b>), water penetration depth (<b>{Dh2o}</b>).
                </h1>
                <table>
                    <thead>
                        <tr>
                            <th><b>Membrane type</b></th>
                            <th><b>Organisms</b></th>
                            <th><b>{Dpp} (Å)</b></th>
                            <th><b><i>APL</i> in/out (Å)</b> </th>
                            <th><b>&Theta; (<sup>o</sup>)</b></th>
                            <th><b>{Scd}</b> </th>
                            <th><b>{Ka} (dyn/cm)</b></th>
                            <th><b>{Dh2o} in/out (Å)</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><b>Plasma</b></td>
                            <td>mammals</td>
                            <td>45.5</td>
                            <td>48.7/45.5</td>
                            <td>14.1</td>
                            <td>0.34</td>
                            <td>633</td>
                            <td>-14.2/14.7</td>
                        </tr>
                        <tr>
                            <td><b>Plasma</b></td>
                            <td>fungi</td>
                            <td>42.2</td>
                            <td>58.9/43.3</td>
                            <td>13.3</td>
                            <td>0.28</td>
                            <td>877</td>
                            <td>-8.3/17.6</td>
                        </tr>
                        <tr>
                            <td><b>Plasma</b></td>
                            <td>plants</td>
                            <td>45.3</td>
                            <td>47.4/41.6</td>
                            <td>11.3</td>
                            <td>0.39</td>
                            <td>1306</td>
                            <td>-12.7/16.5</td>
                        </tr>
                        <tr>
                            <td><b>Endoplasmic reticulum</b></td>
                            <td>mammals</td>
                            <td>40.2</td>
                            <td>62.2</td>
                            <td>22.3</td>
                            <td>0.24</td>
                            <td>264</td>
                            <td>-11.0/11.0</td>
                        </tr>
                        <tr>
                            <td><b>Endoplasmic reticulum</b></td>
                            <td>fungi</td>
                            <td>37.9</td>
                            <td>63.0</td>
                            <td>24.1</td>
                            <td>0.23</td>
                            <td>358</td>
                            <td>-10.0/10.0</td>
                        </tr>
                        <tr>
                            <td><b>Golgi apparatus</b></td>
                            <td>mammals</td>
                            <td>41.9</td>
                            <td>57.1</td>
                            <td>20.6</td>
                            <td>0.26</td>
                            <td>289</td>
                            <td>11.6/11.6</td>
                        </tr>
                        <tr>
                            <td><b>Golgi apparatus</b></td>
                            <td>fungi</td>
                            <td>39.6</td>
                            <td>54.51</td>
                            <td>21.7</td>
                            <td>0.26</td>
                            <td>384</td>
                            <td>-11.0/11.0</td>
                        </tr>
                        <tr>
                            <td><b>Endosome</b></td>
                            <td>mammals</td>
                            <td>44.9</td>
                            <td>49.6</td>
                            <td>16.1</td>
                            <td>0.32</td>
                            <td>452</td>
                            <td>-14.0/14.0</td>
                        </tr>
                        <tr>
                            <td><b>Lysosome</b></td>
                            <td>mammals</td>
                            <td>42.2</td>
                            <td>56.6</td>
                            <td>19.7</td>
                            <td>0.272</td>
                            <td>4368</td>
                            <td>-12.3/12.3</td>
                        </tr>
                        <tr>
                            <td><b>Vacuole</b></td>
                            <td>plants</td>
                            <td>46.3</td>
                            <td>46.8</td>
                            <td>11.4</td>
                            <td>0.40</td>
                            <td>1510</td>
                            <td>-15/14.8</td>
                        </tr>
                        <tr>
                            <td><b>Mitochondrial outer</b></td>
                            <td>mammals</td>
                            <td>39.5</td>
                            <td>66.2/66.2</td>
                            <td>N/A</td>
                            <td>0.22</td>
                            <td>265</td>
                            <td>-10.3/10.9</td>
                        </tr>
                        <tr>
                            <td><b>Mitochondrial inner</b></td>
                            <td>mammals</td>
                            <td>39.1</td>
                            <td>78.1/73.8</td>
                            <td>N/A</td>
                            <td>0.23</td>
                            <td>293</td>
                            <td>-10.9/10.5</td>
                        </tr>
                        <tr>
                            <td><b>Thylakoid</b></td>
                            <td>plants</td>
                            <td>39.0</td>
                            <td>65.4</td>
                            <td>N/A</td>
                            <td>0.22</td>
                            <td>313</td>
                            <td>-9.8/9.8</td>
                        </tr>
                        <tr>
                            <td><b>Thylakoid</b></td>
                            <td>cyanobacteria</td>
                            <td>41.1</td>
                            <td>63.0</td>
                            <td>N/A</td>
                            <td>0.22</td>
                            <td>211</td>
                            <td>-10.6/10.6</td>
                        </tr>
                        <tr>
                            <td><b>Plasma</b></td>
                            <td>archaebacteria</td>
                            <td>43.3</td>
                            <td>69.7</td>
                            <td>N/A</td>
                            <td>0.15</td>
                            <td>187</td>
                            <td>-9.5/10.2</td>
                        </tr>
                        <tr>
                            <td><b>Outer cell</b></td>
                            <td>Gram-neg.bacteria</td>
                            <td>36.6</td>
                            <td>63.8/182.3</td>
                            <td>N/A</td>
                            <td>0.23</td>
                            <td>1573</td>
                            <td>-9.2/10.0</td>
                        </tr>
                        <tr>
                            <td><b>Inner cell</b></td>
                            <td>Gram-neg.bacteria</td>
                            <td>38.3</td>
                            <td>64.0</td>
                            <td>N/A</td>
                            <td>0.23</td>
                            <td>417</td>
                            <td>-10.3/10.3</td>
                        </tr>
                        <tr>
                            <td><b>Plasma</b></td>
                            <td>Gram-pos.bacteria</td>
                            <td>37.0</td>
                            <td>61.5</td>
                            <td>N/A</td>
                            <td>0.22</td>
                            <td>289</td>
                            <td>-9.2/9.2</td>
                        </tr>
                        <tr>
                            <td><b>DOPC</b></td>
                            <td>artificial</td>
                            <td>38.8</td>
                            <td>67.5</td>
                            <td>N/A</td>
                            <td> 0.18</td>
                            <td>308</td>
                            <td>-10.0/10.0</td>
                        </tr>
                        <tr>
                            <td><b>DPPC</b></td>
                            <td>artificial</td>
                            <td>39.7</td>
                            <td>61.5</td>
                            <td>N/A</td>
                            <td> 0.22</td>
                            <td>220</td>
                            <td>-9.3/9.3</td>
                        </tr>
                    </tbody>
                    <p>N/A, not applicable.</p>
                </table>
            </div>
        </div>
    );
};

export default MembraneProperties;