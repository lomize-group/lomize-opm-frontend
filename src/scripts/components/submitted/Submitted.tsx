import * as React from "react";
import useStore from "../../hooks/useStore";
import ExternalLink from "../external_link/ExternalLink";
import WaitingPage from "../waiting_page/WaitingPage";
import { Link } from "react-router-dom";
import "./Submitted.scss";

type SubmittedProps = {
    submitAgain?: () => void;
    userEmail?: string;
    autoLoadResults?: boolean;
};

const Submitted: React.FC<SubmittedProps> = ({ submitAgain, userEmail, autoLoadResults = true }) => {
    const { query, url } = useStore((state) => state.currentUrl);
    let submitAgainUrl = url.replace("/submitted", "");

    const resultsUrl: string | undefined = query.resultsUrl;
    const waitingUrl: string | undefined = query.waitingUrl;
    const message: string | undefined = query.message;
    console.log(resultsUrl);
    console.log(waitingUrl);
    if (!resultsUrl || !waitingUrl) {
        return (
            <div className="submitted-text">
                Must provide resultsUrl and waitingUrl parameters in URL query.
            </div>
        );
    }

    return (
        <div className="submitted-text">
            { message && <p>{message}</p> }
            <p>
                Your results will be available below (
                <ExternalLink href={waitingUrl}>open in new tab</ExternalLink>)
            </p>
            {userEmail && (
                <p>You will be notified at {userEmail} when the calculations are complete</p>
            )}
            <p>
                <Link to={submitAgainUrl} className="expand-link" onClick={submitAgain}>
                    Submit Again?
                </Link>
            </p>
            <div className="results-window">
            <WaitingPage redirectUrl={resultsUrl}>
                {!autoLoadResults &&
                <p>
                    <hr/>
                    Your computation is complete.
                    <br/>
                    <a href={resultsUrl}>Open results</a>
                </p> }
            </WaitingPage>
            </div>
        </div>
    );
};
export default Submitted;
