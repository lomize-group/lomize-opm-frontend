import * as appendQuery from "append-query";
import axios from "axios";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { ComputationServer, Urls } from "../../config/Urls";
import { RootState } from "../../store";
import Submitted from "../submitted/Submitted";
import { Assets } from "../../config/Assets";
import "./OpmAlign.scss";


type AlignState = {
    pdbList: File | null,
    errors: string[],
    loading: boolean,
    resultsUrl: string | null,
    progress: number,
    total: number,
    //--------
    // added
    P_over_protein: string,
    P_ide_protein: string,
    P_over_family: string,
    P_ide_family: string,
    rmsd_same: string,
    mode: string,
    rmsd_max: string,
    userEmail: string,
}

const defaultAlignState: AlignState = {
    pdbList: null,
    errors: [],
    loading: false,
    resultsUrl: null,
    progress: 0,
    total: 0,
    //--------
    // added
    P_over_protein: "95",
    P_ide_protein: "98",
    P_over_family: "25",
    P_ide_family: "50",
    rmsd_same: "2.0",
    mode: "3",
    rmsd_max: "5.0",
    userEmail: "",
};

export default class OpmAlign extends React.Component<{}, AlignState> {
    constructor(props: {}) {
        super(props);
        this.state = defaultAlignState;
        this.handleFloatChange = this.handleFloatChange.bind(this);
        this.handlePdbListFileChange = this.handlePdbListFileChange.bind(this);
        this.submit = this.submit.bind(this);
        this.handleStringChange = this.handleStringChange.bind(this);
    }

    handleFloatChange(event: React.ChangeEvent<HTMLInputElement>) {
        const numRegex = /^[0-9 .]*$/;
        event.persist();
        this.setState((state: AlignState) => {
            if (event.target.value === "" || numRegex.test(event.target.value)) {
                state = {...state, [event.target.name]: event.target.value};
            }
            return state;
        });
    }
    handlePdbListFileChange(e: React.ChangeEvent<HTMLInputElement>){
        const files = e.target.files;
        this.setState({pdbList: files ? files[0] : null,});
    }
    handleStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ userEmail: e.target.value });
    }

    submit() {
        const { pdbList, errors, userEmail, mode, P_over_protein, P_ide_protein, 
            P_over_family, P_ide_family, rmsd_same, rmsd_max } = this.state;
        if (pdbList === null) {
            this.setState({errors: ["Must upload a pdb list"],});
            return;
        }
        if (errors.length !== 0) {
            this.setState({errors: []});
        }
        if (mode === null || P_over_family === null || P_ide_family === null ||
            P_over_protein === null || P_ide_protein === null || rmsd_same === null 
            || rmsd_max === null) {
            this.setState({errors: ["Must fill all required fields"],});
            return;
        }
        const form = new FormData();
        // Append all the data to the form
        form.append("new_pdb", pdbList);
        if (userEmail !== "") {
            form.append("user_email", userEmail);
        }
        form.append("mode", mode);
        form.append("P_over_protein", P_over_protein.toString());
        form.append("P_ide_protein", P_ide_protein.toString());
        form.append("P_over_family", P_over_family.toString());
        form.append("P_ide_family", P_ide_family.toString());
        form.append("rmsd_same", rmsd_same.toString());
        form.append("rmsd_max", rmsd_max.toString());

        this.setState({loading: true});
        axios.post(Urls.api_base + "/opm_align", form)
        .then(res => {
            if (res.status !== 202 && res.status !== 200) {
                throw new Error("Unexpected status code when submitting opm align: " + res.status);
            }
            this.setState({ resultsUrl: res.data.results });
            this.waitForResults();
        }).catch(err => {
            console.error("Error when submitting opm align: " + err);
            this.setState({ errors: ["Error when submitting opm align: " + err],
                            loading: false });
        });
    }

    waitForResults() {
        const timeout = 10*1000; // 10 seconds
        const { resultsUrl, progress, total } = this.state;
        if (resultsUrl === null) {
            console.error("results url was null when waiting for results");
            this.setState({ errors: ["Results url was null when waiting for results"] });
            setTimeout(this.waitForResults, timeout);
        }
        axios.get(Urls.api_base + resultsUrl!)
        .then(res => {
            if (res.status !== 202 && res.status !== 200) {
                throw new Error("Unexpected status code when submitting opm align: " + res.status);
            }
            if (res.status === 202) {
                this.setState({ errors: [] });
                if ( res.data.progress !== undefined && res.data.total !== undefined ) {
                    this.setState({ progress: res.data.progress, total: res.data.total });
                }
                setTimeout(() => this.waitForResults(), timeout);
            } else if (res.status === 200) {
                this.setState({
                    errors: [],
                    loading: false,
                });
                alert("Results are ready!");
            } else {
                console.error("Unexpected status code when waiting for results: " + res.status);
                this.setState({ errors: ["Unexpected status code when waiting for results: " + res.status],
                                loading: false });
                return;
            }
        }).catch(err => {
            console.error("Error when waiting for results: " + err);
            this.setState({ errors: ["Unexpected status code when waiting for results: " + err],
            loading: false })});
    }
    render() {
        const { errors, pdbList, loading, resultsUrl } = this.state;
        const { progress, total } = this.state;
        let errorsHtml = null;
        if (errors.length !== 0) {
            errorsHtml = (
                <div>
                    {errors.map(error => <div>{error}</div>)}
                </div>
            );
        }
        const USalignParams = (
            <div className='align-table'>
            <table className="table-form">
                <thead>
                    <tr>
                        <td colSpan={2}> USAlign Parameters </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> 
                            <h5>
                            P_over_protein
                            </h5>
                            Minimal percentage of overlapped residues
                            in the superposition (same protein)
                        </td>
                        <td>
                            <input type="text" className='narrow-input' name="P_over_protein" onChange={this.handleFloatChange} value={this.state.P_over_protein} />
                        </td>
                    </tr>
                    <tr className='dark'>
                        <td>
                            <h5>
                            P_ide_protein
                            </h5>
                            Minimal percentage of identity (same protein)
                        </td>
                        <td>
                            <input type="text" className='narrow-input' name="P_ide_protein" onChange={this.handleFloatChange} value={this.state.P_ide_protein} />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5>
                            P_over_family
                            </h5>
                            Minimal percentage of overlapped residues
                            in the superposition (same family)
                        </td>
                        <td>
                            <input type="text" className='narrow-input' name="P_over_family" onChange={this.handleFloatChange} value={this.state.P_over_family} />
                        </td>
                    </tr>
                    <tr className='dark'>
                        <td>
                            <h5>
                                P_ide_family
                            </h5>
                            Minimal percentage of identity (same family)
                        </td>
                        <td>
                            <input type="text" className="narrow-input" name="P_ide_family" onChange={this.handleFloatChange} value={this.state.P_ide_family} />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5>
                                rmsd_same
                            </h5>
                            Maximal RMSD (same protein)
                        </td>
                        <td>
                            <input type="text" className="narrow-input" name="rmsd_same" onChange={this.handleFloatChange} value={this.state.rmsd_same} />
                        </td>
                    </tr>
                    <tr className="dark">
                        <td>
                            <h5>
                                mode
                            </h5>
                            Mode of superposition 1: -ter 1 -byresi 1 <br />
                            2: -ter 1 -byresi 0 <br />
                            3:-ter 1 -mm 1 <br />
                            Else: set by user
                        </td>
                        <td>
                            <input type="text" className="narrow-input" name="mode" onChange={this.handleStringChange} value={this.state.mode} />
                        </td>
                    </tr>
                    <tr >
                        <td>
                            <h5>
                                rmsd_max
                            </h5>
                            Maximal RMSD (same family)
                        </td>
                        <td>
                            <input type="text" className="narrow-input" name="rmsd_max" onChange={this.handleFloatChange} value={this.state.rmsd_max} />
                        </td>
                    </tr>
                    <tr className='dark'>
                                <td>
                                    Email
                                    <br />
                                    You will be notified by email when the calculations are complete.
                                </td>
                                <td>
                                    <input
                                        type="text"
                                        name="userEmail"
                                        placeholder="email (optional)"
                                        value={this.state.userEmail || undefined}
                                        onChange={this.handleStringChange}
                                    />
                                </td>
                            </tr>
                </tbody>
            </table>
            </div>
        );

        const formHtml = (
            <div>
                {USalignParams}
                <div>
                    <label htmlFor='pdb-list'>PDB List</label>
                    <input 
                        type="file"
                        name="new_pdb"
                        id="new_pdb"
                        required
                        onChange={this.handlePdbListFileChange}
                    />
                </div>
                <div>
                    <input className='button-opmalign' type="submit"
                        name="submit"
                        value="submit"
                        onClick={this.submit}
                    />
                </div>
            </div>
        );
        const loadingHtml = (
            <>
                <img className="loading-image" src={Assets.images.loading} alt="loading" />
                <div>
                {progress !== 0 && total !== 0 && progress !== undefined && total !== undefined && (
                    <div className="progress-bar">
                        <div className="progress" style={{ width: `${(progress / total) * 100}%` }}></div>
                    </div>
                )}
                </div>
                <br />
                <br />
                <div>
                {progress !== 0 && total !== 0 && progress !== undefined && total !== undefined && (
                    <p>Progress: {progress} out of {total}</p>
                )}
                </div>
                <div>
                    { resultsUrl !== null && (
                         <a href={Urls.api_base + resultsUrl}>{Urls.api_base + resultsUrl}</a>
                    )}
                </div>
            </>
        );

        const contentHtml = loading ? loadingHtml : formHtml;
        let downloadHtml = null;
        if (!loading && resultsUrl !== null) {
            downloadHtml = (
                    <a href={Urls.api_base + resultsUrl}>Download results</a>
            );
        }
        return (<>
            <div className="frame-container">
                <div className="opm-align">OPM Align</div>
                {contentHtml}
                {downloadHtml}
                {errorsHtml}
            </div>
            </>
        );
    }



}