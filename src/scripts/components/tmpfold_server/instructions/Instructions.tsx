import * as React from 'react';
import { Assets } from "../../../config/Assets";
import ExternalLink from "../../external_link/ExternalLink";
import './Instructions.scss';

type InstructionsState = {
	hidden: boolean
};

class Instructions extends React.Component<{}, InstructionsState> {

	state = {
		hidden: true
	}

	render() {
		const instructions = (this.state.hidden ? null :
			<div className="popup">
				<div>
					<h1>Instructions</h1>
					<p>
						Input includes a standard PDB coordinate file and TM segments of α-helices.
						The format for TM segments is the same as in the OPM database, for example:
						</p>
					<strong>A;</strong>1(189-207),2(233-251)
							<br />
					<strong>B;</strong>1(23-45),2(62-80), 3(88-107),4(125-137),5(141-160),6(200-211),7(225-239)
							<br />
					<strong>C;</strong>1(49-69),2(91-109),3(136-156),4(174-193),5(253-258)
							<br />
					<br />
					<p>
						The residue numbers and the name of subunits must be consistent with numbering
						in the input PDB file. Two adjacent TM segments must be separated by at least
						one non-helical residue. The TM segments should include only standard amino
						acid residues with atoms identified under "ATOM" records.
						</p>
					<p>
						Some side chain atoms of a few residues may be missing in coordinate files provided
						by the RCSB PDB. We recommend reconstructing missing side chain atoms in TM segments
						prior to calculations with TMPfold. Otherwise, the calculated free energy of helix
						association will be underestimated.
						</p>
					<p>
						The results of calculations depend on the definition of TM segments and the quality
						of the provided structure. The method should be used for structures determined with
						resolution higher than 3.5 Å. Calculations account only for interactions between TM
						helices, but not interactions with or within loops, surface helices, or with cofactors
						present in experimental structures.
						</p>
					<p>
						The PDBx/mmCIF coordinate files can be converted to the standard PDB format
							using <ExternalLink href="https://pymol.org/2/">PyMol</ExternalLink> or{" "}
						<ExternalLink href="https://mmcif.pdbj.org/converter/index.php?l=en">online conversion service</ExternalLink>.
						</p>
					<p>
						<a download href={Assets.tmpfold_source}>Source code of TMPfold (web server version)</a>
					</p>
				</div>

				<div id="contact">
					<h1>Contact</h1>
					<p>
						Please send any questions or requests to Andrei Lomize (
							<a href="mailto:lomizeal@gmail.com">lomizeal@gmail.com</a>
							)
						</p>
				</div>

                <div>
                    <h1>Source code</h1>
                    <p>TMPfold&nbsp;
                        <a href="https://console.cloud.google.com/storage/browser/opm-assets/tmpfold_fmap_code">
                            source code
                        </a> for multiple input PDB files
                    </p>
                </div>
			</div>
		);

		return (
			<div className="tmpfold-instructions">
				<span
					className="expand-link"
					onClick={() => this.setState({ hidden: !this.state.hidden })}
				>
					{this.state.hidden ? 'show instructions' : 'hide instructions'}
				</span>
				<br />
				{instructions}
			</div>
		);
	}
}

export default Instructions;
