import * as appendQuery from "append-query";
import axios from "axios";
import "bootstrap";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import { Assets } from "../../config/Assets";
import { GenericObj } from "../../config/Types";
import { ComputationServer, Urls } from "../../config/Urls";
import { validate } from "../../config/Util";
import { RootState } from "../../store";
import ApiSearch from "../api_search/ApiSearch";
import Submitted from "../submitted/Submitted";
import Instructions from "./instructions/Instructions";
import "./TMPfoldServer.scss";

function mapStateToProps(state: RootState) {
	return {
		currentUrl: state.currentUrl
	};
}
const connector = connect(mapStateToProps)

type TMPfoldProps = ConnectedProps<typeof connector> & {
    computationServer: ComputationServer
	submitted?: boolean
};

type FileInputMode = "upload" | "search";
type SegmentInputMode = "manual" | "pdbId";

type TMPfoldForm = {
	fileInputMode: FileInputMode,
	fileSearchResults: GenericObj[],
	fileSearchPdbId: string,
	pdbFile?: File,

	segmentInputMode: SegmentInputMode,
	searchSegments: string,
	segments: string,

	userEmail: string
};

type TMPfoldState = TMPfoldForm & {
	loading: boolean,
	fileSearchLoading: boolean,
	segmentSearchLoading: boolean,
	instructions: boolean,
	errorText: string,
};

function getInitialState(): TMPfoldState {
	return {
		// page state
		loading: false,
		fileSearchLoading: false,
		segmentSearchLoading: false,
		instructions: false,
		errorText: "",
		// form
		pdbFile: undefined,
		fileInputMode: "upload",
		segmentInputMode: "manual",
		fileSearchResults: [],
		fileSearchPdbId: "",
		searchSegments: "",
		userEmail: "",
		segments: "",
	};
}

class TMPfoldServer extends React.Component<TMPfoldProps, TMPfoldState> {

	state = getInitialState();

	handlePdbFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const files = e.target.files;
		this.setState({ pdbFile: files ? files[0] : undefined });
	}

	handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => {
		let newState: Pick<TMPfoldForm, any> = {
			[e.target.name]: e.target.value
		};

		this.setState({
			errorText: "",
			...newState
		});
	}

	validateForm = (): string[] => {
		let errors = [];
		if (this.state.fileInputMode === "upload" && !validate.pdbFilename(this.state.pdbFile?.name)) {
			errors.push("invalid filename");
		}
		else if (this.state.fileInputMode === "search" && !validate.pdbFilename(this.state.fileSearchPdbId + ".pdb")) {
			errors.push("invalid filename");
		}

		const segments = this.state.segmentInputMode === "manual" ? this.state.segments : this.state.searchSegments;
		const segErrors = validate.tmSegments(segments);
		if (segErrors.length > 0) {
			errors.push("invalid TM segments: " + segErrors.join(", "));
		}

		if (!validate.email(this.state.userEmail)) {
			errors.push("invalid email");
		}
		return errors;
	}

	disableSubmit = () => {
		if (this.state.fileInputMode === "upload" && this.state.pdbFile === undefined) {
			return true;
		}
		if (this.state.fileInputMode === "search" && this.state.fileSearchPdbId === undefined) {
			return true;
		}
		if (this.state.errorText) {
			return true;
		}
		return false;
	}

	sendRequest = (params: FormData) => {
        const { computationServer } = this.props;
		axios.post(Urls.computation_servers[computationServer] + "tmpfold", params)
			.then(({ data }) => {
				this.setState({ loading: false });
				const urlParams = {
					resultsUrl: data.resultsUrl || "",
					waitingUrl: data.waitingUrl || "",
                    message: data.message,
				};
				const submitUrl = appendQuery(this.props.currentUrl.url + "/submitted", urlParams);
				this.props.currentUrl.history.push(submitUrl);
			});
	}

	submit = () => {
		const errors = this.validateForm();

		if (errors.length > 0) {
			this.setState({ errorText: errors.join("; ") });
			return;
		}

		const segs = this.state.segmentInputMode === "manual" ? this.state.segments : this.state.searchSegments;

		let params = new FormData();
		params.append("segments", segs)

		if (this.state.userEmail) {
			params.append("userEmail", this.state.userEmail);
		}

		if (this.state.fileInputMode === "upload") {
			params.append("pdbFile", this.state.pdbFile!);
			this.sendRequest(params);
		}
		else {
			const url = Urls.pdb_source_file(this.state.fileSearchPdbId);
			axios.get(url, { responseType: "arraybuffer" })
				.then(({ data }) => {
					const fileBlob = new Blob([data]);
					params.append("pdbFile", fileBlob, this.state.fileSearchPdbId + ".pdb")
					this.sendRequest(params);
				})
		}

		this.setState({ loading: true });
	}

	reset = () => {
		this.setState(getInitialState());
	}

	onSegmentSearchComplete = (data: GenericObj[] | undefined) => {
		if (!data || data.length === 0) {
			this.setState({
				segmentSearchLoading: false,
				errorText: "no subunits found",
				searchSegments: ""
			});
		}
		else {
			const segments = data.map(s => s.protein_letter + ":" + s.segment).join("\n");
			this.setState({
				searchSegments: segments,
				segmentSearchLoading: false
			});
		}
	}

	onSegmentSearch = (search: string) => {
		if (!validate.pdbCode(search)) {
			this.setState({
				searchSegments: "",
				errorText: "invalid pdb code " + search,
			});
			return false;
		}
		this.setState({
			segmentSearchLoading: true,
			errorText: ""
		});
		return true;
	}

	renderSegmentInput() {
		let content: JSX.Element;
		if (this.state.segmentInputMode === "manual") {
			content = (
				<textarea
					rows={10}
					placeholder="Input segments"
					id="segments"
					name="segments"
					onChange={this.handleChange}
				/>
			);
		}
		else {
			content = (
				<div>
					<div className="form-search">
						<ApiSearch
							apiEndpoint={(search: string) => "structure_subunits/pdbid/" + search}
							onSearch={this.onSegmentSearch}
							onSearchComplete={this.onSegmentSearchComplete}
							placeholder="PDB code"
							searchButtonText="get segments"
						/>
					</div>
					<div className="pdb-code-segments">
						{this.state.segmentSearchLoading ?
							<img src={Assets.images.loading_small} alt="Loading image" /> :
							this.state.searchSegments.split("\n").map((val, i) => <div key={i}>{val}</div>)
						}
					</div>
				</div>
			);
		}
		return (
			<td className="segments" colSpan={2}>
				<div className="align-left">
					<input
						type="radio"
						name="segmentInputMode"
						id="manualSegmentInputChoice"
						value="manual"
						checked={this.state.segmentInputMode === "manual"}
						onChange={this.handleChange}
					/>
					{" "}
					<label className="description" htmlFor="manualSegmentInputChoice">
						Input TM segments (please follow the format specified in the instructions)
					</label>
					{" "}
					<br />
					<input
						type="radio"
						name="segmentInputMode"
						id="pdbIdSegmentInputChoice"
						value="pdbId"
						checked={this.state.segmentInputMode === "pdbId"}
						onChange={this.handleChange}
					/>{" "}
					<label className="description" htmlFor="pdbIdSegmentInputChoice">
						Get TM segments from OPM database
					</label>{" "}
				</div>
				<br />
				{content}
			</td>
		);
	}

	renderFileSearchResults() {
		if (this.state.fileSearchLoading) {
			return <img src={Assets.images.loading_small} alt="Loading" />;
		}

		if (!this.state.fileSearchResults) {
			return null;
		}

		if (this.state.fileSearchResults.length === 0) {
			if (this.state.errorText) {
				return (
					<span className="error-msg">
						No results found.
					</span>
				);
			}
			return null;
		}

		const items = this.state.fileSearchResults.map((val, idx) =>
			<option value={val.pdbid} key={idx}>{val.pdbid}.pdb</option>
		);
		return (
			<select
				className="pdb-search-results"
				name="fileSearchPdbId"
				onChange={this.handleChange}
			>
				{items}
			</select>
		);
	}

	renderFileInput() {
		let content: JSX.Element;
		if (this.state.fileInputMode === "upload") {
			content = (
				<input
					name="pdbFile"
					className="file-upload"
					type="file"
					onChange={this.handlePdbFileChange}
				/>
			);
		}
		else {
			const onComplete = (data: any) => {
				this.setState({
					fileSearchLoading: false,
					fileSearchResults: data.objects,
					fileSearchPdbId: data.objects.length > 0 ? data.objects[0].pdbid : ""
				});
			}
			content = (
				<div className="form-search">
					<ApiSearch
						apiEndpoint="primary_structures"
						onSearch={() => this.setState({ fileSearchLoading: true })}
						onSearchComplete={onComplete}
					/>
					{this.renderFileSearchResults()}
				</div>
			);
		}

		return (
			<td className="file">
				<input
					type="radio"
					name="fileInputMode"
					id="fileUploadChoice"
					value="upload"
					checked={this.state.fileInputMode === "upload"}
					onChange={this.handleChange}
				/>
				{" "}
				<label className="description" htmlFor="fileUploadChoice">
					Upload your PDB file (please use extension .pdb)
				</label>
				{" "}
				<br />
				<input
					type="radio"
					name="fileInputMode"
					id="fileSearchChoice"
					value="search"
					checked={this.state.fileInputMode === "search"}
					onChange={this.handleChange}
				/>{" "}
				<label className="description" htmlFor="fileSearchChoice">
					Search for PDB file in OPM
				</label>
				<br />
				{content}
			</td>
		)
	}

	renderContent() {
        const { computationServer, submitted } = this.props;
		if (submitted) {
			return (
				<Submitted
					userEmail={this.state.userEmail}
					submitAgain={this.reset}
				/>
			);
		}

        const useOtherServer = computationServer == "memprot"
            ? <div>
                <strong>Having trouble submitting?</strong> Try out the{" "}
                <Link to="/tmpfold_server_cgopm">
                    cgopm-based version
                </Link>
            </div>
            : <div>
                <strong>Having trouble submitting?</strong> Try out the{" "}
                <Link to="/tmpfold_server">
                    AWS-based version
                </Link>
            </div>;

		const submitDisabled = this.disableSubmit();
		return (
			<>
				<div>
					The TMPfold server calculates free energies of association of
					transmembrane (TM) &alpha;-helical segments (<i>&Delta;G<sub>asc</sub></i>)
					in individual subunits of polytopic TM proteins and energies of subunit
					association in multi-protein TM complexes using their 3D structures as input.
					The method also produces the most probable
					{" "}<Link to="/about#tm_helix_assembly">assembly pathway</Link>{" "}
					of the TM helices into the 3D structure.
					<p>The source code of the computational server can be accessed through our {" "} 
						<a href="https://cggit.cc.lehigh.edu/biomembhub/tmpfold/-/tree/master/tmpfold?ref_type=heads"> GitLab</a>.
					</p>
				</div>

				<table className="table-form">
					<thead className="table-header">
						<tr>
							<td colSpan={2}>
								Find Free Energies of TM Helix Association
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							{this.renderFileInput()}
						</tr>
						<tr>
							{this.renderSegmentInput()}
						</tr>
						<tr>
							<td className="instructions-area" colSpan={2}>
								<div className="input-group email-input-div">
									<input
										name="userEmail"
										type="text"
										className="search-bar form-control input-sm email-input"
										placeholder="email (optional)"
										value={this.state.userEmail}
										onChange={this.handleChange}
									/>
								</div>
								<p className="helper">
									(email where you will be notified when the calculations are complete)
							    </p>
								<button
									title={submitDisabled ? "" : "submit"}
									className={submitDisabled ? "disabled-button" : "submit-button"}
									onClick={this.submit}
									disabled={submitDisabled}
								>
									Submit
								</button>
								<br />
								{this.state.errorText &&
									<span className="error-msg">
										{this.state.errorText}
									</span>
								}
								<br />
								<Instructions />
							</td>
						</tr>
					</tbody>
				</table>
				{useOtherServer}
			</>
		);
	}

	render() {
        const { computationServer } = this.props;
		if (this.state.loading) {
			return (
				<div className="info-loading">
					<img className="loading-image" src={Assets.images.loading} />
				</div>
			);
		}

        const serverType = computationServer == "memprot"
            ? "AWS"
            : computationServer;

		return (
			<div className="tmpfold-server">
				<div className="server-header">TMPfold Server ({serverType})</div>
				{this.renderContent()}
				<div className="server-footer">
				</div>
			</div>
		);
	}
}

export default connector(TMPfoldServer);
