import * as React from 'react';
import SwaggerUI from "swagger-ui-react";

import { Assets } from "../../config/Assets";
import { Urls } from "../../config/Urls";
import Classification from "../../config/Classification";

import "swagger-ui-react/swagger-ui.css"
import './Download.scss';

class Download extends React.Component
{
	static swagger_json_url = "/swagger-opm-api.json";

	constructor(props)
	{
		super(props);
		this.state = {};
	}

	renderTarFilesLink() {
		let items = [];

		for (let i = 0; i < Assets.pdb_tar_filenames.length; ++i) {
			const filename = Assets.pdb_tar_filenames[i];
			const href = Urls.pdb_tar_file(filename);
			items.push(
				<li key={i}>
					<a href={href} download>Download {filename}</a>
				</li>
			);
		}

		return(
			<div className="download-file">
				<h3>Tar Files</h3>
				<ul className="file-list">
					{items}
				</ul>
			</div>
		);
	}

	renderDownloadFileLinks(format) {
		let items = [];

		// query param
		const file_param = {
			fileFormat: format
		};

		// create list of download links
		for (let i = 0; i < Classification.ctypes.length; ++i) {
			const download_tablename = Classification.ctypes[i];
			const download_definition = Classification.definitions[download_tablename];

			// gen api url
			const url = Urls.api_url(download_definition.api.route, file_param);
			if(
				download_definition.api.route !== "/assembly_superfamilies" && 
				download_definition.api.route !== "/assembly_families" &&
				download_definition.api.route !== "/assembly_membranes"
			){
				items.push(
					<li key={i}>
						<a href={url} download>Download {download_definition.name}</a>
					</li>
				);
			}
		}

		return (
			<div className="download-file">
				<h3>CSV Files</h3>
				<ul className="file-list">
					{items}
				</ul>
			</div>
		);
	}

	render()
	{
		return(
			<div className="download">
				<div className="log-file">
					<a href={Assets.logfile} target="_blank">
						Download log file of recent OPM changes
					</a>
				</div>
				<hr/>
				{this.renderTarFilesLink()}
				<hr/>
				{this.renderDownloadFileLinks("csv")}
				<br/>
				<hr/>
				<SwaggerUI url={Download.swagger_json_url} />
			</div>
		);
	}
}

export default Download;
