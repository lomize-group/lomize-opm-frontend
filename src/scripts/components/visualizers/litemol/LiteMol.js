import * as React from "react";
import * as PropTypes from "prop-types";

import LiteMol from "litemol";

import "litemol/dist/css/LiteMol-plugin-light.min.css";
import "./LiteMol.scss";

class LiteMolViewer extends React.Component {
    static propTypes = {
        url: PropTypes.string.isRequired,
        width: PropTypes.string,
        minWidth: PropTypes.string,
        maxWidth: PropTypes.string,
        height: PropTypes.string,
        maxHeight: PropTypes.string,
    };

    static defaultProps = {
        width: "1400px",
        minWidth: "200px",
        maxWidth: "75%",
        height: "700px",
        maxHeight: `${screen.height - 200}px`,
    };

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
        };

        // ref to viewport div element
        this.viewport_target = undefined;

        this.init = () => {
            LiteMolViewer.createPlugin(this.viewport_target)
                .loadMolecule({
                    format: "pdb",
                    url: this.props.url,
                })
                .then(() => this.setState({ loading: false }));
        };
    }

    static createPlugin(viewport_target) {
        const { Plugin, Bootstrap } = LiteMol;
        const { Views } = Plugin;
        const { Transformer } = Bootstrap.Entity;
        const { LayoutRegion } = Bootstrap.Components;

        const customSpecification = {
            settings: {
                // 'density.defaultVisualBehaviourRadius': 5
            },
            transforms: [
                // {
                //     transformer: Transformer.Molecule.CreateVisual,
                //     view: Views.Transform.Molecule.CreateVisual
                // },
                // {
                //     transformer: Transformer.Density.CreateVisualBehaviour,
                //     view: Views.Transform.Density.CreateVisualBehaviour
                // },
                // { transformer: Transformer.Molecule.CreateVisual, view: Views.Transform.Molecule.CreateVisual },
            ],
            behaviours: [
                // you will find the source of all behaviours in the Bootstrap/Behaviour directory

                Bootstrap.Behaviour.SetEntityToCurrentWhenAdded,
                Bootstrap.Behaviour.FocusCameraOnSelect,

                // this colors the visual when it's selected by mouse or touch
                Bootstrap.Behaviour.ApplyInteractivitySelection,

                // this shows what atom/residue is the pointer currently over
                Bootstrap.Behaviour.Molecule.HighlightElementInfo,

                // distance to the last "clicked" element
                Bootstrap.Behaviour.Molecule.DistanceToLastClickedElement,

                // when the same element is clicked twice in a row, the selection is emptied
                Bootstrap.Behaviour.UnselectElementOnRepeatedClick,

                // when something is selected, this will create an "overlay visual" of the selected residue and show every other residue within 5ang
                // you will not want to use this for the ligand pages, where you create the same thing this does at startup
                // Bootstrap.Behaviour.Molecule.ShowInteractionOnSelect(5),
            ],
            components: [
                // highlight in the top right when hovering a part of the visualization
                Plugin.Components.Visualization.HighlightInfo(LayoutRegion.Main, true),

                Plugin.Components.Context.Log(LayoutRegion.Bottom, true),

                // show loading info
                Plugin.Components.Context.Overlay(LayoutRegion.Root),

                Plugin.Components.Context.Toast(LayoutRegion.Main, true),

                Plugin.Components.Context.BackgroundTasks(LayoutRegion.Main, true),
            ],
            viewport: {
                view: Views.Visualization.Viewport,
                controlsView: Views.Visualization.ViewportControls,
            },
            layoutView: Views.Layout,
            tree: void 0,
        };

        let plugin = Plugin.create({
            target: viewport_target,
            customSpecification,
            viewportBackground: "#D3D3D3",
            layoutState: {
                hideControls: true,
                isExpanded: false,
            },
        });
        plugin.context.logger.message(`LiteMol Plugin ${Plugin.VERSION.number}`);
        return plugin;
    }

    componentDidMount() {
        this.init();
    }

    render() {
        const viewport_dimensions = {
            height: this.props.height,
            maxHeight: this.props.maxHeight,
        };

        return (
            <div className="litemol-viewer">
                <div className="litemol-header">
                    <b>LiteMol Visualization</b>
                </div>
                <div
                    className="viewport"
                    style={viewport_dimensions}
                    ref={(ref) => (this.viewport_target = ref)}
                />
            </div>
        );
    }
}

export default LiteMolViewer;
