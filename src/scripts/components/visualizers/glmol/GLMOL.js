import * as React from "react";
import * as PropTypes from "prop-types";
import axios from "axios";
import scriptLoader from "react-async-script-loader";

import { Assets } from "../../../config/Assets";
import Classification from "../../../config/Classification";

import "./GLMOL.scss";

class GLmolViewer extends React.Component
{
	static propTypes = {
		url: PropTypes.string.isRequired,
		type: PropTypes.PropTypes.oneOf(["complex", "subunit", "protein_lipid"]).isRequired,
		color_by_default: PropTypes.oneOf(["chain", "chainbow", "ss", "b", "polarity"]),
		show_sidechains_default: PropTypes.bool,
		show_header: PropTypes.bool,
		width: PropTypes.string,
		minWidth: PropTypes.string,
		maxWidth: PropTypes.string,
		height: PropTypes.string,
		maxHeight: PropTypes.string,
		useFullWidth: PropTypes.bool,
	};
	
	static defaultProps = {
		color_by_default: "chain",
		show_sidechains_default: false,
		show_header: true,
		width: "1400px",
		minWidth: "200px",
		maxWidth: "75%",
		height: "700px",
		maxHeight: `${screen.height - 200}px`,
		useFullWidth: true
	};

	constructor(props)
	{
		super(props);

		this.state = {
			color_by: props.color_by_default,
			show_sidechains: props.show_sidechains_default,
			models: [],
			model_number: 0, // index of the model that will be rendered (if multi-model PDB)
			loaded: false,
			is_dimer: false,
			src: ""
		};

		this.glmol_obj = undefined;

		// handle change of user input options; update state and glmol re-render
		this.handle_color_change = event => {
			this.setState({color_by: event.target.value}, () => this.glmol_obj.loadMolecule());
		}
		this.handle_sidechains_change = event => {
			this.setState({show_sidechains: !this.state.show_sidechains}, () => this.glmol_obj.loadMolecule());
		}

		this.change_model = new_model_num => {
			// keep within bounds of available models
			if (new_model_num >= this.state.models.length) {
				new_model_num = this.state.models.length - 1; // keep at the final model
			}
			if (new_model_num < 0) {
				new_model_num = 0; // keep at the first model
			}

			// don't need to update if the model did not change
			if (new_model_num == this.state.model_number) {
				return;
			}

			// update current model and source
			this.setState({
				model_number: new_model_num,
				src: this.state.models[new_model_num].source.join("\n")
			}, () => {
				// re-render glmol scene for setState callback
				this.glmol_obj.rebuildScene();
				this.glmol_obj.loadMolecule();
			});
		}

		this.parse_pdb_data = data => {
			if ((this.props.type === "complex" || this.props.type === "protein_lipid") && !data.startsWith("MODEL")) {
				this.setState({src: data, loaded: true}, () => this.glmol_obj.loadMolecule());
				return;
			};

			const lines = data.split("\n");
			let cur_model_number = 0;

			// create object for first model
			let models = [ { source: [[" "]] } ];
			let is_dimer = false;
			let should_pop = false;

			
			let first_seq = false;

			for (let i = 0; i < lines.length; ++i) {
				const line = lines[i];
				models[cur_model_number].source.push(line);

				if (line.startsWith("MODEL")) {
					is_dimer = true;
					const model_number = line.trim().replace(/^\D+/g, "");
					models[cur_model_number].model_number = model_number;
					should_pop = false;
				}
				else if (line.startsWith("REMARK")) {
					const remarks = line.substring(7, line.length);
					let properties = remarks.split(",");
					const has_names = properties.length > 1;
					
					for (let j = 0; j < properties.length; ++j) {
						if (has_names) {
							const attribute_name = properties[j].split("=")[0].trim();
							const prop_value = properties[j].match("\\-?\\d*\\.\\d?")[0]; //detect floats in the remarks line
							models[cur_model_number][attribute_name] = prop_value;
						}
						else if (this.props.type === "complex") {
							let seq = "";
							if (first_seq) {
								if (properties[0].indexOf(".") === -1) {
									seq = "seq1";
									first_seq = false;
								}
								else {
									seq = "seq2";
								}
							}
							else {
								if (properties[0].indexOf(".") === -1) {
									seq = "seq3";
									first_seq = false;
								}
								else {
									seq = "seq4";
								}
							}

							if (seq !== "") {
								models[cur_model_number][seq] = properties[0].replace(/\s/g, "");
							}

						}
						else {
							const without_whitespace = properties[0].replace(/\s/g, "");
							if (properties[0].indexOf(".") === -1){
								models[cur_model_number].pathway = without_whitespace.replace("Pathway:", "");
							}
							else {
								models[cur_model_number].DGasc = without_whitespace.replace("DGasc=", "");
							}
						}
					}
				}
				else if (line.startsWith("ENDMDL")) {
					cur_model_number += 1;
					models.push({ source: [[" "]] });
					should_pop = true;
				}
			}

			// the final model will be empty from the loop if there was an ENDMDL for it
			if (should_pop) {
				models.pop();
			}

			const src = models[0].source.join("\n");

			// sequences shown below the glmol viewer window
			let seq1 = "";
			let seq2 = "";
			let seq3 = "";
			let seq4 = "";

			if (is_dimer) {
				if (this.props.type === "complex") {
					// magic number 1200 for some reason, sorry i didn't originally write this
					const head_arr = src.substring(0, 1200).split("\n");
					seq1 = head_arr[4].slice(8, -1);
					seq2 = head_arr[5].slice(8, -1);
					seq3 = head_arr[6].slice(8, -1);
					seq4 = head_arr[7].slice(8, -1);
				}
				else if (this.props.type === "subunit") {
					seq1 = models[0].seq1;
					seq2 = models[0].seq2;
				}
			}

			this.setState({
				loaded: true, src, models, is_dimer, seq1, seq2, seq3, seq4
			}, () => this.glmol_obj.loadMolecule());
			
		}

		this.init = () => {
			// needed for access within defineRepresentation (scope of "this" changes)
			const glmol = this;

			// define the glmol object and its defineRepresentation method.
			// defineRepresentation is used by glmol loadMolecule rendering.
			this.glmol_obj = new GLmol("glmol", true);

			this.glmol_obj.defineRepresentation = function() {
				const { color_by, show_sidechains } = glmol.state;

				let all = this.getAllAtoms();
				let allHet = this.getHetatms(all);
				let hetatm = this.removeSolvents(allHet);

				this.colorByAtom(all, {});
				this.colorByResidue(this.getSidechains(all), this.AminoAcidResidueColors);

				switch(color_by) {
					case "chainbow":
						this.colorChainbow(all);
						break;
					case "chain":
						this.colorByChain(all);
						break;
					case "b":
						this.colorByBFactor(all);
						break;
					case "ss":
						this.colorByStructure(all, 0xcc00cc, 0x00cccc);
						break;
					case "polarity":
						this.colorByResidue(all, this.AminoAcidResidueColors);
						break;
					default:
						break;
				}

				let asu = new THREE.Object3D();

				this.drawAtomsAsSphere(asu, hetatm, this.sphereRadius);

				if (show_sidechains) {
					this.drawBondsAsStick(asu, this.getSidechains(all), this.cylinderRadius, this.cylinderRadius);
				}

				this.drawCartoon(asu, all, this.curveWidth, this.thickness);
				this.drawSymmetryMates2(this.modelGroup, asu, this.protein.biomtMatrices);
				this.modelGroup.add(asu);
			};

			let url = this.props.url;
			axios.get(this.props.url).then(res => this.parse_pdb_data(res.data));
		}
	}

	componentWillReceiveProps ({ isScriptLoaded, isScriptLoadSucceed })
	{
		if (isScriptLoaded && !this.props.isScriptLoaded && isScriptLoadSucceed) { // load finished
			this.init();
		}
	}

	renderTableRows()
	{
		let rows = [];
		if (this.props.type === "complex") {
			for (let i = 0; i < this.state.models.length; ++i) {
				const model = this.state.models[i];
				rows.push(
					<tr
						className={"glmol-tr" + ((this.state.model_number === i) ? " selected" : "")}
						key={i}
						onClick={ () => {this.change_model(i)} }
					>
						<td>{model.model_number}</td>
						<td>{model.DGasc}</td>
						<td>{model.DGstb}</td>
						<td>{model.Easc}</td>
						<td>{model["Interhelix distance"]}</td>
						<td>{model.Angle}</td>
					</tr>
				);
			}
		}
		else if (this.props.type === "subunit") {
			for (let i = 0; i < this.state.models.length; ++i) {
				const model = this.state.models[i];
				rows.push(
					<tr
						className={"glmol-tr" + ((this.state.model_number === i) ? " selected" : "")}
						key={i}
						onClick={ () => {this.change_model(i)} }
					>
						<td>{model.model_number}</td>
						<td>{model.DGasc.replace("kcal/mol", "")}</td>
						<td>{model.pathway}</td>
					</tr>
				);
			}
		}
		return rows;
	}

	renderRemarksTable()
	{
		if (!this.state.loaded) {
			return (
				<div className="info-loading">
					<img className="loading-image" src={Assets.images.loading}/>
				</div>
			);
		}
		if (!this.state.is_dimer) {
			return null;
		}
		
		const filename = this.props.url.split("/").slice(-1)[0].replace("path.pdb", ".pdb");

		return(
			<div id="remarks-table-container">
				<br/>
				<table className="remarks-table">
					<thead>
						<tr>
							<th>
								<b>Step</b>
							</th>
							<th>
								<b>&Delta;<i>G<sub>assoc</sub></i></b>
							</th>
							<th>
								<b>Assembly</b>
							</th>
						</tr>
						<tr>
							<td></td>
							<td>kcal/mol</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						{this.renderTableRows()}
					</tbody>
				</table>
				<br/>
				<a href={this.props.url} download>Download File: {filename}</a>
			</div>
		);
	}

	renderViewController()
	{
		let select = null;
		const { type } = this.props;
		if (type === "complex" || type === "protein_lipid") {
			select = (
				<select
					id="glmol_color"
					value={this.state.color_by}
					onChange={this.handle_color_change}
				>
					<option value="chain">chain</option>
					<option value="chainbow">spectrum</option>
					<option value="ss">secondary structure</option>
					<option value="b">B factor</option>
					<option value="polarity">polarity</option>
				</select>
			);
		}
		else if (type === "subunit") {
			select = (
					<select
						id="glmol_color"
						value={this.state.color_by}
						onChange={this.handle_color_change}
					>
						<option value="polarity">polarity</option>
						<option value="chainbow">spectrum</option>
					</select>
			);
		}

		return(
			<div className="view-controller">
				{select}
				{" "}Sidechains <input
					id="show-sidechains"
					type="checkbox"
					checked={this.state.show_sidechains}
					onChange={this.handle_sidechains_change}
				/>
			</div>
		)

	}

	componentDidMount()
	{
		const { isScriptLoaded, isScriptLoadSucceed } = this.props;
		if (isScriptLoaded && isScriptLoadSucceed) {
			this.init();
		}
	}

	render()
	{
		const footer_sizing = {
			width: this.props.width,
			minWidth: this.props.minWidth,
			maxWidth: this.props.maxWidth,
		};
		const glmol_sizing = Object.assign({
			height: this.props.height,
			maxHeight: this.props.maxHeight
		}, footer_sizing);

		const seq3_4_class = this.props.type == "complex" ? "sequence" : "hidden";

		return(
			<div className={this.props.useFullWidth ? "glmol" : "glmol glmol-assembly"}>
				<div className={this.props.useFullWidth ? "complex_molcontainer" : "molcontainer"}>
					{ this.props.show_header &&
						<div className="molheader">
							<b>GLMol Visualization</b>
						</div>
					}
					<div style={glmol_sizing} id="glmol"></div>
					<div style={footer_sizing} className="molfooter">
						<p id="model_number"></p>
						<p id="sequence_1" className="sequence">{this.state.seq1}</p>
						<p id="sequence_2" className="sequence">{this.state.seq2}</p>
						<p id="sequence_3" className={seq3_4_class}>{this.state.seq3}</p>
						<p id="sequence_4" className={seq3_4_class}>{this.state.seq4}</p>
					</div>
					{this.renderViewController()}
				</div>

				{/* this has to be here for glmol to read the data */}
				<textarea
					id="glmol_src"
					value={this.state.src}
					readOnly
				/>
				{this.renderRemarksTable()}
			</div>
		);
	}
}

export default scriptLoader(
	'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
	'https://biomembhub.org/shared/membranome-assets/server_assets/Three49custom.js',
	'https://biomembhub.org/shared/membranome-assets/server_assets/GLmol.js',
)(GLmolViewer);
