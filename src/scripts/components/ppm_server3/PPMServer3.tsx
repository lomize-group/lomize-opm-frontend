import * as zlib from "zlib";
import * as appendQuery from "append-query";
import axios from "axios";
import "bootstrap";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Assets } from "../../config/Assets";
import { GenericObj } from "../../config/Types"
import { ComputationServer, Urls } from "../../config/Urls";
import { validate } from "../../config/Util";
import { RootState } from "../../store";
import ApiSearch from "../api_search/ApiSearch";
import PdbSearch, { PdbSearchTypes } from "../pdb_search/PdbSearch";
import Instructions from "./Instructions";
import Submitted from "../submitted/Submitted";
import "./PPMServer3.scss";
import { Link } from "react-router-dom";

function mapStateToProps(state: RootState) {
    return {
        currentUrl: state.currentUrl
    };
}

const connector = connect(mapStateToProps)

type PPM3Props = ConnectedProps<typeof connector> & {
    computationServer: ComputationServer,
    submitted?: boolean
};

type FileInputMode = "upload" | "searchOPM" | "searchPDB";

type MembraneType = null | "" | "PMm" | "PMp" | "PMf" | "ERf" | "ERm" | "GOL" | "LYS" | "END"
    | "VAC" | "MOM" | "MIM" | "THp" | "THb" | "GnO" | "GnI" | "GpI" | "ARC" | "LPC" | "MPC" | "OPC"
    | "EPC" | "MIC";

type MembraneForm = {
    membraneType: MembraneType,
    allowCurvature: boolean,
    topologyIn: boolean,
    subunits: string,
};

const defaultMembraneForm: MembraneForm = {
    membraneType: "",
    allowCurvature: false,
    topologyIn: true,
    subunits: "",
};

type PPM3SubmitForm = {
    membranes: MembraneForm[],
    heteroatoms: boolean,
    pdbFilename: string,
    pdbFile: string,
    userEmail: string | null,
};

export type PPM3Form = {
    fileInputMode: FileInputMode,
    fileSearchResults: GenericObj[],
    fileSearchPdbId: string,
    pdbSearchType: PdbSearchTypes,
    biologicalAssemblyNumber: number | null,
    hasSearched: boolean,
    pdbFile?: File,
    pdbSearchError: string,
    numMembranes: number,
    membranes: MembraneForm[],
    heteroatoms: boolean,
    userEmail?: string
};

type PPM3State = PPM3Form & {
    loading: boolean,
    fileSearchLoading: boolean,
    instructions: boolean,
    error: boolean,
    errorText: string
};

function getInitialState(): PPM3State {
    return {
        // page state
        loading: false,
        fileSearchLoading: false,
        instructions: false,
        error: false,
        errorText: "",
        // form
        pdbFile: undefined,
        fileInputMode: "upload",
        fileSearchResults: [],
        fileSearchPdbId: "",
        pdbSearchType: "asymmetric_unit",
        biologicalAssemblyNumber: 1,
        pdbSearchError: "",
        hasSearched: false,
        numMembranes: 1,
        membranes: [{...defaultMembraneForm}],
        heteroatoms: false,
        userEmail: undefined
    };
}

class PPMServer3 extends React.Component<PPM3Props, PPM3State> {

    state = getInitialState();

    handlePdbFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const files = e.target.files;
        this.setState({ pdbFile: files ? files[0] : undefined });
    }

    handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => {
        let newState: Pick<PPM3Form, any>;

        if (e.target.name === "numMembranes") {
            let value = parseInt(e.target.value);
            if (value > 4) value = 4;
            if (value < 1) value = 1;
            newState = {numMembranes: value};
        } else {
            newState = {
                [e.target.name]: e.target.value
            };
        }

        this.setState((oldState: PPM3State) => {
            const {
                error, errorText,
            } = getInitialState();
            const state: PPM3State = {...oldState, error, errorText, ...newState};

            if (state.numMembranes > state.membranes.length) {
                state.membranes = state.membranes.concat({...defaultMembraneForm});
            } else if (state.numMembranes < state.membranes.length) {
                state.membranes = [...state.membranes];
                state.membranes.pop();

                if (state.membranes.length === 1) {
                    const { subunits } = defaultMembraneForm;
                    state.membranes[0] = {...state.membranes[0], subunits};
                }
            }

            return state;
        });
    }

    handleMembraneChange(num: number, name: string, value: any) {
        this.setState((oldState: PPM3State) => {
            const {membranes} = oldState;
            const newMembranes: MembraneForm[]  = [...membranes];
            newMembranes[num] = {
                ...newMembranes[num],
                [name]: value,
            };
            return {membranes: newMembranes};
        });
    }

    handlePdbSearchTypeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        const value: any = e.target.value;
        this.setState({
            pdbSearchType: value
        });
    }

    handleBiologicalAssemblyNumberChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        let value: number | null = null;
        if (e.target.value) value = parseInt(e.target.value);
        this.setState({
            biologicalAssemblyNumber: value
        });
    }

    filenameValid = (filename: string) => {
        if (filename.indexOf(".") < 0) {
            return false;
        }
        let parts = filename.split(".");
        if (parts[parts.length - 1].toLowerCase() !== "pdb") {
            return false;
        }
        return true
    }

    handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const files = e?.target?.files;
        this.setState({
            pdbFile: files ? files[0] : undefined,
            error: false,
            errorText: ""
        });
    }

    disableSubmit = () => {
        if (this.state.fileInputMode === "upload" && this.state.pdbFile === undefined) {
            return true;
        }
        if (this.state.fileInputMode === "searchOPM" && this.state.fileSearchPdbId === undefined) {
            return true;
        }
        if (this.state.fileInputMode === "searchPDB") {
            if (!this.state.fileSearchPdbId) {
                return true;
            }

            if (this.state.pdbSearchType === "biological_assembly" && this.state.biologicalAssemblyNumber === null) {
                return true;
            }
        }
        if (this.state.errorText) {
            return true;
        }
        return false;
    }

    sendRequest = (params: PPM3SubmitForm) => {
        const { computationServer } = this.props;
        axios.post(Urls.computation_servers[computationServer] + "ppm3", params)
        .then(res => {
            // push url for submitted page
            const urlParams = {
                resultsUrl: res.data.resultsUrl || "",
                waitingUrl: res.data.waitingUrl || "",
                message: res.data.message,
            };
            let s = this.props.currentUrl.url.charAt(this.props.currentUrl.url.length - 1) === '/' ? "submitted" : "/submitted";
            const submitUrl = appendQuery(this.props.currentUrl.url + s, urlParams);
            this.props.currentUrl.history.push(submitUrl);
            this.setState({ loading: false });
        })
        .catch(err => {
            let error: string;
            if (err.response) {
                // Request made and server responded
                error = "Request made and server responded with failure status code: " + err.response.status.toString();
            }
            else if (err.request) {
                // Request made but no response received,
                error = "Request was made to the server but no response was received, this may be because the server is down.";
            }
            else {
                // Something happened in setting up the request that triggered an error
                error = "Setting up the request triggered an error: " + err.message;
            }
            console.log("An error occurred: " + JSON.stringify(err))
            this.setState({
                error: true,
                errorText: error,
                loading: false
            });
        });
    }

    validForm = (): string[] => {
        let errors: string[] = [];
        if (this.state.fileInputMode === "upload" && !validate.pdbFilename(this.state.pdbFile?.name)) {
            errors.push("invalid filename");
        }
        else if (this.state.fileInputMode === "searchOPM" && !validate.pdbFilename(this.state.fileSearchPdbId + ".pdb")) {
            errors.push("invalid filename");
        }

        if (!validate.email(this.state.userEmail)) {
            errors.push("invalid email");
        }
        return errors;
    }

    submit = async () => {
        // check for errors
        const errors = this.validForm();

        // don't submit if errors
        if (errors.length > 0) {
            this.setState({
                error: true,
                errorText: errors.join("; ")
            });
            return;
        }

        let pdbFilename: string | null = null;
        let pdbFile: string | null = null;
        const toBlob = (data: any) => new Blob([new Uint8Array(data)]);

        if (this.state.fileInputMode === "upload") {
            pdbFilename = this.state.pdbFile!.name;
            pdbFile = await this.state.pdbFile!.text();
        } else if (this.state.fileInputMode === "searchOPM") {
            const url = Urls.pdb_source_file(this.state.fileSearchPdbId);
            pdbFilename = this.state.fileSearchPdbId + ".pdb";

            try {
                const response = await axios.get(url, { responseType: "text" });
                pdbFile = response.data;
            } catch (err) {
                this.setState({ loading: false });
                this.setState({
                    error: true,
                    errorText: `The coordinate file ${this.state.fileSearchPdbId} was not included to the OPM database.`
                });
            }
        } else if (this.state.fileInputMode === "searchPDB") {
            pdbFilename = this.state.fileSearchPdbId + ".pdb";

            try {
                if (this.state.pdbSearchType === "asymmetric_unit") {
                    const url = Urls.rcsb.asymmetric_unit(this.state.fileSearchPdbId);
                    const response = await axios.get(url, { responseType: "text" });
                    pdbFile = response.data;
                } else {
                    // pdbSearchType === "biological_assembly"
                    if (this.state.biologicalAssemblyNumber == null) {
                        // this should already be checked
                        return;
                    }
                    const url = Urls.rcsb.biological_assembly(this.state.fileSearchPdbId, this.state.biologicalAssemblyNumber);
                    const response = await axios.get(url, { responseType: "arraybuffer" });
                    const data = zlib.gunzipSync(new Buffer(response.data));
                    pdbFile = await toBlob(data).text();
                }
            } catch (error) {
                this.setState({
                    loading: false,
                    error: true,
                    errorText: `The coordinate file ${this.state.fileSearchPdbId} was not included to the PDB database.`
                });
                throw error;
            }
        }

        if (pdbFilename === null || pdbFile === null) {
            throw `assertion error: pdbFilename = ${pdbFilename} or pdbFile = ${pdbFile}`;
        }
        let formData: PPM3SubmitForm = {
            membranes: this.state.membranes.map((membrane) => {
                const newMembrane = {...membrane};
                if (!newMembrane.membraneType) newMembrane.membraneType = null;
                return newMembrane;
            }),
            heteroatoms: this.state.heteroatoms,
            pdbFilename, pdbFile,
            userEmail: null,
        };
        if (this.state.userEmail) {
            formData.userEmail = this.state.userEmail;
        }
        this.setState({ loading: true });
        console.log(formData);
        this.sendRequest(formData);
    }

    reset = () => {
        this.setState(getInitialState());
    }

    renderFileSearchResults() {
        if (this.state.fileInputMode === "searchOPM") {
            if (this.state.fileSearchLoading) {
                return <img src={Assets.images.loading_small} alt="Loading" />;
            }

            if (!this.state.fileSearchResults) {
                return null;
            }

            if (this.state.fileSearchResults.length === 0) {
                if (this.state.errorText) {
                    return (
                        <span className="error-msg">
                            No results found.
                        </span>
                    );
                }
                if (this.state.hasSearched) {
                    return (
                        <span className="error-msg">
                            The PDB entry was not found in the OPM database.
                        </span>
                    );
                }
                return null;
            }

            const items = this.state.fileSearchResults.map((val, idx) =>
                <option value={val.pdbid} key={idx}>{val.pdbid}.pdb</option>
            );
            return (
                <select
                    className="pdb-search-results"
                    name="fileSearchPdbId"
                    onChange={this.handleChange}
                >
                    {items}
                </select>
            );
        }

        if (this.state.fileInputMode === "searchPDB") {
            if (!this.state.hasSearched) {
                return null;
            }

            if (this.state.fileSearchLoading) {
                return <img src={Assets.images.loading_small} alt="Loading" />;
            }

            const pdbId = this.state.fileSearchPdbId

            if (pdbId === null) {
                return (
                    <span className="error-msg">
                        {this.state.pdbSearchError}
                    </span>
                );
            }

            return (
                <select
                    className="pdb-search-results"
                    name="fileSearchPdbId"
                    onChange={this.handleChange}
                >
                    <option value={pdbId}>
                        {pdbId}.pdb
                    </option>
                </select>
            );
        }

        return null;
    }

    renderMembraneForm(num: number) {
        const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => {
            this.handleMembraneChange(num, e.target.name, e.target.value);
        };
        const subunits = this.state.numMembranes === 1 ? undefined : (
            <>
                <div className="membrane-bold">Chains</div>
                <input
                    type="text"
                    name="subunits"
                    placeholder="(ex: A,B,C...)"
                    value={this.state.membranes[num].subunits}
                    onChange={handleChange}
                />
                <div className="membrane-wide">(indicate subunits only in case of multiple membranes)</div>
            </>
        );
        const disabled = this.state.membranes[num].membraneType === "MIC";
        return (
            <div className="membrane">
                <div className="membrane-header">
                    {"Membrane "}
                    {num + 1}
                </div>
                <div className="membrane-bold">Type of membrane</div>
                <select
                    name="membraneType"
                    value={this.state.membranes[num].membraneType || ""}
                    onChange={handleChange}
                >
                    <option value="PMm">Plasma membrane (mammalian)</option>
                    <option value="PMp">Plasma membrane (plants)</option>
                    <option value="PMf">Plasma membrane (Fungi)</option>
                    <option value="ERf">ER (fungi)</option>
                    <option value="ERm">ER (mammalian)</option>
                    <option value="GOL">Golgi membrane</option>
                    <option value="LYS">Lysosome membrane</option>
                    <option value="END">Endosome membrane</option>
                    <option value="VAC">Vacuole membrane</option>
                    <option value="MOM">Outer mithochondrial membrane</option>
                    <option value="MIM">Inner mitochondrial membrane</option>
                    <option value="THp">Thylakoid membrane (plants)</option>
                    <option value="THb">Thylakoid membrane (bacteria)</option>
                    <option value="GnO">Gram-negative bacteria outer membrane</option>
                    <option value="GnI">Gram-negative bacteria inner membrane</option>
                    <option value="GpI">Gram-positive bacteria inner membrane</option>
                    <option value="ARC">Archaebacteria cell membrane</option>
                    <option value="">Undefined membrane</option>
                    <option value="LPC">DLPC bilayer</option>
                    <option value="MPC">DMPC bilayer</option>
                    <option value="OPC">DOPC bilayer</option>
                    <option value="EPC">DEuPC bilayer</option>
                    <option value="MIC">DPC micelle</option>
                </select>
                <div className="membrane-bold">Allow curvature</div>
                <div>
                    <input
                        type="radio"
                        checked={!this.state.membranes[num].allowCurvature && !disabled}
                        id={`membrane-${num}-allowCurvature-false`}
                        onChange={() => this.handleMembraneChange(num, "allowCurvature", false)}
                        disabled={disabled}
                    />
                    <label htmlFor={`membrane-${num}-allowCurvature-false`}>no</label>
                    <input
                        type="radio"
                        checked={this.state.membranes[num].allowCurvature === true && !disabled}
                        id={`membrane-${num}-allowCurvature-true`}
                        onChange={() => this.handleMembraneChange(num, "allowCurvature", true)}
                        disabled={disabled}
                    />
                    <label htmlFor={`membrane-${num}-allowCurvature-true`}>yes</label>
                </div>
                <div className="membrane-bold">Topology (N-ter)</div>
                <div>
                    <input
                        type="radio"
                        checked={this.state.membranes[num].topologyIn && !disabled}
                        id={`membrane-${num}-topology-in`}
                        onChange={() => this.handleMembraneChange(num, "topologyIn", true)}
                        disabled={disabled}
                    />
                    <label htmlFor={`membrane-${num}-topology-in`}>in</label>
                    <input
                        type="radio"
                        checked={!this.state.membranes[num].topologyIn && !disabled}
                        id={`membrane-${num}-topology-out`}
                        onChange={() => this.handleMembraneChange(num, "topologyIn", false)}
                        disabled={disabled}
                    />
                    <label htmlFor={`membrane-${num}-topology-out`}>out</label>
                </div>
                <div className="membrane-wide">(indicate topology of the N-terminus of the first chain of the PDB file)</div>
                {subunits}
            </div>
        );
    }

    renderMembraneForms() {
        if (this.state.membranes.length === 1) {
            return (
                <tr className="radios">
                    <td colSpan={2}>
                        {this.renderMembraneForm(0)}
                    </td>
                </tr>
            );
        } else if (this.state.membranes.length === 2) {
            return (
                <tr className="radios">
                    <td>
                        {this.renderMembraneForm(0)}
                    </td>
                    <td>
                        {this.renderMembraneForm(1)}
                    </td>
                </tr>
            );
        } else if (this.state.membranes.length === 3) {
            return (
                <>
                    <tr className="radios">
                        <td>
                            {this.renderMembraneForm(0)}
                        </td>
                        <td>
                            {this.renderMembraneForm(1)}
                        </td>
                    </tr>
                    <tr className="radios">
                        <td colSpan={2}>
                            {this.renderMembraneForm(2)}
                        </td>
                    </tr>
                </>
            );
        } else if (this.state.membranes.length === 4) {
            return (
                <>
                    <tr className="radios">
                        <td>
                            {this.renderMembraneForm(0)}
                        </td>
                        <td>
                            {this.renderMembraneForm(1)}
                        </td>
                    </tr>
                    <tr className="radios">
                        <td>
                            {this.renderMembraneForm(2)}
                        </td>
                        <td>
                            {this.renderMembraneForm(3)}
                        </td>
                    </tr>
                </>
            );
        } else {
            throw `assertion error: membranes.length = ${this.state.membranes.length}`;
        }
    }

    renderFileInput() {
        let content: JSX.Element = <div />;
        if (this.state.fileInputMode === "upload") {
            content = (
                <input
                    name="pdbFile"
                    className="file-upload"
                    type="file"
                    onChange={this.handlePdbFileChange}
                />
            );
        }
        else if (this.state.fileInputMode === "searchOPM") {
            const onComplete = (data: any) => {
                this.setState({
                    fileSearchLoading: false,
                    fileSearchResults: data.objects,
                    fileSearchPdbId: data.objects.length > 0 ? data.objects[0].pdbid : ""
                });
            }
            content = (
                <div className="form-search">
                    <ApiSearch
                        apiEndpoint="primary_structures"
                        onSearch={() => this.setState({ fileSearchLoading: true, hasSearched: true })}
                        onSearchComplete={onComplete}
                    />
                    {this.renderFileSearchResults()}
                </div>
            );
        }
        else if (this.state.fileInputMode === "searchPDB") {
            const onComplete = (pdb_id: any, errorMsg: string) => {
                this.setState({
                    fileSearchLoading: false,
                    fileSearchPdbId: pdb_id,
                    pdbSearchError: errorMsg,
                });
            }
            content = (
                <div className="form-search">
                    <PdbSearch
                        onSearch={() => this.setState({ fileSearchLoading: true, hasSearched: true })}
                        onSearchComplete={onComplete}
                        pdbSearchType={this.state.pdbSearchType}
                        biologicalAssemblyNumber={this.state.biologicalAssemblyNumber}
                    />
                    <select
                        name="pdbSearchType"
                        onChange={this.handlePdbSearchTypeChange}
                        value={this.state.pdbSearchType}
                    >
                        <option value="asymmetric_unit">
                            Asymmetric Unit
                        </option>
                        <option value="biological_assembly">
                            Biological Assembly
                        </option>
                    </select>
                    {this.renderBiologicalAssemblyNumberInput()}
                    <br />
                    {this.renderFileSearchResults()}
                </div>
            );
        }

        return (
            <td className="file" colSpan={2}>
                <input
                    type="radio"
                    name="fileInputMode"
                    id="fileUploadChoice"
                    value="upload"
                    checked={this.state.fileInputMode === "upload"}
                    onChange={this.handleChange}
                />
                {" "}
                <label className="description" htmlFor="fileUploadChoice">
                    Coordinate file
                </label>
                <br />
                (ex: xxxx.pdb; avoid additional periods, special symbols, and empty spaces in file
                name)
                <br />
                <input
                    type="radio"
                    name="fileInputMode"
                    id="opmSearchChoice"
                    value="searchOPM"
                    checked={this.state.fileInputMode === "searchOPM"}
                    onChange={this.handleChange}
                />{" "}
                <label className="description" htmlFor="opmSearchChoice">
                    Search for OPM entry
                </label>
                <br />
                <input
                    type="radio"
                    name="fileInputMode"
                    id="pdbSearchChoice"
                    value="searchPDB"
                    checked={this.state.fileInputMode === "searchPDB"}
                    onChange={this.handleChange}
                />{" "}
                <label className="description" htmlFor="pdbSearchChoice">
                    Search for PDB entry
                </label>
                <br />
                {content}
            </td>
        )
    }

    renderBiologicalAssemblyNumberInput() {
        if (this.state.fileInputMode != "searchPDB" || this.state.pdbSearchType != "biological_assembly") {
            return null;
        }

        let value: number | string = "";

        if (this.state.biologicalAssemblyNumber != null) {
            value = this.state.biologicalAssemblyNumber;
        }

        return (
            <input
                type="number"
                min="1"
                max="99"
                onChange={this.handleBiologicalAssemblyNumberChange}
                value={value}
            />
        );
    }

    renderContent() {
        const { computationServer, submitted } = this.props;
        if (submitted) {
            return (
                <Submitted
                    submitAgain={this.reset}
                    userEmail={this.state.userEmail}
                />
            );
        }

        const useOtherServer = computationServer == "memprot"
            ? <div>
                <strong>Having trouble submitting?</strong> Try out the{" "}
                <Link to="/ppm_server3_cgopm">
                    cgopm-based version
                </Link>
            </div>
            : <div>
                <strong>Having trouble submitting?</strong> Try out the{" "}
                <Link to="/ppm_server3">
                    AWS-based version
                </Link>
            </div>;

        const submitDisabled = this.disableSubmit();
        return (
            <>
                <div className="ppm3-introduction">
                    <h1>Positioning of proteins in flat and curved membranes</h1>
                    PPM 3.0 server features:
                    <ul>
                        <li>
                            positioning of transmembrane and peripheral proteins and peptides in
                            membranes and micelles
                        </li>
                        <li>
                            calculation of binding modes and membrane affinity of proteins taking
                            into account the influence of hydrophobic matching and curvature
                            stress
                        </li>
                        <li>
                            prediction of protein-induced membrane deformations
                        </li>
                        <li>
                            calculations in flat artificial and natural membranes
                        </li>
                        <li>
                            calculations in curved membranes with adjustable radii of curvature
                        </li>
                        <li>
                            calculations in spherical micelles of different sizes
                        </li>
                        <li>
                            positioning of proteins spanning two parallel membranes (e.g. outer
                            and inner membranes of Gram-negative bacteria)
                        </li>
                        <li>
                            approximation of deformed membrane surfaces by several (up to four)
                            flat surfaces
                        </li>
                        <li>
                            using polarity profiles along the membrane normal for the DOPC
                            (1,2-dioleoyl-sn-glycero-3-phosphocholine) bilayer [
                            <a href="https://pubmed.ncbi.nlm.nih.gov/21438606/" target="_blank">
                                PubMed
                            </a>
                            ]
                        </li>
                        <li>
                            using hydrophobic thicknesses (± 5Å) of artificial lipid bilayers
                            (experimental values) and of different biological membranes (average
                            values obtained from the large-scale analysis of transmembrane
                            proteins from the OPM database [
                            <a href="https://pubmed.ncbi.nlm.nih.gov/23811361/" target="_blank">
                                PubMed
                            </a>
                            ])*
                        </li>
                    </ul>
                    *Types of artificial and natural membranes and micelles used in
                    calculations and their physico-chemical and mechanical properties, including
                    hydrophobic thicknesses, are specified in Instructions

                    <p>The source code for the computational 
                        server can be found on <a href="https://cggit.cc.lehigh.edu/biomembhub/ppm3_server_code">GitLab</a>.
                    </p>
                </div>

                <table className="table-form">
                    <thead className="table-header">
                        <tr>
                            <td colSpan={2}>
                                Find Orientations of Proteins in Membranes (PPM 3.0)
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr className="radios">
                            <td className="num_membranes" colSpan={2}>
                                <b>Number of Membranes</b>
                                <br />
                                <input
                                    type="number"
                                    name="numMembranes"
                                    onChange={this.handleChange}
                                    value={this.state.numMembranes}
                                    min="1" max="4"
                                />
                            </td>
                        </tr>
                        {this.renderMembraneForms()}
                        <tr>
                            {this.renderFileInput()}
                        </tr>
                        <tr className="radios">
                            <td className="heteroatoms" colSpan={2}>
                                <b>Include heteroatoms, excluding water and detergents, for positioning in membrane:</b>
                                <br />
                                <input
                                    type="radio"
                                    checked={this.state.heteroatoms}
                                    onChange={() => this.setState({ heteroatoms: !this.state.heteroatoms })}
                                />
                                {" "}yes
                                <br />
                                <input
                                    type="radio"
                                    checked={!this.state.heteroatoms}
                                    onChange={() => this.setState({ heteroatoms: !this.state.heteroatoms })}
                                />
                                {" "}no
                                <br />
                                (include heteroatoms for non-standard amino acid residues; water and
                                detergents are automatically excluded)
                            </td>
                        </tr>
                        <tr>
                            <td className="instructions" colSpan={2}>
                                <div className="input-group email-input-div">
                                    <input
                                        type="text"
                                        className="search-bar form-control input-sm email-input"
                                        placeholder="email (optional)"
                                        value={this.state.userEmail}
                                        onChange={(e) => this.setState({ userEmail: e.target.value, error: false, errorText: "" })}
                                    />
                                </div>
                                <p className="helper">
                                    (you will be notified by email when the calculations are
                                    complete)
                                </p>
                                <button
                                    title="reset"
                                    className="submit-button"
                                    onClick={() => {
                                        this.setState(getInitialState());
                                    }}
                                >
                                    Reset
                                </button>
                                <button
                                    title={submitDisabled ? "" : "submit"}
                                    className={submitDisabled ? "disabled-button" : "submit-button"}
                                    onClick={this.submit}
                                    disabled={submitDisabled}
                                >
                                    Submit your query
                                </button>
                                <br />
                                {this.state.error &&
                                    <span className="error-msg">
                                        {this.state.errorText}
                                    </span>
                                }
                                <div>
                                    Please do not submit too many jobs at the same time. The program
                                    can be installed and run locally on any Linux machine using
                                    {" the "}
                                    <a
                                        href="https://console.cloud.google.com/storage/browser/opm-assets/ppm3_code"
                                        target="_blank"
                                    >
                                        source code
                                    </a>
                                    .
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div className="bottom-buttons">
                    <button
                        title="instructions"
                        className="submit-button"
                        onClick={() => this.setState({instructions: !this.state.instructions})}
                    >
                        Instructions
                    </button>
                    <br />
                    <Link to="/ppm_server">
                        <button title="home">
                            Home
                        </button>
                    </Link>
                </div>
                {useOtherServer}
                {this.state.instructions ? <Instructions /> : undefined}
            </>
        );
    }

    render() {
        const { computationServer } = this.props;
        if (this.state.loading) {
            return (
                <div className="info-loading">
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            );
        }

        const serverType = computationServer == "memprot"
            ? "AWS"
            : computationServer;

        return (
            <div className="ppm-server3">
                <div className="server-header">PPM 3.0 Web Server ({serverType})</div>
                {this.renderContent()}
            </div>
        );
    }
}

export default connector(PPMServer3);
