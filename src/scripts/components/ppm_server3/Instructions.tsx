import * as React from 'react';
import { Link } from 'react-router-dom';
import './Instructions.scss';

const D0 = <>D<sub>0</sub></>;
const fMism = <>f<sub>mism</sub></>;

const Instructions: React.FC = () => {
    return (
        <div className="ppm3-instructions-paragraphs">
            <h1>Input</h1>
            <ol>
                <li>
                    As input, user should provide a coordinate file (in pdb format) of a protein of
                    interest. PPM 3.0 server does not reconstruct missing side-chain atoms. Users
                    are advised to: (a) provide structure in a biologically relevant oligomeric
                    state; (b) reconstruct missing side-chain atoms that may interact with lipids;
                    and (c) remove His tags or other artificial chemical modifications.
                </li>
                <li>
                    Option "Number of Membranes 1" is used for calculation of protein positions in a
                    single flat or curved membrane.
                </li>
                <li>
                    Option "Number of Membranes 2" (3 or 4) is used for calculation of protein
                    positions in multiple flat bilayers or different parts of a largely distorted
                    bilayer that could be approximated by several flat surfaces. User should specify
                    protein chains that are associated with each membrane or a membrane part. For
                    example, for AcrAB-TolC efflux pump (5v5s) that spans the outer and the inner
                    membranes of <i>E. coli</i>, subunits A,B,C should be assigned to the outer
                    membranes
                    and subunits J,K,L,D,E,F,H,G,I should be assigned to the inner membrane. In case
                    of the dimeric proton ATPase (6b2z) that induces curvature of the inner
                    mitochondrial membrane and stabilizes cristae, a group of tightly packed
                    subunits B,C,D,E,F,G,H,I,J,K,L,M,N,O,Q,R,S,T should be assigned to one part of
                    the same membrane, while another group of subunits
                    A,1,2,3,4,5,6,7,8,9,0,a,b,d,f,g,i,k should be assigned to the second membrane
                    part.
                </li>
                <li>
                    For any chosen number of membranes, user should define the type of each membrane
                    from the list: PM (mammalian), PM (fungi), PM (plant, algae), ER (mammalian), ER
                    (fungi), Golgi, MOM (mitochondrial inner membrane), MOM (mitochondrial outer
                    membrane), Lysosome, Endosome, Vacuole (plant), Thylakoid (plant or bacteria),
                    Outer or Inner membranes of Gram-negative bacteria, PM of Gram-positive bacteria
                    or Archaea, DLPC (diC12:0 PC), DMPC (diC14:0 PC), DOPC (diC18:1∆9c PC), or DEuPC
                    (diC22:1∆13c PC). Positioning in a spherical DPC (C12PC) micelle option should
                    be used for peptides,
                    but not for TM proteins. The type "undefined membrane" can be used for
                    estimating the <i>intrinsic</i> hydrophobic thickness of a TM protein.
                </li>
                <li>
                    With selected "curvature allowed" option, PPM 3.0 automatically defines whether
                    a protein of interest would better fit to a planar or a spherically deformed
                    bilayer. In the latter case, the output includes the intrinsic curvature of the
                    protein and coordinates of protein together with curved membrane boundaries.
                </li>
                <li>
                    PPM 3.0 uses parameters of symmetric membranes with polarity profile of the DOPC
                    bilayer, distinct hydrophobic thicknesses for different membrane types [
                    <a href="https://pubmed.ncbi.nlm.nih.gov/23811361/" target="_blank">
                        PubMed
                    </a>
                    ],
                    and a quadratic penalty for contraction or stretching of lipids to relieve the
                    hydrophobic mismatch with transmembrane proteins [
                    <a href="https://pubmed.ncbi.nlm.nih.gov/34029472/" target="_blank">
                        PubMed
                    </a>
                    ]. The values of
                    hydrophobic thicknesses ({D0}) and lipid deformation penalties ({fMism}) for
                    different membranes are indicated in Table 1.
                </li>
                <li>
                    {"Topology information (\"in\" or \"out\", see "}
                    <Link to="/about#topology_definitions" target="_blank">
                        topology definitions
                    </Link>
                    ) serves only to
                    color DUMMY atoms and define the sign of Z coordinates of atoms ("-" corresponds
                    to "in" side).
                </li>
            </ol>
            <h1>
                Table 1. Parameters of different membranes used by PPM 3.0: hydrophobic thicknesses
                ({D0}) and stretching stiffness coefficients ({fMism}).
            </h1>
            <table>
                <thead>
                    <tr>
                        <th>
                            Membrane type
                        </th>
                        <th>
                            {D0}, Å<sup>a</sup>
                        </th>
                        <th>
                            {fMism}<sup>b</sup>, kcal/mol/Å<sup>b</sup>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Plasma membrane (mammalian)</td>
                        <td>32.0</td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>Plasma membrane (plants)</td>
                        <td>30.6</td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>Plasma membrane (Fungi)</td>
                        <td>30.6</td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>ER (fungi)</td>
                        <td>28.7</td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>ER (mammalian)</td>
                        <td>28.7</td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>Golgi membrane</td>
                        <td>30.0</td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>Lysosome membrane</td>
                        <td>30.7</td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>Endosome membrane</td>
                        <td>30.0</td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>Vacuole membrane</td>
                        <td>30.4</td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>Outer mithochondrial membrane</td>
                        <td>22.7</td>
                        <td>0.01</td>
                    </tr>
                    <tr>
                        <td>Inner mitochondrial membrane</td>
                        <td>28.4</td>
                        <td>0.01</td>
                    </tr>
                    <tr>
                        <td>Thylakoid membrane (plants)</td>
                        <td>30.2</td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>Thylakoid membrane (bacteria)</td>
                        <td>30.8</td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>Gram-negative bacteria outer membrane</td>
                        <td>23.9</td>
                        <td>0.015</td>
                    </tr>
                    <tr>
                        <td>Gram-negative bacteria inner membrane</td>
                        <td>30.1</td>
                        <td>0.015</td>
                    </tr>
                    <tr>
                        <td>Gram-positive bacteria inner membrane</td>
                        <td>30.3</td>
                        <td>0.015</td>
                    </tr>
                    <tr>
                        <td>Archaebacteria cell membrane</td>
                        <td>30.2</td>
                        <td>0.015</td>
                    </tr>
                    <tr>
                        <td>Undefined membrane</td>
                        <td>30.0/23.9<sup>c</sup></td>
                        <td>0.001</td>
                    </tr>
                    <tr>
                        <td>DLPC bilayer</td>
                        <td>21.7<sup>d</sup></td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>DMPC bilayer</td>
                        <td>25.7<sup>d</sup></td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>DOPC bilayer</td>
                        <td>28.8<sup>d</sup></td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>DEuPC bilayer</td>
                        <td>35.8<sup>e</sup></td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>DPC micelle</td>
                        <td>38.0<sup>f</sup></td>
                        <td>N/A</td>
                    </tr>
                </tbody>
            </table>
            <div>
                <div>
                    <sup>a</sup> for biomembranes, {D0} is based on the average hydrophobic
                    thicknesses of TM proteins from the corresponding membranes [
                    <a href="https://pubmed.ncbi.nlm.nih.gov/23811361/" target="_blank">
                        PubMed
                    </a>
                    ]
                </div>
                <div>
                    <sup>b</sup> estimations from [
                    <a href="https://pubmed.ncbi.nlm.nih.gov/34029472/" target="_blank">
                        PubMed
                    </a>
                    ]
                </div>
                <div>
                    <sup>c</sup> {D0} of membranes carrying either TM α-helical or TM β-barrel
                    proteins
                </div>
                <div>
                    <sup>d</sup> from [
                    <a href="https://pubmed.ncbi.nlm.nih.gov/21819968/" target="_blank">
                        PubMed
                    </a>
                    ]
                </div>
                <div>
                    <sup>e</sup> from [
                    <a href="https://pubmed.ncbi.nlm.nih.gov/18782557/" target="_blank">
                        PubMed
                    </a>
                    ]
                </div>
                <div>
                    <sup>f</sup> from [
                    <a href="https://pubmed.ncbi.nlm.nih.gov/26301341/" target="_blank">
                        PubMed
                    </a>
                    ]
                </div>
            </div>
            <h1>
                Output
            </h1>
            <div className="paragraphs">
                <div>
                    Output includes coordinates of a protein with calculated boundaries of flat or
                    curved membranes together with the characteristic parameters.
                </div>
                <div>
                    <b>Coordinate files of proteins in membranes.</b> For structures positioned in a
                    single planar bilayer, the origin of coordinates corresponds to the center of
                    the lipid bilayer. Z axis coincides with membrane normal; atoms with the
                    positive sign of Z coordinate are associated with the "outer" leaflet as defined
                    by the user-specified topology. Positions of DUMMY atoms correspond to membrane
                    boundaries at the level of lipid carbonyl groups. For structures with several
                    bilayers, the origin of coordinates is defined by the last bilayer in the set.
                    The center of the spherical membrane or the micelle corresponds to the origin of
                    coordinates.
                </div>
                <div>
                    <b>Membrane penetration depth or hydrophobic thickness.</b> This parameter
                    indicates the calculated hydrophobic thickness (for TM proteins) or maximal
                    penetration depth of protein atoms into the lipid hydrocarbon core (for
                    peripheral/monotopic) proteins.
                </div>
                <div>
                    <b>Tilt angle</b> is calculated between membrane normal (Z axis) and protein
                    axis. The protein axis is calculated as the sum of TM secondary structure
                    segment vectors (for TM proteins) or as the principal inertia axis (for
                    peripheral proteins).
                </div>
                <div>
                    <b>Transfer energy of peripheral proteins from water to the lipid bilayer.</b>
                    The calculated transfer energy corresponds to the actual membrane binding energy
                    of peripheral proteins, unless: (a) some membrane-anchoring elements are missing
                    or disordered in the crystal structure; (b) there is strong specific binding of
                    lipids (e.g. in PH domains and other "lipid clamps"); or (c) membrane binding is
                    coupled with significant structural changes of the protein (e.g. helix-coil
                    transition for amphiphilic α-helices). In situations (a) and (b), the calculated
                    membrane binding free energy is underestimated. In situation (c) it is usually
                    overestimated.
                </div>
                <div>
                    Results of calculations for peripheral proteins with transfer energies smaller
                    than -5 kcal/mol must be interpreted with caution because hydrophobic surfaces
                    of these proteins may be involved in protein-protein rather than protein-lipid
                    interactions.
                </div>
            </div>
            <h1>
                Contact
            </h1>
            <div>
                Please send any questions or requests to Andrei Lomize (
                <a href="mailto:lomizeal@gmail.com" target="_blank">
                    lomizeal@gmail.com
                </a>
                )
            </div>
            <h1>
                Source code
            </h1>
            <div>
                {"PPM "}
                <a
                    href="https://console.cloud.google.com/storage/browser/opm-assets/ppm3_code"
                    target="_blank"
                >
                    source code
                </a>
                {" for multiple input PDB files"}
            </div>
        </div>
    );
}

export default Instructions;
