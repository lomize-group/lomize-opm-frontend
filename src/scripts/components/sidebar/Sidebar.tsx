import * as React from "react";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect, ConnectedProps } from "react-redux";

import axios from "axios";

import { Assets } from "../../config/Assets";
import { Urls } from "../../config/Urls";
import Classification from "../../config/Classification";
import { capitalize } from "../../config/Util";

import { loadStats } from "../../store/stats";
import ExternalLink from "../external_link/ExternalLink";

import "./Sidebar.scss";
import { RootState } from "../../store";

function mapStateToProps(state: RootState) {
	return {
		stats: state.stats,
		currentUrl: state.currentUrl,
	};
}

const connector = connect(mapStateToProps, { loadStats });

type SidebarProps = ConnectedProps<typeof connector>;
type SidebarState = {
	loading: boolean
};

function renderNavItem(key: number, name: string, route: string, subclasses: any, selected: boolean) {
	if (selected) {
		return (
			<div key={key} className={"item"}>
				<strong>{name}</strong>
				<span>{" "}({subclasses})</span>
			</div>
		);
		
	}
	return (
		<div key={key} className={"item not-selected"}>
			<Link to={route}><strong>{name}</strong></Link>
			<span>{" "}({subclasses})</span>
		</div>
	);
}

class Sidebar extends React.Component<SidebarProps, SidebarState> {

	state = {
		loading: this.props.stats === null
	};

	componentDidMount() {
		if (this.props.stats === null){
			const url = Urls.api_url("stats");
			axios.get(url)
			.then(res => {
				this.props.loadStats({
					protein_types: res.data.table.types,
					protein_classes: res.data.table.classtypes,
					protein_superfamilies: res.data.table.superfamilies,
					protein_families: res.data.table.family,
					protein_species: res.data.table.species,
					protein_localizations: res.data.table.membrane,
					protein_structure_stats: res.data.table.structure_stats,
					proteins: res.data.table.primary_structure,
					assembly_superfamilies: res.data.table.assembly_superfamilies,
					assembly_families: res.data.table.assembly_families,
					assembly_localizations: res.data.table.assembly_membrane,
					assemblies: res.data.table.assembly,
				});

				this.setState({ loading: false });
			});
		}
	}

	private getCurrentCtype = () => {
		return this.props.currentUrl.params.id ? "" : this.props.currentUrl.url.split("/")[1];
	}

	renderProteinItems() {
		if (this.state.loading || this.props.stats === null) {
			return null;
		}

		const curCtype = this.getCurrentCtype();

		let items = [];
		for (let i = 0; i < Classification.proteins.length; i++) {
		    const ctype = Classification.proteins[i];
		    const name = capitalize(Classification.definitions[ctype].name);
		    const route = "/" + ctype;
		    items.push(renderNavItem(i, name, route, this.props.stats[ctype], curCtype === ctype));
		}
		return items;
	}

	renderAssemblyItems() {
		if (this.state.loading || this.props.stats === null) {
			return;
		}

		const curCtype = this.getCurrentCtype();

		let items = [];
		for (let i = 0; i < Classification.assemblies.length; ++i) {
			const ctype = Classification.assemblies[i];
			const name = capitalize(Classification.definitions[ctype].name);
			const route = '/' + ctype;
			items.push(renderNavItem(i, name, route, this.props.stats[ctype], curCtype === ctype));
		}
		return items;
	}

	render()
	{	
		return(
			<div className="sidebar">
				<div className="title">
					Protein Classification
				</div>
				<div className={this.state.loading ? "hidden" : "sidebar-nav"}>
						{this.renderProteinItems()}
				</div>
				<div className={this.state.loading ? "info-loading" : "hidden"}>
					<img className="loading-image" src={Assets.images.loading}/>
				</div>
				<div className="title">
					Assembly
				</div>
				<div className={this.state.loading ? "hidden" : "sidebar-nav"}>
						{this.renderAssemblyItems()}
				</div>
				<div className={this.state.loading ? "info-loading" : "hidden"}>
					<img className="loading-image" src={Assets.images.loading}/>
				</div>
				<div className="title">
					Protein Links
				</div>
				<div className="sidebar-nav minor">
					<ExternalLink href="https://www.ebi.ac.uk/thornton-srv/databases/pdbsum/">
						PDB Sum
					</ExternalLink>,{" "}  
   					<ExternalLink href="https://www.rcsb.org/pdb/">
   						PDB
   					</ExternalLink>,{" "}  
   					<ExternalLink href="https://blanco.biomol.uci.edu/mpstruc/">
   						MPKS
   					</ExternalLink>,{" "}
   					<br/>
   					<ExternalLink href="http://www.mpdb.tcd.ie/" >
   						MPDB
   					</ExternalLink>{" "} 
				</div>
				<div className="server-img">
					<Link to="/ppm_server">
						<img className="img-style" src={Assets.images.ppm_server}/>
					</Link>
				</div>
				<div className="server-img">
					<Link to="/tmpfold_server_cgopm">
						<img className="img-style" src={Assets.images.tmpfold_server}/>
					</Link>
				</div>
			</div>
		);
	}
}


export default connector(Sidebar);
