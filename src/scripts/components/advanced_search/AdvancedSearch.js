import * as React from "react";

import Table from '../table/Table.js';

import { Urls } from "../../config/Urls";
import Classification from "../../config/Classification";
import Columns from "../table/Columns";

import { capitalize } from "../../config/Util";

import Info from "./info.jsx";
import "./AdvancedSearch.scss";


class AdvancedSearch extends React.Component
{
	static searchable_objects = ["proteins"];

	constructor(props)
	{
		super(props);

		this.get_initial_state = () => ({
			search_for: AdvancedSearch.searchable_objects[0],
			submitted: false,
			submit_data: {},
			results: []
		})

		this.state = this.get_initial_state();

		this.reset = () => {
			this.setState(this.get_initial_state());
		}

		this.handle_change = (event) => {
			this.setState({[event.target.id]: event.target.value});
		}

		this.search = (event) => {
			event.preventDefault();

			const not_blank = val => val !== "" && val !== null && val !== undefined;
			
			// get named form fields
			const form_vals = Object.values(event.target.elements).filter(elem => not_blank(elem.name));

			// append all needed search params
			let submit_data = {};
			for (let i = 0; i < form_vals.length; ++i) {
				const { name, value } = form_vals[i];
				submit_data[name] = value;
			}

			// force table to reload by setting submitted to false, then true again
			this.setState({
				submitted: false,
			}, () => this.setState({
					submitted: true,
					submit_data
				})
			);
		}
	}

	renderResults()
	{
		if (!this.state.submitted) {
			return null;
		}

		const ctype = this.state.search_for;

		return(
			<Table
				title={"Search results for " + capitalize(ctype)}
				url={Urls.api_url(Classification.definitions[ctype].api.route + "/advanced_search", this.state.submit_data)}
				columns={Columns[ctype].filterByPage(ctype).columns}
				redirect={false}
				show_search={false}
			/>
		);
	}

	renderProteinInputs()
	{
		const render_comparisons = (id_name) => {
			return(
				<select
					className="form-control input-sm"
					name={id_name}
					id={id_name}
					value={this.state[id_name]}
				>
					<option value=">=">&ge;</option>
					<option value="<=">&le;</option>
					{(id_name === "subunits_count_compare" || id_name === "subunit_segments_count_compare") && 
						<option value="=">=</option>
					}
				</select>
			);
		}

		return(
			<div className="row">
				<div className="col-xs-4">
					<div className="form-group">
						<label htmlFor="name">Protein Name</label>{" "}
						<input
							type="text"
							className="form-control input-sm"
							name="name"
							id="name"
							placeholder="Enter entire or part of a protein name"
						/>
					</div>
					<div className="form-group">
						<label htmlFor="pdbid">PDB ID</label>{" "}
						<input
							type="text"
							className="form-control input-sm"
							name="pdbid"
							id="pdbid"
							placeholder="Examples: '1UAZ' or '1uaz' or '1u'"
						/>
					</div>
                    <div className="form-group">
                        <label htmlFor="uniprotcode">Uniprot code</label>{" "}
                        <input
                            type="text"
                            className="form-control input-sm"
                            name="uniprotcode"
                            id="uniprotcode"
                            placeholder="Entire entire or part of a uniprot code"
                        />
                    </div>
					<div className="form-group">
						<label htmlFor="type">Type</label>{" "}
						<input
							type="text"
							className="form-control input-sm"
							name="type"
							id="type"
							placeholder="Enter entire or part of a type name"
						/>
					</div>
					<div className="form-group">
						<label htmlFor="classtype">Class</label>{" "}
						<input
							type="text"
							className="form-control input-sm"
							name="classtype"
							id="classtype"
							placeholder="Enter entire or part of a class name"
						/>
					</div>
					<div className="form-group">
						<label htmlFor="species">Species</label>{" "}
						<input
							type="text"
							className="form-control input-sm"
							name="species"
							id="species"
							placeholder="Enter entire or part of a species name"
						/>
					</div>
					<div className="form-group" id="localization-group">
						<label htmlFor="membrane">Localization</label>{" "}
                        <div className="my-tooltip">
                            <Info />
                            <div className="my-tooltip-text">
                                {"Search for localizations as defined in "}
                                <a href="https://opm.phar.umich.edu/protein_localizations" target="_blank">OPM</a>
                                , e.g. "Bacterial", "Gram-positive inner", "Plasma", or "Thylakoid"
                            </div>
                        </div>
						<input
							type="text"
							className="form-control input-sm"
							name="membrane"
							id="membrane"
							placeholder="Enter entire or part of a localization name"
						/>
					</div>
					<div className="form-group">
						<label htmlFor="superfamily">Superfamily</label>{" "}
					<input
						type="text"
						className="form-control input-sm"
						name="superfamily"
						id="superfamily"
						value={this.state.superfamily}
						placeholder="Enter entire or part of a superfamily name"
					/>
					</div>
					<div className="form-group">
						<label htmlFor="family">Family</label>{" "}
						<input
							type="text"
							className="form-control input-sm"
							name="family"
							id="family"
							value={this.state.family}
							placeholder="Enter entire or part of a family name"
						/>
					</div>
				</div>
				<div className="col-xs-7 col-xs-offset-1">
					<div className="form-group form-inline row">
						<div className="col-xs-3">
							<label htmlFor="resolution">Resolution</label>
						</div>
						<div className="col-xs-9">
							{render_comparisons("resolution_compare")}{" "}
							<input
								type="text"
								className="form-control input-sm"
								id="resolution"
								name="resolution"
								placeholder="Example: 1"
							/>
						</div>
					</div>
					<div className="form-group form-inline row">
						<div className="col-xs-3">
							<label htmlFor="subunits_count">Number of TM Subunits</label>
						</div>
						<div className="col-xs-9">
							{render_comparisons("subunits_count_compare")}{" "}
							<input
								type="text"
								className="form-control input-sm"
								id="subunits_count"
								name="subunits_count"
								placeholder="Example: 2"
							/>
						</div>
					</div>
					<div className="form-group form-inline row">
						<div className="col-xs-3">
							<label htmlFor="subunit_segments_count">Number of TM Secondary Structures</label>
						</div>
						<div className="col-xs-9">
							{render_comparisons("subunit_segments_count_compare")}{" "}
							<input
								type="text"
								className="form-control input-sm"
								id="subunit_segments_count"
								name="subunit_segments_count"
								placeholder="Example: 3"
							/>
						</div>
					</div>
					<div className="form-group form-inline row">
						<div className="col-xs-3">
							<label htmlFor="thickness">Thickness / Depth</label>
						</div>
						<div className="col-xs-9">
							{render_comparisons("thickness_compare")}{" "}
							<input
								type="text"
								className="form-control input-sm"
								id="thickness"
								name="thickness"
								placeholder="Examples: 4.3, 4"
							/>
						</div>
					</div>
					<div className="form-group form-inline row">
						<div className="col-xs-3">
							<label htmlFor="tilt">Tilt Angle</label>
						</div>
						<div className="col-xs-9">
							{render_comparisons("tilt_compare")}{" "}
							<input
								type="text"
								className="form-control input-sm"
								id="tilt"
								name="tilt"
								placeholder="Example: 5"
							/>
						</div>
					</div>
					<div className="form-group form-inline row">
						<div className="col-xs-3">
							<label htmlFor="gibbs">&Delta;G<sub>transfer</sub></label>
						</div>
						<div className="col-xs-9">
							{render_comparisons("gibbs_compare")}{" "}
							<input
								type="text"
								className="form-control input-sm"
								id="gibbs"
								name="gibbs"
								placeholder="Examples: 6.3, 6"
							/>
						</div>
					</div>
					<div className="form-group form-inline row">
						<div className="col-xs-3">
							<label htmlFor="taxon">Taxon</label>
                            <div className="my-tooltip">
                                <Info />
                                <div className="my-tooltip-text">
                                    Search for taxonomic groups as defined in Uniprot, e.g. Bacteria,
                                    Eukaryota, Fungi, Viruses, Viridiplantae, Arthropoda, Mammalia,
                                    Cyanobacteria, or Firmicutes
                                </div>
                            </div>
						</div>
						<div className="col-xs-9">
							<input
								type="text"
								className="form-control input-sm"
								id="taxon"
								name="taxon"
   								placeholder="Example: Bacteria, Eukaryota; Fungi, Viruses, Viridiplantae, Arthropoda"
							/>
						</div>
					</div>
				</div>
			</div>
		);
	}

	renderAssembliesInputs()
	{
		// TODO
		return(
			null
		);
	}

	render()
	{
		return(
			<div className="advanced-search">
				<div className="page-header">Search</div>
				<form className="inputs-area" onSubmit={this.search}>
					<div className="form-inline">
						<label htmlFor="search_for">Search for</label>{" "}
						<select
							className="form-control input-sm"
							name="search_for"
							id="search_for"
							value={this.state.search_for}
							onChange={this.handle_change}
						>
							{
								AdvancedSearch.searchable_objects.map((name, idx) =>
									<option key={idx} value={name}>{name}</option>
								)
							}
						</select>
					</div>
					<br/>
					<div className="search-options">
						<h2>Search Options</h2>
						<p>
							Search results will satisfy the conditions of all inputs. Leaving an input
							empty will exclude it from the search.
						</p>
						<br/>
						{this.state.search_for === "proteins" && this.renderProteinInputs()}
						{this.state.search_for === "assemblies" && this.renderAssembliesInputs()}
					</div>
					<br/>
					<div className="input-group search-buttons">
						<input
							type="submit"
							value="Search"
							className="submit-button"
						/>
						{" "}
						<input
							type="reset"
							value="Reset"
							className="submit-button"
							onClick={this.reset}
						/>
					</div>
				</form>
				<br/>
				{this.renderResults()}
			</div>
		);
	}
}

export default AdvancedSearch;
