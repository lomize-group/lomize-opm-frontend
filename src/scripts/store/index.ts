import { combineReducers } from "redux";
import { statsReducer } from "./stats";
import { urlReducer } from "./url";

const reducersMap = {
    currentUrl: urlReducer,
    stats: statsReducer,
} as const;

export const rootReducer = combineReducers(reducersMap);

export type RootState = ReturnType<typeof rootReducer>;
