import { CType } from "../../config/Classification";

export type StatsType =
    | Exclude<CType, "secondary_representations" | "structure_subunits" | "citations">
    | "protein_structure_stats";

export type StatsState = { [key in StatsType]: any };

export const LOAD_STATS = "LOAD_STATS" as const;

type LoadStatsAction = {
    type: typeof LOAD_STATS;
    payload: StatsState;
};

export type StatsActionType = LoadStatsAction;
