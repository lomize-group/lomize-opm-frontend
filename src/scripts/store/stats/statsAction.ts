import { LOAD_STATS, StatsActionType, StatsState } from "./statsTypes";

export function loadStats(siteStats: StatsState): StatsActionType {
    return {
        type: LOAD_STATS,
        payload: siteStats,
    };
}
