import { History } from "history";
import { match } from "react-router-dom";
import { GenericObj } from "../../config/Types";

export const UPDATE_CURRENT_URL = "UPDATE_CURRENT_URL" as const;

// url info passed to component by redux
export interface UrlState<MatchParams = any> extends match<MatchParams> {
    query: GenericObj;
    history: History;
}

type UpdateCurrentUrlAction<T> = {
    type: typeof UPDATE_CURRENT_URL;
    payload: UrlState<T>;
};

export type UrlActionType<T> = UpdateCurrentUrlAction<T>;
