import { createBrowserHistory } from "history";
import { UPDATE_CURRENT_URL, UrlActionType, UrlState } from "./urlTypes";

const initialState: UrlState = {
    history: createBrowserHistory(),
    query: {},
    params: {},
    isExact: false,
    path: "",
    url: "",
};

export function urlReducer<T>(state: UrlState = initialState, action: UrlActionType<T>): UrlState {
    switch (action.type) {
        case UPDATE_CURRENT_URL:
            return action.payload;
        default:
            return state;
    }
}
