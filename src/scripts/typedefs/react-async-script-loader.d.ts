declare module "react-async-script-loader" {
    import { ComponentType } from "react";

    interface ScriptLoadProps {
        isScriptLoaded: boolean;
        isScriptLoadSucceed: boolean;
    }

    interface ClassDecorator<P> {
        (component: ComponentType): ComponentType<P>;
    }

    function scriptLoader<P>(...urls: string[]): ClassDecorator<Omit<P, keyof ScriptLoadProps>>;

    export default scriptLoader;
}
