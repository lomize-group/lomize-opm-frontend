const path = require("path");

const paths = {
    src: path.resolve(__dirname, "src"),
    public: path.resolve(__dirname, "public"),
};

const baseConfig = {
    context: paths.src,
    entry: ["./scripts/main.js"],
    output: {
        path: paths.public,
        publicPath: "/",
        filename: "main.js",
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx"],
    },
    module: {
        rules: [
            {
                // .ts and .js files
                test: /\.(t|j)sx?$/,
                loader: "ts-loader",
                exclude: /node_modules/,
            },
            {
                // sass
                test: /\.scss$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader", // compiles Sass to CSS
                ],
            },
            {
                // css
                test: /\.css$/,
                include: /node_modules/,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
                use: "url-loader",
            },
        ],
    },
};

module.exports = {
    paths,
    baseConfig,
};
