const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { paths, baseConfig } = require("./webpack.base.config");
const NodePolyfillPlugin = require("node-polyfill-webpack-plugin");

module.exports = {
    ...baseConfig,
    mode: "development",
    plugins: [
        // for compiling CSS
        new MiniCssExtractPlugin({
            filename: "styles.css",
        }),
        new NodePolyfillPlugin({
            excludeAliases: ["console"],
        }),
    ],
    optimization: {
        // prints more readable module names in the browser console on HMR updates
        moduleIds: "named",
    },
    devtool: "eval-source-map",
    devServer: {
        port: 8080,
        static: {
            directory: paths.public,
        },
        historyApiFallback: {
            rewrites: [
                {
                    from: /\./,
                    to: "/",
                },
            ],
        },
    },
};
